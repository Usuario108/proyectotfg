package com.example.proyectorest.modelo;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;

@Entity
@Table(name = "mascotas")
public class Mascota {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	@Column(name = "refugioid", nullable = false)
    private Long refugioid;
	@Column(name = "nombre")
    private String nombre;
	@Column(name = "nacimiento")
    private Date nacimiento;
	@Column(name = "raza")
    private String raza;
	@Column(name = "peso")
    private Float peso;
	@Column(name = "descripcion", columnDefinition = "TEXT")
    private String descripcion;
	@Lob
	@Column(name = "imagen", nullable = false)
    private byte[] imagen;
	
	public Mascota() {
		super();
	}

	public Mascota(Long id, Long refugioid, String nombre, Date nacimiento, String raza, Float peso, String descripcion,
			byte[] imagen) {
		super();
		this.id = id;
		this.refugioid = refugioid;
		this.nombre = nombre;
		this.nacimiento = nacimiento;
		this.raza = raza;
		this.peso = peso;
		this.descripcion = descripcion;
		this.imagen = imagen;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRefugioid() {
		return refugioid;
	}

	public void setRefugioid(Long refugioid) {
		this.refugioid = refugioid;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public Float getPeso() {
		return peso;
	}

	public void setPeso(Float peso) {
		this.peso = peso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}
}
