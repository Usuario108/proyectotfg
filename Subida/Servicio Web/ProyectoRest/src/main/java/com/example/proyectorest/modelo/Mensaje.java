package com.example.proyectorest.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "mensajes")
public class Mensaje {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "emisorid", nullable = false)
    private Long emisorid;
    @Column(name = "receptorid", nullable = false)
    private Long receptorid;
    @Column(name = "hora", nullable = false)
    private String  hora;
    @Column(name = "mensaje")
    private String mensaje;

	public Mensaje() {
		super();
	}

	public Mensaje(Long id, Long emisorid, Long receptorid, String hora, String mensaje) {
		super();
		this.id = id;
		this.emisorid = emisorid;
		this.receptorid = receptorid;
		this.hora = hora;
		this.mensaje = mensaje;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEmisorid() {
		return emisorid;
	}

	public void setEmisorid(Long emisorid) {
		this.emisorid = emisorid;
	}

	public Long getReceptorid() {
		return receptorid;
	}

	public void setReceptorid(Long receptorid) {
		this.receptorid = receptorid;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
