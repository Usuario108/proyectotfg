package com.example.proyectorest.controlador;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.proyectorest.modelo.Refugio;
import com.example.proyectorest.modelo.RefugioRepositorio;

@RestController
public class RefugioController {

    private final RefugioRepositorio refugioRepositorio;
    
    public RefugioController(RefugioRepositorio refugioRepositorio) {
		super();
		this.refugioRepositorio = refugioRepositorio;
	}

	@GetMapping("/refugios")
    public List<Refugio> getRefugios(){
        return this.refugioRepositorio.findAll();
    }
	
	@GetMapping("/refugios/{id}")
	public ResponseEntity<?> getrefugio(@PathVariable Long id) {
		Refugio refugio = refugioRepositorio.findById(id).orElse(null);
		if(refugio==null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(refugio);
	}
	
	@PostMapping("/refugios")
	public ResponseEntity<Refugio> insertRefugio(@RequestBody Refugio refugio) {
		Refugio refu = refugioRepositorio.save(refugio);
		return ResponseEntity.status(HttpStatus.CREATED).body(refu);
	}
	
	@PutMapping("/refugios/{id}")
	public ResponseEntity<?> editRefugio(@PathVariable Long id,@RequestBody Refugio editar){
		Refugio refu = refugioRepositorio.findById(id).orElse(null);
		if(refu==null)
			return ResponseEntity.notFound().build();
		
		refu.setNombre(editar.getNombre());
		refu.setDireccion(editar.getDireccion());
		refu.setTelefono(editar.getTelefono());
		refu.setCorreo(editar.getCorreo());
		refu.setPassword(editar.getPassword());
		refu.setMaximoanimales(editar.getMaximoanimales());
		refu.setResponsableid(editar.getResponsableid());
		refu.setImagen(editar.getImagen());
		
		return ResponseEntity.ok(refugioRepositorio.save(refu));
	}
}
