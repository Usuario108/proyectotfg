package com.example.proyectorest.modelo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdopcionRepositorio extends JpaRepository<Adopcion, Long>
{

}
