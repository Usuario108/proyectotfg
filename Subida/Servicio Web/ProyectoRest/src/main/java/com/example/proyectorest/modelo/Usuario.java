package com.example.proyectorest.modelo;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;

@Entity
@Table(name = "usuarios")
public class Usuario {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	@Column(name = "refugioid")
    private Long refugioid;
	@Column(name = "nombre")
    private String nombre;
	@Column(name = "nacimiento")
	@JsonFormat(pattern = "yyyy-MM-dd")
    private Date nacimiento;
	@Column(name = "telefono")
    private String telefono;
    @Column(name = "correo", nullable = false)
    private String correo;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "direccion")
    private String direccion;
    @Lob
    @Column(name = "imagen", nullable = false)
    private byte[] imagen;
    
	public Usuario() {
		super();
	}

	public Usuario(Long id, Long refugioid, String nombre, Date nacimiento, String telefono, String correo,
			String password, String direccion, byte[] imagen) {
		super();
		this.id = id;
		this.refugioid = refugioid;
		this.nombre = nombre;
		this.nacimiento = nacimiento;
		this.telefono = telefono;
		this.correo = correo;
		this.password = password;
		this.direccion = direccion;
		this.imagen = imagen;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRefugioid() {
		return refugioid;
	}

	public void setRefugioid(Long refugioid) {
		this.refugioid = refugioid;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}
}
