package com.example.proyectorest.controlador;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.proyectorest.modelo.Mensaje;
import com.example.proyectorest.modelo.MensajeRepositorio;

@RestController
public class MensajeController {

	private final MensajeRepositorio mensajeRepositorio;
    
    public MensajeController(MensajeRepositorio mensajeRepositorio) {
		super();
		this.mensajeRepositorio = mensajeRepositorio;
	}

	@GetMapping("/mensajes")
    public List<Mensaje> getMensajes(){
        return this.mensajeRepositorio.findAll();
    }
	
	@PostMapping("/mensajes")
	public ResponseEntity<Mensaje> insertMensaje(@RequestBody Mensaje mensaje) {
		Mensaje men = mensajeRepositorio.save(mensaje);
		return ResponseEntity.status(HttpStatus.CREATED).body(men);
	}
}
