package com.example.proyectorest.modelo;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.Table;

@Entity
@Table(name = "refugios")
public class Refugio {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "direccion")
    private String direccion;
    @Column(name = "telefono")
    private String telefono;
    @Column(name = "correo", nullable = false)
    private String correo;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "maximoanimales")
    private Integer maximoanimales;
    @Column(name = "responsableid", nullable = false)
    private Long responsableid;
    @Lob
    @Column(name = "imagen", nullable = false)
    private byte[] imagen;
    
    public Refugio() {
		super();
	}
    
	public Refugio(Long id, String nombre, String direccion, String telefono, String correo, String password,
			Integer maximoanimales, Long responsableid, byte[] imagen) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.direccion = direccion;
		this.telefono = telefono;
		this.correo = correo;
		this.password = password;
		this.maximoanimales = maximoanimales;
		this.responsableid = responsableid;
		this.imagen = imagen;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getMaximoanimales() {
		return maximoanimales;
	}

	public void setMaximoanimales(Integer maximoanimales) {
		this.maximoanimales = maximoanimales;
	}

	public Long getResponsableid() {
		return responsableid;
	}

	public void setResponsableid(Long responsableid) {
		this.responsableid = responsableid;
	}

	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}
}
