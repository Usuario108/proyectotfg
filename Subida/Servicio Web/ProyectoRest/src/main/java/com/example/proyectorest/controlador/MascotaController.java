package com.example.proyectorest.controlador;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.proyectorest.modelo.Mascota;
import com.example.proyectorest.modelo.MascotaRepositorio;

@RestController
public class MascotaController {

	private final MascotaRepositorio mascotaRepositorio;
    
    public MascotaController(MascotaRepositorio mascotaRepositorio) {
		super();
		this.mascotaRepositorio = mascotaRepositorio;
	}

	@GetMapping("/mascotas")
    public List<Mascota> getMascotas(){
        return this.mascotaRepositorio.findAll();
    }
	
	@GetMapping("/mascotas/{id}")
	public ResponseEntity<?> getMascota(@PathVariable Long id) {
		Mascota mascota = mascotaRepositorio.findById(id).orElse(null);
		if(mascota==null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(mascota);
	}
	
	@PostMapping("/mascotas")
	public ResponseEntity<Mascota> insertMascota(@RequestBody Mascota mascota) {
		Mascota mas = mascotaRepositorio.save(mascota);
		return ResponseEntity.status(HttpStatus.CREATED).body(mas);
	}
	
	@PutMapping("/mascotas/{id}")
	public ResponseEntity<?> editMascota(@PathVariable Long id,@RequestBody Mascota editar){
		Mascota mas = mascotaRepositorio.findById(id).orElse(null);
		if(mas==null)
			return ResponseEntity.notFound().build();
		
		mas.setNombre(editar.getNombre());
		mas.setNacimiento(editar.getNacimiento());
		mas.setRaza(editar.getRaza());
		mas.setPeso(editar.getPeso());
		mas.setDescripcion(editar.getDescripcion());
		mas.setImagen(editar.getImagen());
		
		return ResponseEntity.ok(mascotaRepositorio.save(mas));
	}
	
	@DeleteMapping("/mascotas/{id}")
	public ResponseEntity<?> borrarMascota(@PathVariable Long id){
		mascotaRepositorio.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}

