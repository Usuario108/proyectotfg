package com.example.proyectorest.controlador;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.proyectorest.modelo.Adopcion;
import com.example.proyectorest.modelo.AdopcionRepositorio;

@RestController
public class AdopcionController {

	private final AdopcionRepositorio adopcionRepositorio;
    
    public AdopcionController(AdopcionRepositorio adopcionRepositorio) {
		super();
		this.adopcionRepositorio = adopcionRepositorio;
	}

	@GetMapping("/adopciones")
    public List<Adopcion> getAdopciones(){
        return this.adopcionRepositorio.findAll();
    }
	
	@GetMapping("/adopciones/{id}")
	public ResponseEntity<?> getAdopcion(@PathVariable Long id) {
		Adopcion adopcion = adopcionRepositorio.findById(id).orElse(null);
		if(adopcion==null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(adopcion);
	}
	
	@PostMapping("/adopciones")
	public ResponseEntity<Adopcion> insertAdopcion(@RequestBody Adopcion adopcion) {
		Adopcion adop = adopcionRepositorio.save(adopcion);
		return ResponseEntity.status(HttpStatus.CREATED).body(adop);
	}
	
	@PutMapping("/adopciones/{id}")
	public ResponseEntity<?> editAdopcion(@PathVariable Long id,@RequestBody Adopcion editar){
		Adopcion adop = adopcionRepositorio.findById(id).orElse(null);
		if(adop==null)
			return ResponseEntity.notFound().build();
		
		adop.setIniciofec(editar.getIniciofec());
		adop.setFinfec(editar.getFinfec());
		
		return ResponseEntity.ok(adopcionRepositorio.save(adop));
	}
	
	@DeleteMapping("/adopciones/{id}")
	public ResponseEntity<?> borrarAdopcion(@PathVariable Long id){
		adopcionRepositorio.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}

