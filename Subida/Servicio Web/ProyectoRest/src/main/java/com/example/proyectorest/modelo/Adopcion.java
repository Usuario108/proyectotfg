package com.example.proyectorest.modelo;

import java.sql.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "adopciones")
public class Adopcion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; 
    @Column(name = "userid", nullable = false)
    private Long userid;
    @Column(name = "mascotaid", nullable = false)
    private Long mascotaid;
    @Column(name = "solicitudfec", nullable = false)
    private Date solicitudfec;
    @Column(name = "iniciofec")
    private Date iniciofec;
    @Column(name = "finfec")
    private Date finfec;
    
	public Adopcion() {
		super();
	}

	public Adopcion(Long id, Long userid, Long mascotaid, Date solicitudfec, Date iniciofec, Date finfec) {
		super();
		this.id = id;
		this.userid = userid;
		this.mascotaid = mascotaid;
		this.solicitudfec = solicitudfec;
		this.iniciofec = iniciofec;
		this.finfec = finfec;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserid() {
		return userid;
	}

	public void setUserid(Long userid) {
		this.userid = userid;
	}

	public Long getMascotaid() {
		return mascotaid;
	}

	public void setMascotaid(Long mascotaid) {
		this.mascotaid = mascotaid;
	}

	public Date getSolicitudfec() {
		return solicitudfec;
	}

	public void setSolicitudfec(Date solicitudfec) {
		this.solicitudfec = solicitudfec;
	}

	public Date getIniciofec() {
		return iniciofec;
	}

	public void setIniciofec(Date iniciofec) {
		this.iniciofec = iniciofec;
	}

	public Date getFinfec() {
		return finfec;
	}

	public void setFinfec(Date finfec) {
		this.finfec = finfec;
	}
}
