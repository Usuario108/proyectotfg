package com.example.proyectorest.controlador;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.proyectorest.modelo.Usuario;
import com.example.proyectorest.modelo.UsuarioRepositorio;

@RestController
public class UsuarioController {

	private final UsuarioRepositorio usuarioRepositorio;
    
    public UsuarioController(UsuarioRepositorio usuarioRepositorio) {
		super();
		this.usuarioRepositorio = usuarioRepositorio;
	}

	@GetMapping("/usuarios")
    public List<Usuario> getUsuarios(){
        return this.usuarioRepositorio.findAll();
    }
	
	@GetMapping("/usuarios/{id}")
	public ResponseEntity<?> getUsuario(@PathVariable Long id) {
		Usuario usuario = usuarioRepositorio.findById(id).orElse(null);
		if(usuario==null)
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok(usuario);
	}
	
	@PostMapping("/usuarios")
	public ResponseEntity<Usuario> insertUsuario(@RequestBody Usuario usuario) {
		Usuario user = usuarioRepositorio.save(usuario);
		return ResponseEntity.status(HttpStatus.CREATED).body(user);
	}
	
	@PutMapping("/usuarios/{id}")
	public ResponseEntity<?> editUsuario(@PathVariable Long id,@RequestBody Usuario editar){
		Usuario user = usuarioRepositorio.findById(id).orElse(null);
		if(user==null)
			return ResponseEntity.notFound().build();
		
		user.setRefugioid(editar.getRefugioid());
		user.setNombre(editar.getNombre());
		user.setNacimiento(editar.getNacimiento());
		user.setTelefono(editar.getTelefono());
		user.setCorreo(editar.getCorreo());
		user.setPassword(editar.getPassword());
		user.setDireccion(editar.getDireccion());
		user.setImagen(editar.getImagen());
		
		return ResponseEntity.ok(usuarioRepositorio.save(user));
	}
	
	@DeleteMapping("/usuarios/{id}")
	public ResponseEntity<?> borrarUsuario(@PathVariable Long id){
		usuarioRepositorio.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
