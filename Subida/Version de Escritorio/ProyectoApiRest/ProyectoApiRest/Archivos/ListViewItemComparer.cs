﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Archivos
{
    public class ListViewItemComparer : IComparer
    {
        private int col;

        public ListViewItemComparer()
        {
            col = 0;
        }

        public ListViewItemComparer(int column)
        {
            col = column;
        }

        public int Compare(object x, object y)
        {
            ListViewItem itx = (ListViewItem)x;
            ListViewItem ity = (ListViewItem)y;

            int returnVal = -1;
            returnVal = String.Compare(itx.SubItems[col].Text, ity.SubItems[col].Text);
            return returnVal;
        }
    }
}
