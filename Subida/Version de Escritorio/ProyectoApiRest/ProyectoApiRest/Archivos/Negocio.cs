﻿using Newtonsoft.Json;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Archivos
{
    public class Negocio
    {
        public Negocio() { }

        public Image ByteToImage(byte[] blob)
        {
            MemoryStream ms = new MemoryStream(blob);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }
        public byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }

        public async Task<List<Refugio>> sacarRefugiosAsync()
        {
            string apiUrl = "http://192.168.0.18:8080/refugios";
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonContent = await response.Content.ReadAsStringAsync();
                        List<Refugio> listaRefugios = JsonConvert.DeserializeObject<List<Refugio>>(jsonContent);
                        return listaRefugios;
                    }
                    else
                    {
                        throw new Exception($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error al obtener refugios: {ex.Message}");
                }
            }
        }
        public async Task<List<Mascota>> sacarMascotasAsync()
        {
            string apiUrl = "http://192.168.0.18:8080/mascotas";
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonContent = await response.Content.ReadAsStringAsync();
                        List<Mascota> listaMascotas = JsonConvert.DeserializeObject<List<Mascota>>(jsonContent);
                        return listaMascotas;
                    }
                    else
                    {
                        throw new Exception($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error al obtener refugios: {ex.Message}");
                }
            }
        }
        public async Task<List<Usuario>> sacarUsuariosAsync()
        {
            string apiUrl = "http://192.168.0.18:8080/usuarios";
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonContent = await response.Content.ReadAsStringAsync();
                        List<Usuario> listaUsuarios = JsonConvert.DeserializeObject<List<Usuario>>(jsonContent);
                        return listaUsuarios;
                    }
                    else
                    {
                        throw new Exception($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error al obtener refugios: {ex.Message}");
                }
            }
        }
        public async Task<List<Mensaje>> sacarMensajesAsync()
        {
            string apiUrl = "http://192.168.0.18:8080/mensajes";
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonContent = await response.Content.ReadAsStringAsync();
                        List<Mensaje> listaMensajes = JsonConvert.DeserializeObject<List<Mensaje>>(jsonContent);
                        return listaMensajes;
                    }
                    else
                    {
                        throw new Exception($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error al obtener refugios: {ex.Message}");
                }
            }
        }
        public async Task<List<Adopcion>> sacarAdopcionesAsync()
        {
            string apiUrl = "http://192.168.0.18:8080/adopciones";
            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await httpClient.GetAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonContent = await response.Content.ReadAsStringAsync();
                        List<Adopcion> listaAdopciones = JsonConvert.DeserializeObject<List<Adopcion>>(jsonContent);
                        return listaAdopciones;
                    }
                    else
                    {
                        throw new Exception($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Error al obtener refugios: {ex.Message}");
                }
            }
        }


        public async Task InsertarUsuario(Usuario usuario)
        {
            string apiUrl = "http://192.168.0.18:8080/usuarios";

            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    List<Usuario> usuarios = await sacarUsuariosAsync();
                    usuario.id = usuarios.Count + 1;

                    string jsonContent = JsonConvert.SerializeObject(usuario);
                    StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
        public async Task InsertarMascota(Mascota mascota)
        {
            string apiUrl = "http://192.168.0.18:8080/mascotas";

            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    List<Mascota> mascotas = await sacarMascotasAsync();
                    mascota.id = mascotas.Count + 1;

                    string jsonContent = JsonConvert.SerializeObject(mascota);
                    StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
        public async Task<bool> ValidarNuevaAdopcion(Adopcion soli)
        {
            List<Adopcion> adopciones = await sacarAdopcionesAsync();

            foreach (var adopcion in adopciones)
            {
                if (adopcion.mascotaid == soli.mascotaid && adopcion.userid == soli.userid && adopcion.iniciofec == null)
                {
                    MessageBox.Show("Ya has enviado la adopcion de este animal.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }
        public async Task InsertarMensaje(Mensaje mensaje)
        {
            string apiUrl = "http://192.168.0.18:8080/mensajes";

            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    List<Mensaje> mensajes = await sacarMensajesAsync();
                    mensaje.id = mensajes.Count + 1;

                    string jsonContent = JsonConvert.SerializeObject(mensaje);
                    StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
        public async Task InsertarSolicitud(Adopcion adopcion)
        {
            string apiUrl = "http://192.168.0.18:8080/adopciones";

            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    List<Adopcion> adopciones = await sacarAdopcionesAsync();
                    adopcion.id = adopciones.Count + 1;

                    string jsonContent = JsonConvert.SerializeObject(adopcion);
                    StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }
        public async Task InsertarRefugio(Refugio refugio)
        {
            string apiUrl = "http://192.168.0.18:8080/refugios";

            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    List<Refugio> refugios = await sacarRefugiosAsync();
                    refugio.id = refugios.Count + 1;

                    string jsonContent = JsonConvert.SerializeObject(refugio);
                    StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PostAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }

        public async Task<bool> ComprobarCorreo(string correo)
        {
            List<Refugio> refugios = await sacarRefugiosAsync();
            List<Usuario> usuarios = await sacarUsuariosAsync();

            foreach (var refugio in refugios)
            {
                if (refugio.correo.Equals(correo))
                {
                    MessageBox.Show("El correo le pertenece a otra cuenta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            foreach (var usuario in usuarios)
            {
                if (usuario.correo.Equals(correo))
                {
                    MessageBox.Show("El correo le pertenece a otra cuenta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }

        public async Task<Refugio> LoginRefugio(string correo, string password)
        {
            List<Refugio> refugios = await sacarRefugiosAsync();

            foreach (var refugio in refugios)
            {
                if (refugio.correo.Equals(correo) && refugio.password.Equals(password))
                {
                    return refugio;
                }
            }

            return null;
        }

        public async Task<Usuario> LoginUsuario(string correo, string password)
        {
            List<Usuario> usuarios = await sacarUsuariosAsync();

            foreach (var usuario in usuarios)
            {
                if (usuario.correo.Equals(correo) && usuario.password.Equals(password))
                {
                    return usuario;
                }
            }

            return null;
        }

        public async Task<List<Usuario>> ObtenerUsuariosDeRefugio(int idRefugio)
        {
            List<Usuario> usuarios = await sacarUsuariosAsync();
            List<Usuario> users = new List<Usuario>();

            foreach (var usuario in usuarios)
            {
                if (usuario.refugioid == idRefugio)
                {
                    users.Add(usuario);
                }
            }

            return users;
        }


        public async Task ModificarRefugio(Refugio refugioModificado)
        {
            if (await ValidarModificacionRefugio(refugioModificado))
            {
                string apiUrl = $"http://192.168.0.18:8080/refugios/{refugioModificado.id}";

                using (HttpClient httpClient = new HttpClient())
                {
                    try
                    {
                        string jsonContent = JsonConvert.SerializeObject(refugioModificado);
                        StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                        HttpResponseMessage response = await httpClient.PutAsync(apiUrl, content);

                        if (response.IsSuccessStatusCode)
                        {
                            string jsonResponse = await response.Content.ReadAsStringAsync();
                        }
                        else
                        {
                            Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                    }
                }
            }
        }

        public async Task ModificarMascota(Mascota mascotaModificado)
        {
            string apiUrl = $"http://192.168.0.18:8080/mascotas/{mascotaModificado.id}";

            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    string jsonContent = JsonConvert.SerializeObject(mascotaModificado);
                    StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PutAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }

        public async Task ModificarUsuario(Usuario usuarioModificado)
        {
            string apiUrl = $"http://192.168.0.18:8080/usuarios/{usuarioModificado.id}";

            using (HttpClient httpClient = new HttpClient())
            {
                try
                {
                    string jsonContent = JsonConvert.SerializeObject(usuarioModificado);
                    StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage response = await httpClient.PutAsync(apiUrl, content);

                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
        }

        public async Task ModificarAdopcion(Adopcion adopcionMod)
        {
            if (await ValidarModificacionAdopcion(adopcionMod))
            {
                string apiUrl = $"http://192.168.0.18:8080/adopciones/{adopcionMod.id}";

                using (HttpClient httpClient = new HttpClient())
                {
                    try
                    {
                        string jsonContent = JsonConvert.SerializeObject(adopcionMod);
                        StringContent content = new StringContent(jsonContent, System.Text.Encoding.UTF8, "application/json");
                        HttpResponseMessage response = await httpClient.PutAsync(apiUrl, content);

                        if (response.IsSuccessStatusCode)
                        {
                            string jsonResponse = await response.Content.ReadAsStringAsync();
                        }
                        else
                        {
                            Console.WriteLine($"Error en la solicitud: {response.StatusCode}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                    }
                }
            }
        }

        public async Task<bool> ValidarModificacionAdopcion(Adopcion adopcion)
        {
            if (adopcion.iniciofec == null)
            {
                return false;
            }

            List<Adopcion> adopciones = await sacarAdopcionesAsync();
            foreach (var adop in adopciones)
            {
                if (adop.mascotaid == adopcion.mascotaid && adop.userid == adopcion.userid && adop.solicitudfec != adopcion.solicitudfec && adop.iniciofec != null && adop.finfec == null)
                {
                    MessageBox.Show("Esta usuario ya ha adoptado la mascota.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (adopcion.finfec == null && adop.mascotaid == adopcion.mascotaid && adop.iniciofec != null && adop.finfec == null)
                {
                    MessageBox.Show("La mascota ha sido adoptada por otro.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (adop.iniciofec != null && adop.finfec != null)
                {
                    DateTime fechaIni2 = DateTime.Parse(adop.iniciofec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    DateTime fechaFin2 = DateTime.Parse(adop.finfec, null, System.Globalization.DateTimeStyles.RoundtripKind);


                    if (adopcion.iniciofec != null && adopcion.finfec != null)
                    {
                        DateTime fechaIni1 = DateTime.Parse(adopcion.iniciofec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                        DateTime fechaFin1 = DateTime.Parse(adopcion.finfec, null, System.Globalization.DateTimeStyles.RoundtripKind);

                        if (adop.mascotaid == adopcion.mascotaid && adop.iniciofec != null && fechaIni2 > fechaIni1 && fechaFin2 < fechaFin1)
                        {
                            MessageBox.Show("La mascota ha sido adoptada por otro.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }

                    if (adopcion.iniciofec != null)
                    {
                        DateTime fechaIni1 = DateTime.Parse(adopcion.iniciofec, null, System.Globalization.DateTimeStyles.RoundtripKind);

                        if (adop.mascotaid == adopcion.mascotaid && adop.iniciofec != null && fechaFin2 > fechaIni1 && fechaIni2 < fechaIni1)
                        {
                            MessageBox.Show("La mascota ha sido adoptada por otro.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }


                    if (adopcion.finfec != null)
                    {
                        DateTime fechaFin1 = DateTime.Parse(adopcion.finfec, null, System.Globalization.DateTimeStyles.RoundtripKind);

                        if (adop.mascotaid == adopcion.mascotaid && adop.iniciofec != null && fechaFin2 > fechaFin1 && fechaIni2 < fechaFin1)
                        {
                            MessageBox.Show("La mascota ha sido adoptada por otro.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private async Task<bool> ValidarModificacionRefugio(Refugio refugioModificado)
        {
            int max = 0;
            List<Mascota> mascotas = await sacarMascotasAsync();

            foreach (var mascota in mascotas)
            {
                if (mascota.refugioid == refugioModificado.id)
                {
                    max++;
                }
            }

            if (refugioModificado.maximoanimales < max)
            {
                MessageBox.Show("El limite de animales es menor que el numero de animales en plantilla.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public async Task<Refugio> ObtenerRefugio(int idRefugio)
        {
            string apiUrl = $"http://192.168.0.18:8080/refugios/{idRefugio}";

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(apiUrl);

                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    Refugio refugio = JsonConvert.DeserializeObject<Refugio>(jsonResponse);
                    return refugio;
                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                    return null;
                }
            }
        }

        public async Task<Mascota> ObtenerMascota(int idMascota)
        {
            string apiUrl = $"http://192.168.0.18:8080/mascotas/{idMascota}";

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(apiUrl);

                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    Mascota mascota = JsonConvert.DeserializeObject<Mascota>(jsonResponse);
                    return mascota;
                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                    return null;
                }
            }
        }

        public async Task<Usuario> ObtenerUsuario(int idUsuario)
        {
            string apiUrl = $"http://192.168.0.18:8080/usuarios/{idUsuario}";

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(apiUrl);

                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    Usuario usuario = JsonConvert.DeserializeObject<Usuario>(jsonResponse);
                    return usuario;
                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                    return null;
                }
            }
        }

        public async Task<Adopcion> ObtenerAdopcion(int idAdopcion)
        {
            string apiUrl = $"http://192.168.0.18:8080/adopciones/{idAdopcion}";

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(apiUrl);

                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                    Adopcion adopcion = JsonConvert.DeserializeObject<Adopcion>(jsonResponse);
                    return adopcion;
                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                    return null;
                }
            }
        }

        public async Task<List<Mascota>> ObtenerMascotasDeRefugioAdoptadas(int idRefugio)
        {
            List<Mascota> lista = new List<Mascota>();
            List<Mascota> mascotas = await sacarMascotasAsync();

            foreach (var mascota in mascotas)
            {
                List<Adopcion> adopciones = await sacarAdopcionesAsync();

                foreach (var adopcion in adopciones)
                {
                    if (mascota.refugioid == idRefugio && adopcion.mascotaid == mascota.id && adopcion.iniciofec != null && adopcion.finfec == null)
                    {
                        lista.Add(mascota);
                    }
                }
            }

            return lista;
        }

        public async Task<List<Mascota>> ObtenerMascotasDeRefugioSinAdoptar(int idRefugio)
        {
            List<Mascota> lista = new List<Mascota>();
            List<Mascota> mascotas = await sacarMascotasAsync();

            foreach (var mascota in mascotas)
            {
                List<Adopcion> adopciones = await sacarAdopcionesAsync();

                if (mascota.refugioid == idRefugio)
                {
                    lista.Add(mascota);
                }

                foreach (var adopcion in adopciones)
                {
                    if (adopcion.mascotaid == mascota.id && adopcion.iniciofec != null && adopcion.finfec == null)
                    {
                        lista.Remove(mascota);
                    }
                }
            }

            return lista;
        }

        public async Task EliminarMascota(int idMascota)
        {
            string apiUrl = $"http://192.168.0.18:8080/mascotas/{idMascota}";

            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.DeleteAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    string jsonResponse = await response.Content.ReadAsStringAsync();
                }
                else
                {
                    Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
        }

        public async Task EliminarCuidador(int idUsuario)
        {
            if (await ValidarEliminarCuidador(idUsuario))
            {
                string apiUrl = $"http://192.168.0.18:8080/usuarios/{idUsuario}";

                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.DeleteAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                    }
                }
            }
        }

        private async Task<bool> ValidarEliminarCuidador(int idCuidador)
        {
            Usuario user = await ObtenerUsuario(idCuidador);
            Refugio refu = await ObtenerRefugio((int)user.refugioid);

            if (refu.responsableid == idCuidador)
            {
                MessageBox.Show("No se puede eliminar al responsable del refugio.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        public async Task<List<Adopcion>> ObtenerAdopcionesConfirmadas(int idRefugio)
        {
            List<Adopcion> lista = new List<Adopcion>();
            List<Mascota> mascotas = await sacarMascotasAsync();

            foreach (var mascota in mascotas)
            {
                if (mascota.refugioid == idRefugio)
                {
                    List<Adopcion> adopciones = await sacarAdopcionesAsync();

                    foreach (var adopcion in adopciones)
                    {
                        if (adopcion.mascotaid == mascota.id && adopcion.iniciofec != null)
                        {
                            lista.Add(adopcion);
                        }
                    }
                }
            }

            return lista;
        }

        public async Task<List<Adopcion>> ObtenerAdopcionesPendientes(int idRefugio)
        {
            List<Adopcion> lista = new List<Adopcion>();
            List<Mascota> mascotas = await sacarMascotasAsync();

            foreach (var mascota in mascotas)
            {
                if (mascota.refugioid == idRefugio)
                {
                    List<Adopcion> adopciones = await sacarAdopcionesAsync();

                    foreach (var adopcion in adopciones)
                    {
                        if (adopcion.mascotaid == mascota.id && adopcion.iniciofec == null)
                        {
                            lista.Add(adopcion);
                        }
                    }
                }
            }

            return lista;
        }

        public async Task<string> ObtenerNombreUsuarioPorId(int idUsuario)
        {
            Usuario user = await ObtenerUsuario(idUsuario);
            return user.nombre;
        }

        public async Task<string> ObtenerNombreMascotaPorId(int idMascota)
        {
            Mascota mas = await ObtenerMascota(idMascota);
            return mas.nombre;
        }

        public async Task EliminarAdopcion(int id)
        {
            if (await ValidarEliminarSolicitud(id))
            {
                string apiUrl = $"http://192.168.0.18:8080/adopciones/{id}";

                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.DeleteAsync(apiUrl);
                    if (response.IsSuccessStatusCode)
                    {
                        string jsonResponse = await response.Content.ReadAsStringAsync();
                    }
                    else
                    {
                        Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                    }
                }
            }
        }

        private async Task<bool> ValidarEliminarSolicitud(int id)
        {
            List<Adopcion> adopciones = await sacarAdopcionesAsync();
            foreach (var adop in adopciones)
            {
                if (adop.id == id && adop.iniciofec != null)
                {
                    MessageBox.Show("No se puede eliminar un adopcion en curso.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }

        public async Task<string> ObtenerNombreRefugioPorId(int idRefugio)
        {
            Refugio refu = await ObtenerRefugio(idRefugio);
            return refu.nombre;
        }

        public async Task<int> ObtenerNumeroDeMascotasDeRefugio(int idRefugio)
        {
            int num = 0;
            List<Mascota> mascotas = await sacarMascotasAsync();

            foreach (var mascota in mascotas)
            {
                if (mascota.refugioid == idRefugio)
                {
                    num++;
                }
            }

            return num;
        }

        public async Task<List<Mascota>> ObtenerMascotasSinAdoptar()
        {
            List<Mascota> mascotas = await sacarMascotasAsync();
            List<Mascota> lista = mascotas.ToList();

            foreach (var mascota in mascotas)
            {
                List<Adopcion> adopciones = await sacarAdopcionesAsync();

                foreach (var adopcion in adopciones)
                {
                    if (adopcion.mascotaid == mascota.id && adopcion.iniciofec != null && adopcion.finfec == null)
                    {
                        lista.Remove(mascota);
                    }
                }
            }

            return lista;
        }

        public async Task<List<Usuario>> ObtenerContactos(int idUsuario)
        {
            List<Mensaje> mensajes = await sacarMensajesAsync();
            List<Usuario> lista = new List<Usuario>();

            foreach (var men in mensajes)
            {
                if (men.emisorid == idUsuario)
                {
                    Usuario use = await ObtenerUsuario(men.receptorid);
                    lista.Add(use);
                }

                if (men.receptorid == idUsuario)
                {
                    Usuario use = await ObtenerUsuario(men.emisorid);
                    lista.Add(use);
                }
            }

            List<Usuario> listaSinDuplicados = new List<Usuario>();
            HashSet<int> idsVistos = new HashSet<int>();

            foreach (Usuario usuario in lista)
            {
                if (idsVistos.Add(usuario.id))
                {
                    listaSinDuplicados.Add(usuario);
                }
            }

            return listaSinDuplicados;
        }

        public async Task<List<Mensaje>> ObtenerMensajesDeContacto(int id1, int id2)
        {
            List<Mensaje> lista = new List<Mensaje>();
            List<Mensaje> mensajes = await sacarMensajesAsync();

            foreach (var men in mensajes)
            {
                if ((men.emisorid == id1 && men.receptorid == id2) || (men.emisorid == id2 && men.receptorid == id1))
                {
                    lista.Add(men);
                }
            }

            return lista;
        }

        public async Task<Usuario> ObtenerUsuarioPorCorreo(string correo)
        {
            List<Usuario> usuarios = await sacarUsuariosAsync();
            foreach (var user in usuarios)
            {
                if (user.correo == correo)
                {
                    return user;
                }
            }
            return null;
        }
        public async Task<Refugio> ObtenerRefugioPorCorreo(string correo)
        {
            List<Refugio> refugios = await sacarRefugiosAsync();
            foreach (var refu in refugios)
            {
                if (refu.correo == correo)
                {
                    return refu;
                }
            }
            return null;
        }

        public async Task<List<Adopcion>> ObtenerAdopcionesDeUsuario(int idUsuario)
        {
            List<Adopcion> lista = new List<Adopcion>();
            List<Adopcion> adopciones = await sacarAdopcionesAsync();

            foreach (var adopcion in adopciones)
            {
                if (adopcion.userid == idUsuario)
                {
                    lista.Add(adopcion);
                }
            }

            return lista;
        }
    }
}
