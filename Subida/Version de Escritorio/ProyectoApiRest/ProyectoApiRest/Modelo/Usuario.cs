﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoApiRest.Modelo
{
    public class Usuario
    {
        public int id { get; set; }
        public Nullable<int> refugioid { get; set; }
        public string nombre { get; set; }
        public string nacimiento { get; set; }
        public string telefono { get; set; }
        public string correo { get; set; }
        public string password { get; set; }
        public string direccion { get; set; }
        public byte[] imagen { get; set; }
    }
}
