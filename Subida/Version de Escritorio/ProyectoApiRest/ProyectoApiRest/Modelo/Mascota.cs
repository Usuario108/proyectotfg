﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoApiRest.Modelo
{
    public class Mascota
    {
        public int id { get; set; }
        public int refugioid { get; set; }
        public string nombre { get; set; }
        public string nacimiento { get; set; }
        public string raza { get; set; }
        public float peso { get; set; }
        public string descripcion { get; set; }
        public byte[] imagen { get; set; }
    }
}
