﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoApiRest.Modelo
{
    public class Refugio
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string correo { get; set; }
        public string password { get; set; }
        public int maximoanimales { get; set; }
        public int responsableid { get; set; }
        public byte[] imagen { get; set; }
    }
}
