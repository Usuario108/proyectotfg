﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoApiRest.Modelo
{
    public class Mensaje
    {
        public int id { get; set; }
        public int emisorid { get; set; }
        public int receptorid { get; set; }
        public string hora { get; set; }
        public string mensaje { get; set; }
    }
}
