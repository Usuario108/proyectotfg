﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoApiRest.Modelo
{
    public class Adopcion
    {
        public int id { get; set; }
        public int userid { get; set; }
        public int mascotaid { get; set; }
        public string solicitudfec { get; set; }
        public string iniciofec { get; set; }
        public string finfec { get; set; }
    }
}
