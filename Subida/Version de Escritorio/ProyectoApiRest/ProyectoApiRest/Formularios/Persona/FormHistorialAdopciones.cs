﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona
{
    public partial class frmHistorialAdopciones : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        private Adopcion _adopcion;

        public frmHistorialAdopciones(Negocio n, Usuario _user)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = _user;
            txtDescripcion.ReadOnly = true;
            txtNombre.ReadOnly = true;
            txtPeso.ReadOnly = true;
            dtpInicio.Enabled = false;
            dtpFin.Enabled = false;
            btnFoto.Enabled = false;
            btnAceptar.Enabled = false;
            dtpInicio.Format = DateTimePickerFormat.Custom;
            dtpInicio.CustomFormat = " ";
            dtpFin.Format = DateTimePickerFormat.Custom;
            dtpFin.CustomFormat = " ";
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Adopcion> lista = await _negocio.ObtenerAdopcionesDeUsuario(_usuario.id);

            lvwAdopciones.Items.Clear();
            foreach (Adopcion sol in lista)
            {
                string ini = "Adopcion No Confirmada";
                if (sol.iniciofec != null)
                {
                    ini = sol.iniciofec.ToString();
                }
                string fin = "Adopcion Actual";
                if (sol.finfec != null)
                {
                    fin = sol.finfec.ToString();
                }

                Mascota mas = await _negocio.ObtenerMascota(sol.mascotaid);
                ListViewItem item = new ListViewItem(new string[] { mas.nombre, ini, fin });
                item.Tag = sol.id;
                lvwAdopciones.Items.Add(item);
            }
        }

        private async void lvwAdopciones_DoubleClick(object sender, EventArgs e)
        {
            if (lvwAdopciones.SelectedItems.Count == 1)
            {
                dtpInicio.Format = DateTimePickerFormat.Custom;
                dtpInicio.CustomFormat = " ";
                dtpFin.Format = DateTimePickerFormat.Custom;
                dtpFin.CustomFormat = " ";

                int id = (int)lvwAdopciones.SelectedItems[0].Tag;
                _adopcion = await _negocio.ObtenerAdopcion(id);
                Mascota mas = await _negocio.ObtenerMascota(_adopcion.mascotaid);

                txtNombre.Text = mas.nombre;
                txtDescripcion.Text = mas.descripcion;
                txtPeso.Text = mas.peso.ToString();
                picFoto.Image = _negocio.ByteToImage(mas.imagen);

                if (_adopcion.iniciofec != null)
                {
                    dtpInicio.Format = DateTimePickerFormat.Custom;
                    dtpInicio.CustomFormat = "dd/MM/yyyy";
                    DateTime fechaInicio = DateTime.Parse(_adopcion.iniciofec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    dtpInicio.Value = fechaInicio;
                }
                if (_adopcion.finfec != null)
                {
                    dtpFin.Format = DateTimePickerFormat.Custom;
                    dtpFin.CustomFormat = "dd/MM/yyyy";
                    DateTime fechaFin = DateTime.Parse(_adopcion.finfec,null,System.Globalization.DateTimeStyles.RoundtripKind);
                    dtpFin.Value = fechaFin;
                }

                if (_adopcion.iniciofec != null && _adopcion.finfec == null)
                {
                    txtDescripcion.ReadOnly = false;
                    txtNombre.ReadOnly = false;
                    txtPeso.ReadOnly = false;
                    btnFoto.Enabled = true;
                    btnAceptar.Enabled = true;
                }
                else
                {
                    txtDescripcion.ReadOnly = true;
                    txtNombre.ReadOnly = true;
                    txtPeso.ReadOnly = true;
                    btnFoto.Enabled = false;
                    btnAceptar.Enabled = false;
                }
            }
        }

        private void lvwAdopciones_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwAdopciones.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwAdopciones.Sort();
        }

        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult rs = ofd.ShowDialog();
            if (rs == DialogResult.OK)
            {
                picFoto.Image = Image.FromFile(ofd.FileName);
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtPeso.Text))
            {
                MessageBox.Show("El peso no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario() && _adopcion != null)
            {
                int id = (int)lvwAdopciones.SelectedItems[0].Tag;
                Adopcion adop = await _negocio.ObtenerAdopcion(id);
                Mascota mas = await _negocio.ObtenerMascota(adop.mascotaid);

                mas.nombre = txtNombre.Text;
                mas.peso = Convert.ToSingle(txtPeso.Text);
                mas.descripcion = txtDescripcion.Text;
                mas.imagen = _negocio.ImageToByteArray(picFoto.Image);

                await _negocio.ModificarMascota(mas);
            }
        }
    }
}
