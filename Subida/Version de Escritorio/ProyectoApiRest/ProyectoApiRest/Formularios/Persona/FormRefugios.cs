﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona
{
    public partial class frmRefugios : Form
    {
        private Negocio _negocio;

        private Refugio _refugio;

        public frmRefugios(Negocio n)
        {
            InitializeComponent();
            _negocio = n;
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Refugio> lista = await _negocio.sacarRefugiosAsync();

            lvwRefugios.Items.Clear();
            foreach (Refugio re in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { re.nombre, re.correo });
                item.Tag = re.id;

                lvwRefugios.Items.Add(item);
            }
        }

        private void lvwRefugios_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwRefugios.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwRefugios.Sort();
        }

        private async void lvwRefugios_DoubleClick(object sender, EventArgs e)
        {
            if (lvwRefugios.SelectedItems.Count == 1)
            {
                int id = (int)lvwRefugios.SelectedItems[0].Tag;
                _refugio = await _negocio.ObtenerRefugio(id);

                txtNombre.Text = _refugio.nombre;
                txtTelefono.Text = _refugio.telefono;
                txtDireccion.Text = _refugio.direccion;
                int num = await _negocio.ObtenerNumeroDeMascotasDeRefugio(id);
                txtMascotas.Text = num.ToString();
                Usuario user = await _negocio.ObtenerUsuario(_refugio.responsableid);
                txtResponsable.Text = user.correo;
                picFoto.Image = _negocio.ByteToImage(_refugio.imagen);
            }
        }
    }
}
