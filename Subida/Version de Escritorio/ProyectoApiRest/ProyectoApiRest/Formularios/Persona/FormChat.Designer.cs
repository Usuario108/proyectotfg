﻿namespace ProyectoApiRest.Formularios.Persona
{
    partial class frmChat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lstMensaje = new System.Windows.Forms.ListBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.txtMensaje = new System.Windows.Forms.TextBox();
            this.lvwChat = new System.Windows.Forms.ListView();
            this.columnNombre = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmsAnadir = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmiAgregar = new System.Windows.Forms.ToolStripMenuItem();
            this.cmsAnadir.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstMensaje
            // 
            this.lstMensaje.FormattingEnabled = true;
            this.lstMensaje.HorizontalScrollbar = true;
            this.lstMensaje.Location = new System.Drawing.Point(182, 12);
            this.lstMensaje.Name = "lstMensaje";
            this.lstMensaje.Size = new System.Drawing.Size(324, 277);
            this.lstMensaje.TabIndex = 121;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(431, 310);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(75, 23);
            this.btnEnviar.TabIndex = 120;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // txtMensaje
            // 
            this.txtMensaje.Location = new System.Drawing.Point(182, 310);
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.Size = new System.Drawing.Size(243, 20);
            this.txtMensaje.TabIndex = 119;
            // 
            // lvwChat
            // 
            this.lvwChat.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnNombre});
            this.lvwChat.ContextMenuStrip = this.cmsAnadir;
            this.lvwChat.Dock = System.Windows.Forms.DockStyle.Left;
            this.lvwChat.FullRowSelect = true;
            this.lvwChat.GridLines = true;
            this.lvwChat.HideSelection = false;
            this.lvwChat.Location = new System.Drawing.Point(0, 0);
            this.lvwChat.MultiSelect = false;
            this.lvwChat.Name = "lvwChat";
            this.lvwChat.Size = new System.Drawing.Size(150, 361);
            this.lvwChat.TabIndex = 118;
            this.lvwChat.UseCompatibleStateImageBehavior = false;
            this.lvwChat.View = System.Windows.Forms.View.Details;
            this.lvwChat.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvwChat_ColumnClick);
            this.lvwChat.DoubleClick += new System.EventHandler(this.lvwChat_DoubleClick);
            // 
            // columnNombre
            // 
            this.columnNombre.Text = "Contactos";
            this.columnNombre.Width = 141;
            // 
            // cmsAnadir
            // 
            this.cmsAnadir.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiAgregar});
            this.cmsAnadir.Name = "cmsAnadir";
            this.cmsAnadir.Size = new System.Drawing.Size(169, 26);
            // 
            // tsmiAgregar
            // 
            this.tsmiAgregar.Image = global::ProyectoApiRest.Properties.Resources.action_add_16xMD;
            this.tsmiAgregar.Name = "tsmiAgregar";
            this.tsmiAgregar.Size = new System.Drawing.Size(180, 22);
            this.tsmiAgregar.Text = "Agregar Contacto";
            this.tsmiAgregar.Click += new System.EventHandler(this.tsmiAgregar_Click);
            // 
            // frmChat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 361);
            this.Controls.Add(this.lstMensaje);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.txtMensaje);
            this.Controls.Add(this.lvwChat);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmChat";
            this.Text = "TuOjitoDerecho";
            this.cmsAnadir.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstMensaje;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.TextBox txtMensaje;
        private System.Windows.Forms.ListView lvwChat;
        private System.Windows.Forms.ColumnHeader columnNombre;
        private System.Windows.Forms.ContextMenuStrip cmsAnadir;
        private System.Windows.Forms.ToolStripMenuItem tsmiAgregar;
    }
}