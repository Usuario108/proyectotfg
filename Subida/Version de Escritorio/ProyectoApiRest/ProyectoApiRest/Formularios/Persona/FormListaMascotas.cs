﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona
{
    public partial class frmListaMascotas : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        private Mascota _mascota;

        public frmListaMascotas(Negocio n, Usuario user)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = user;
            dtpFechaNacimiento.Enabled = false;
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Mascota> lista = await _negocio.ObtenerMascotasSinAdoptar();

            lvwMascotas.Items.Clear();
            foreach (Mascota m in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { m.nombre, m.raza, await _negocio.ObtenerNombreRefugioPorId(m.refugioid) });
                item.Tag = m.id;
                lvwMascotas.Items.Add(item);
            }
        }

        private async void lvwMascotas_DoubleClick(object sender, EventArgs e)
        {
            if (lvwMascotas.SelectedItems.Count == 1)
            {
                int id = (int)lvwMascotas.SelectedItems[0].Tag;
                _mascota = await _negocio.ObtenerMascota(id);
                Refugio refu = await _negocio.ObtenerRefugio(_mascota.refugioid);

                txtNombre.Text = _mascota.nombre;
                txtRaza.Text = _mascota.raza;
                txtRefugio.Text = refu.nombre;
                Usuario user = await _negocio.ObtenerUsuario(refu.responsableid);
                txtResponsable.Text = user.correo;
                DateTime fechaHora = DateTime.Parse(_mascota.nacimiento, null, System.Globalization.DateTimeStyles.RoundtripKind);
                dtpFechaNacimiento.Value = fechaHora;
                txtDescripcion.Text = _mascota.descripcion;
                picFoto.Image = _negocio.ByteToImage(_mascota.imagen);
            }
        }

        private void lvwMascotas_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwMascotas.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwMascotas.Sort();
        }

        private async void btnSolicitud_Click(object sender, EventArgs e)
        {
            if (lvwMascotas.SelectedItems.Count == 1)
            {
                Adopcion adop = new Adopcion();
                adop.userid = _usuario.id;
                adop.mascotaid = (int)lvwMascotas.SelectedItems[0].Tag;
                DateTime fechaParseada = DateTime.ParseExact(DateTime.Now.ToShortDateString(), "dd/MM/yyyy", null);
                adop.solicitudfec = fechaParseada.ToString("yyyy-MM-dd");
                adop.iniciofec = null;
                adop.finfec = null;

                if (await _negocio.ValidarNuevaAdopcion(adop))
                {
                    await _negocio.InsertarSolicitud(adop);
                    MessageBox.Show("Solicitud enviada.", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Para mandar una solicitud debe seleccionar una mascota.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
