﻿namespace ProyectoApiRest.Formularios.Persona.Dueño
{
    partial class frmDueñoLogueado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDueñoLogueado));
            this.pnlFormulario = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnAdopciones = new System.Windows.Forms.Button();
            this.btnMensajes = new System.Windows.Forms.Button();
            this.btnAnimales = new System.Windows.Forms.Button();
            this.btnRefugios = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.pnlLogo = new System.Windows.Forms.Panel();
            this.picImagen = new System.Windows.Forms.PictureBox();
            this.pnlMenu.SuspendLayout();
            this.pnlLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFormulario
            // 
            this.pnlFormulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFormulario.Location = new System.Drawing.Point(150, 0);
            this.pnlFormulario.Name = "pnlFormulario";
            this.pnlFormulario.Size = new System.Drawing.Size(534, 411);
            this.pnlFormulario.TabIndex = 9;
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.pnlMenu.Controls.Add(this.btnAdopciones);
            this.pnlMenu.Controls.Add(this.btnMensajes);
            this.pnlMenu.Controls.Add(this.btnAnimales);
            this.pnlMenu.Controls.Add(this.btnRefugios);
            this.pnlMenu.Controls.Add(this.btnSalir);
            this.pnlMenu.Controls.Add(this.pnlLogo);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(150, 411);
            this.pnlMenu.TabIndex = 8;
            // 
            // btnAdopciones
            // 
            this.btnAdopciones.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdopciones.FlatAppearance.BorderSize = 0;
            this.btnAdopciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdopciones.Location = new System.Drawing.Point(0, 169);
            this.btnAdopciones.Name = "btnAdopciones";
            this.btnAdopciones.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnAdopciones.Size = new System.Drawing.Size(150, 23);
            this.btnAdopciones.TabIndex = 11;
            this.btnAdopciones.Text = "Adopciones";
            this.btnAdopciones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdopciones.UseVisualStyleBackColor = true;
            this.btnAdopciones.Click += new System.EventHandler(this.btnAdopciones_Click);
            // 
            // btnMensajes
            // 
            this.btnMensajes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMensajes.FlatAppearance.BorderSize = 0;
            this.btnMensajes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMensajes.Location = new System.Drawing.Point(0, 146);
            this.btnMensajes.Name = "btnMensajes";
            this.btnMensajes.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnMensajes.Size = new System.Drawing.Size(150, 23);
            this.btnMensajes.TabIndex = 10;
            this.btnMensajes.Text = "Chat";
            this.btnMensajes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMensajes.UseVisualStyleBackColor = true;
            this.btnMensajes.Click += new System.EventHandler(this.btnMensajes_Click);
            // 
            // btnAnimales
            // 
            this.btnAnimales.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAnimales.FlatAppearance.BorderSize = 0;
            this.btnAnimales.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnimales.Location = new System.Drawing.Point(0, 123);
            this.btnAnimales.Name = "btnAnimales";
            this.btnAnimales.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnAnimales.Size = new System.Drawing.Size(150, 23);
            this.btnAnimales.TabIndex = 9;
            this.btnAnimales.Text = "Animales";
            this.btnAnimales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnimales.UseVisualStyleBackColor = true;
            this.btnAnimales.Click += new System.EventHandler(this.btnAnimales_Click);
            // 
            // btnRefugios
            // 
            this.btnRefugios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRefugios.FlatAppearance.BorderSize = 0;
            this.btnRefugios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefugios.Location = new System.Drawing.Point(0, 100);
            this.btnRefugios.Name = "btnRefugios";
            this.btnRefugios.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnRefugios.Size = new System.Drawing.Size(150, 23);
            this.btnRefugios.TabIndex = 8;
            this.btnRefugios.Text = "Refugios";
            this.btnRefugios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRefugios.UseVisualStyleBackColor = true;
            this.btnRefugios.Click += new System.EventHandler(this.btnRefugios_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnSalir.Location = new System.Drawing.Point(0, 388);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnSalir.Size = new System.Drawing.Size(150, 23);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Cerrar Sesion";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // pnlLogo
            // 
            this.pnlLogo.Controls.Add(this.picImagen);
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Margin = new System.Windows.Forms.Padding(5);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(150, 100);
            this.pnlLogo.TabIndex = 0;
            // 
            // picImagen
            // 
            this.picImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picImagen.Location = new System.Drawing.Point(0, 0);
            this.picImagen.Name = "picImagen";
            this.picImagen.Size = new System.Drawing.Size(150, 100);
            this.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImagen.TabIndex = 0;
            this.picImagen.TabStop = false;
            this.picImagen.Click += new System.EventHandler(this.picImagen_Click);
            this.picImagen.MouseEnter += new System.EventHandler(this.picImagen_MouseEnter);
            // 
            // frmDueñoLogueado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.pnlFormulario);
            this.Controls.Add(this.pnlMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDueñoLogueado";
            this.Text = "TuOjitoDerecho";
            this.pnlMenu.ResumeLayout(false);
            this.pnlLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFormulario;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnAdopciones;
        private System.Windows.Forms.Button btnMensajes;
        private System.Windows.Forms.Button btnAnimales;
        private System.Windows.Forms.Button btnRefugios;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Panel pnlLogo;
        private System.Windows.Forms.PictureBox picImagen;
    }
}