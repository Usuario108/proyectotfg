﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona.Dueño
{
    public partial class frmDueñoLogueado : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        public frmDueñoLogueado(Negocio n, Usuario user)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = user;
            picImagen.Image = _negocio.ByteToImage(_usuario.imagen);
            AbrirFormularioHijo(new frmPerfilUsuario(_negocio, _usuario));
        }

        private void picImagen_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmPerfilUsuario(_negocio, _usuario));
        }

        private void btnRefugios_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmRefugios(_negocio));
        }

        private void btnAnimales_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaMascotas(_negocio, _usuario));
        }

        private void btnMensajes_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmChat(_negocio, _usuario));
        }

        private void btnAdopciones_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmHistorialAdopciones(_negocio, _usuario));
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void picImagen_MouseEnter(object sender, EventArgs e)
        {
            picImagen.Image = _negocio.ByteToImage(_usuario.imagen);
        }

        private Form frmActivo = null;
        private void AbrirFormularioHijo(Form formHijo)
        {
            if (frmActivo != null)
            {
                frmActivo.Close();
            }

            frmActivo = formHijo;
            formHijo.TopLevel = false;
            formHijo.FormBorderStyle = FormBorderStyle.None;
            formHijo.Dock = DockStyle.Fill;
            pnlFormulario.Controls.Add(formHijo);
            pnlFormulario.Tag = formHijo;
            formHijo.BringToFront();
            formHijo.Show();
        }
    }
}
