﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona
{
    public partial class frmPerfilUsuario : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        public frmPerfilUsuario(Negocio n, Usuario user)
        {
            InitializeComponent();
            _usuario = user;
            _negocio = n;

            txtCorreo.Text = _usuario.correo;
            txtNombre.Text = _usuario.nombre;
            txtTelefono.Text = _usuario.telefono;
            txtDireccion.Text = _usuario.direccion;
            DateTime fecha = DateTime.Parse(_usuario.nacimiento, null, System.Globalization.DateTimeStyles.RoundtripKind);
            dtpNacimiento.Value = fecha;
            picFoto.Image = _negocio.ByteToImage(_usuario.imagen);

            lblRefugio.Visible = false;
            txtRefugio.Visible = false;
            if (_usuario.refugioid != null)
            {
                lblRefugio.Visible = true;
                txtRefugio.Visible = true;

                CargarRefugio();
            }
        }

        public async void CargarRefugio()
        {
            txtRefugio.Text = await _negocio.ObtenerNombreRefugioPorId((int)_usuario.refugioid);
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult rs = ofd.ShowDialog();
            if (rs == DialogResult.OK)
            {
                picFoto.Image = Image.FromFile(ofd.FileName);
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtTelefono.Text))
            {
                MessageBox.Show("El telefono no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtDireccion.Text))
            {
                MessageBox.Show("La direccion no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpNacimiento.Value >= DateTime.Now)
            {
                MessageBox.Show("La fecha no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            int difFechas = DateTime.Now.Year - dtpNacimiento.Value.Year;
            if (difFechas < 18)
            {
                MessageBox.Show("No puede registrarse menores de 18 años.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                _usuario.nombre = txtNombre.Text;
                _usuario.direccion = txtDireccion.Text;
                _usuario.telefono = txtTelefono.Text;
                DateTime fechaParseada = DateTime.ParseExact(dtpNacimiento.Value.ToShortDateString(), "dd/MM/yyyy", null);
                _usuario.nacimiento = fechaParseada.ToString("yyyy-MM-dd");
                _usuario.imagen = _negocio.ImageToByteArray(picFoto.Image);

                await _negocio.ModificarUsuario(_usuario);
            }
        }
    }
}
