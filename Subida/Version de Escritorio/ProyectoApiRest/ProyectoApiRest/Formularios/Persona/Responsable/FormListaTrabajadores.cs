﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Formularios.Protectora;
using ProyectoApiRest.Modelo;
using ProyectoApiRest.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona.Responsable
{
    public partial class frmListaTrabajadores : Form
    {
        private Negocio _negocio;

        private int _idUsuario;

        private Usuario _usuario;

        public frmListaTrabajadores(Negocio n, int idUsuario)
        {
            InitializeComponent();
            _negocio = n;
            _idUsuario = idUsuario;
            CargarLista();
        }

        private async void CargarLista()
        {
            lvwTrabajadores.Items.Clear();
            Usuario usAux = await _negocio.ObtenerUsuario(_idUsuario);
            List<Usuario> lista = await _negocio.ObtenerUsuariosDeRefugio((int)usAux.refugioid);

            foreach (Usuario u in lista)
            {
                if (u.id != usAux.id)
                {
                    ListViewItem item = new ListViewItem(new string[] { u.nombre, u.telefono, u.correo, u.direccion });
                    item.Tag = u.id;
                    lvwTrabajadores.Items.Add(item);
                }
            }
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private async void lvwTrabajadores_DoubleClick(object sender, EventArgs e)
        {
            if (lvwTrabajadores.SelectedItems.Count == 1)
            {
                int id = (int)lvwTrabajadores.SelectedItems[0].Tag;
                _usuario = await _negocio.ObtenerUsuario(id);
                txtCorreo.Text = _usuario.correo;
                txtNombre.Text = _usuario.nombre;
                txtDireccion.Text = _usuario.direccion;
                txtTelefono.Text = _usuario.telefono;
                DateTime fechaHora = DateTime.Parse(_usuario.nacimiento, null, System.Globalization.DateTimeStyles.RoundtripKind);
                dtpFechaNacimiento.Value = fechaHora;
                picFoto.Image = _negocio.ByteToImage(_usuario.imagen);
            }
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult rs = ofd.ShowDialog();
            if (rs == DialogResult.OK)
            {
                picFoto.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void cmsTrabajadores_Opening(object sender, CancelEventArgs e)
        {
            tsmiEliminar.Enabled = false;
            if (lvwTrabajadores.SelectedItems.Count == 1)
            {
                tsmiEliminar.Enabled = true;
            }
        }

        private void lvwTrabajadores_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwTrabajadores.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwTrabajadores.Sort();
        }

        private async void tsmiNuevo_Click(object sender, EventArgs e)
        {
            Usuario cuidador = new Usuario();
            frmNuevoCuidador form = new frmNuevoCuidador(_negocio, cuidador);
            DialogResult dg = form.ShowDialog();

            if (dg == DialogResult.OK)
            {
                Usuario us = await _negocio.ObtenerUsuario(_idUsuario);
                cuidador.refugioid = us.refugioid;
                await _negocio.InsertarUsuario(cuidador);
                CargarLista();
            }
        }

        private async void tsmiEliminar_Click(object sender, EventArgs e)
        {
            int idCuidador = (int)lvwTrabajadores.SelectedItems[0].Tag;
            DialogResult dg = MessageBox.Show("¿Esta seguro que desea eliminar este cuidador?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (dg == DialogResult.OK)
            {
                await _negocio.EliminarCuidador(idCuidador);
                CargarLista();

                txtNombre.Text = null;
                txtCorreo.Text = null;
                txtTelefono.Text = null;
                txtDireccion.Text = null;
                dtpFechaNacimiento.Value = DateTime.Now;
                picFoto.Image = Resources.im_usuario;
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtDireccion.Text))
            {
                MessageBox.Show("La direccion no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtTelefono.Text))
            {
                MessageBox.Show("El telefono no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpFechaNacimiento.Value >= DateTime.Now)
            {
                MessageBox.Show("La fecha no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            int difFechas = DateTime.Now.Year - dtpFechaNacimiento.Value.Year;
            if (difFechas < 18)
            {
                MessageBox.Show("No puede registrarse menores de 18 años.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario() && _usuario != null)
            {
                _usuario.nombre = txtNombre.Text;
                DateTime fechaParseada = DateTime.ParseExact(dtpFechaNacimiento.Value.ToShortDateString(), "dd/MM/yyyy", null);
                _usuario.nacimiento = fechaParseada.ToString("yyyy-MM-dd");
                _usuario.direccion = txtDireccion.Text;
                _usuario.telefono = txtTelefono.Text;
                _usuario.imagen = null;
                _usuario.imagen = _negocio.ImageToByteArray(picFoto.Image);

                await _negocio.ModificarUsuario(_usuario);
                CargarLista();

                txtNombre.Text = null;
                txtCorreo.Text = null;
                txtTelefono.Text = null;
                txtDireccion.Text = null;
                dtpFechaNacimiento.Value = DateTime.Now;
                picFoto.Image = Resources.im_usuario;
            }
        }
    }
}
