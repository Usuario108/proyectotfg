﻿namespace ProyectoApiRest.Formularios.Persona.Responsable
{
    partial class frmResponsableLogueado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmResponsableLogueado));
            this.pnlFormulario = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.pnlAdopciones = new System.Windows.Forms.Panel();
            this.btnPropias = new System.Windows.Forms.Button();
            this.btnAceptadas = new System.Windows.Forms.Button();
            this.btnPendientes = new System.Windows.Forms.Button();
            this.btnAdopciones = new System.Windows.Forms.Button();
            this.btnMensajes = new System.Windows.Forms.Button();
            this.btnCuidadores = new System.Windows.Forms.Button();
            this.pnlAnimales = new System.Windows.Forms.Panel();
            this.btnOtroRefugio = new System.Windows.Forms.Button();
            this.btnSinAdoptar = new System.Windows.Forms.Button();
            this.btnAdoptados = new System.Windows.Forms.Button();
            this.btnAnimales = new System.Windows.Forms.Button();
            this.btnRefugios = new System.Windows.Forms.Button();
            this.btnSalir = new System.Windows.Forms.Button();
            this.pnlLogo = new System.Windows.Forms.Panel();
            this.picImagen = new System.Windows.Forms.PictureBox();
            this.pnlMenu.SuspendLayout();
            this.pnlAdopciones.SuspendLayout();
            this.pnlAnimales.SuspendLayout();
            this.pnlLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFormulario
            // 
            this.pnlFormulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFormulario.Location = new System.Drawing.Point(150, 0);
            this.pnlFormulario.Name = "pnlFormulario";
            this.pnlFormulario.Size = new System.Drawing.Size(534, 411);
            this.pnlFormulario.TabIndex = 12;
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.pnlMenu.Controls.Add(this.pnlAdopciones);
            this.pnlMenu.Controls.Add(this.btnAdopciones);
            this.pnlMenu.Controls.Add(this.btnMensajes);
            this.pnlMenu.Controls.Add(this.btnCuidadores);
            this.pnlMenu.Controls.Add(this.pnlAnimales);
            this.pnlMenu.Controls.Add(this.btnAnimales);
            this.pnlMenu.Controls.Add(this.btnRefugios);
            this.pnlMenu.Controls.Add(this.btnSalir);
            this.pnlMenu.Controls.Add(this.pnlLogo);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(150, 411);
            this.pnlMenu.TabIndex = 11;
            // 
            // pnlAdopciones
            // 
            this.pnlAdopciones.Controls.Add(this.btnPropias);
            this.pnlAdopciones.Controls.Add(this.btnAceptadas);
            this.pnlAdopciones.Controls.Add(this.btnPendientes);
            this.pnlAdopciones.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAdopciones.Location = new System.Drawing.Point(0, 284);
            this.pnlAdopciones.Name = "pnlAdopciones";
            this.pnlAdopciones.Size = new System.Drawing.Size(150, 71);
            this.pnlAdopciones.TabIndex = 15;
            // 
            // btnPropias
            // 
            this.btnPropias.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnPropias.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPropias.FlatAppearance.BorderSize = 0;
            this.btnPropias.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPropias.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnPropias.Location = new System.Drawing.Point(0, 46);
            this.btnPropias.Name = "btnPropias";
            this.btnPropias.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnPropias.Size = new System.Drawing.Size(150, 23);
            this.btnPropias.TabIndex = 4;
            this.btnPropias.Text = "Personales";
            this.btnPropias.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPropias.UseVisualStyleBackColor = false;
            this.btnPropias.Click += new System.EventHandler(this.btnPropias_Click);
            // 
            // btnAceptadas
            // 
            this.btnAceptadas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnAceptadas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAceptadas.FlatAppearance.BorderSize = 0;
            this.btnAceptadas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceptadas.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnAceptadas.Location = new System.Drawing.Point(0, 23);
            this.btnAceptadas.Name = "btnAceptadas";
            this.btnAceptadas.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnAceptadas.Size = new System.Drawing.Size(150, 23);
            this.btnAceptadas.TabIndex = 3;
            this.btnAceptadas.Text = "Aceptadas";
            this.btnAceptadas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAceptadas.UseVisualStyleBackColor = false;
            this.btnAceptadas.Click += new System.EventHandler(this.btnAceptadas_Click);
            // 
            // btnPendientes
            // 
            this.btnPendientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnPendientes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPendientes.FlatAppearance.BorderSize = 0;
            this.btnPendientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPendientes.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnPendientes.Location = new System.Drawing.Point(0, 0);
            this.btnPendientes.Name = "btnPendientes";
            this.btnPendientes.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnPendientes.Size = new System.Drawing.Size(150, 23);
            this.btnPendientes.TabIndex = 2;
            this.btnPendientes.Text = "Pendientes";
            this.btnPendientes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPendientes.UseVisualStyleBackColor = false;
            this.btnPendientes.Click += new System.EventHandler(this.btnPendientes_Click);
            // 
            // btnAdopciones
            // 
            this.btnAdopciones.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdopciones.FlatAppearance.BorderSize = 0;
            this.btnAdopciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdopciones.Location = new System.Drawing.Point(0, 261);
            this.btnAdopciones.Name = "btnAdopciones";
            this.btnAdopciones.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnAdopciones.Size = new System.Drawing.Size(150, 23);
            this.btnAdopciones.TabIndex = 14;
            this.btnAdopciones.Text = "Adopciones";
            this.btnAdopciones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdopciones.UseVisualStyleBackColor = true;
            this.btnAdopciones.Click += new System.EventHandler(this.btnAdopciones_Click);
            // 
            // btnMensajes
            // 
            this.btnMensajes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMensajes.FlatAppearance.BorderSize = 0;
            this.btnMensajes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMensajes.Location = new System.Drawing.Point(0, 238);
            this.btnMensajes.Name = "btnMensajes";
            this.btnMensajes.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnMensajes.Size = new System.Drawing.Size(150, 23);
            this.btnMensajes.TabIndex = 13;
            this.btnMensajes.Text = "Chat";
            this.btnMensajes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMensajes.UseVisualStyleBackColor = true;
            this.btnMensajes.Click += new System.EventHandler(this.btnMensajes_Click);
            // 
            // btnCuidadores
            // 
            this.btnCuidadores.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCuidadores.FlatAppearance.BorderSize = 0;
            this.btnCuidadores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCuidadores.Location = new System.Drawing.Point(0, 215);
            this.btnCuidadores.Name = "btnCuidadores";
            this.btnCuidadores.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnCuidadores.Size = new System.Drawing.Size(150, 23);
            this.btnCuidadores.TabIndex = 12;
            this.btnCuidadores.Text = "Cuidadores";
            this.btnCuidadores.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCuidadores.UseVisualStyleBackColor = true;
            this.btnCuidadores.Click += new System.EventHandler(this.btnCuidadores_Click);
            // 
            // pnlAnimales
            // 
            this.pnlAnimales.Controls.Add(this.btnOtroRefugio);
            this.pnlAnimales.Controls.Add(this.btnSinAdoptar);
            this.pnlAnimales.Controls.Add(this.btnAdoptados);
            this.pnlAnimales.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAnimales.Location = new System.Drawing.Point(0, 146);
            this.pnlAnimales.Name = "pnlAnimales";
            this.pnlAnimales.Size = new System.Drawing.Size(150, 69);
            this.pnlAnimales.TabIndex = 11;
            // 
            // btnOtroRefugio
            // 
            this.btnOtroRefugio.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnOtroRefugio.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOtroRefugio.FlatAppearance.BorderSize = 0;
            this.btnOtroRefugio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOtroRefugio.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnOtroRefugio.Location = new System.Drawing.Point(0, 46);
            this.btnOtroRefugio.Name = "btnOtroRefugio";
            this.btnOtroRefugio.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnOtroRefugio.Size = new System.Drawing.Size(150, 23);
            this.btnOtroRefugio.TabIndex = 4;
            this.btnOtroRefugio.Text = "De otro refugio";
            this.btnOtroRefugio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnOtroRefugio.UseVisualStyleBackColor = false;
            this.btnOtroRefugio.Click += new System.EventHandler(this.btnOtroRefugio_Click);
            // 
            // btnSinAdoptar
            // 
            this.btnSinAdoptar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSinAdoptar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSinAdoptar.FlatAppearance.BorderSize = 0;
            this.btnSinAdoptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSinAdoptar.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnSinAdoptar.Location = new System.Drawing.Point(0, 23);
            this.btnSinAdoptar.Name = "btnSinAdoptar";
            this.btnSinAdoptar.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnSinAdoptar.Size = new System.Drawing.Size(150, 23);
            this.btnSinAdoptar.TabIndex = 3;
            this.btnSinAdoptar.Text = "Sin Adoptar";
            this.btnSinAdoptar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSinAdoptar.UseVisualStyleBackColor = false;
            this.btnSinAdoptar.Click += new System.EventHandler(this.btnSinAdoptar_Click);
            // 
            // btnAdoptados
            // 
            this.btnAdoptados.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnAdoptados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdoptados.FlatAppearance.BorderSize = 0;
            this.btnAdoptados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdoptados.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnAdoptados.Location = new System.Drawing.Point(0, 0);
            this.btnAdoptados.Name = "btnAdoptados";
            this.btnAdoptados.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnAdoptados.Size = new System.Drawing.Size(150, 23);
            this.btnAdoptados.TabIndex = 2;
            this.btnAdoptados.Text = "Adoptados";
            this.btnAdoptados.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdoptados.UseVisualStyleBackColor = false;
            this.btnAdoptados.Click += new System.EventHandler(this.btnAdoptados_Click);
            // 
            // btnAnimales
            // 
            this.btnAnimales.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAnimales.FlatAppearance.BorderSize = 0;
            this.btnAnimales.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnimales.Location = new System.Drawing.Point(0, 123);
            this.btnAnimales.Name = "btnAnimales";
            this.btnAnimales.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnAnimales.Size = new System.Drawing.Size(150, 23);
            this.btnAnimales.TabIndex = 10;
            this.btnAnimales.Text = "Animales";
            this.btnAnimales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnimales.UseVisualStyleBackColor = true;
            this.btnAnimales.Click += new System.EventHandler(this.btnAnimales_Click);
            // 
            // btnRefugios
            // 
            this.btnRefugios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRefugios.FlatAppearance.BorderSize = 0;
            this.btnRefugios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefugios.Location = new System.Drawing.Point(0, 100);
            this.btnRefugios.Name = "btnRefugios";
            this.btnRefugios.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnRefugios.Size = new System.Drawing.Size(150, 23);
            this.btnRefugios.TabIndex = 9;
            this.btnRefugios.Text = "Refugios";
            this.btnRefugios.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRefugios.UseVisualStyleBackColor = true;
            this.btnRefugios.Click += new System.EventHandler(this.btnRefugios_Click);
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnSalir.Location = new System.Drawing.Point(0, 388);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnSalir.Size = new System.Drawing.Size(150, 23);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Cerrar Sesion";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // pnlLogo
            // 
            this.pnlLogo.Controls.Add(this.picImagen);
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Margin = new System.Windows.Forms.Padding(5);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(150, 100);
            this.pnlLogo.TabIndex = 0;
            // 
            // picImagen
            // 
            this.picImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picImagen.Location = new System.Drawing.Point(0, 0);
            this.picImagen.Name = "picImagen";
            this.picImagen.Size = new System.Drawing.Size(150, 100);
            this.picImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picImagen.TabIndex = 0;
            this.picImagen.TabStop = false;
            this.picImagen.Click += new System.EventHandler(this.picImagen_Click);
            this.picImagen.MouseEnter += new System.EventHandler(this.picImagen_MouseEnter);
            // 
            // frmResponsableLogueado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.pnlFormulario);
            this.Controls.Add(this.pnlMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmResponsableLogueado";
            this.Text = "TuOjitoDerecho";
            this.pnlMenu.ResumeLayout(false);
            this.pnlAdopciones.ResumeLayout(false);
            this.pnlAnimales.ResumeLayout(false);
            this.pnlLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picImagen)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFormulario;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Panel pnlAdopciones;
        private System.Windows.Forms.Button btnPropias;
        private System.Windows.Forms.Button btnAceptadas;
        private System.Windows.Forms.Button btnPendientes;
        private System.Windows.Forms.Button btnAdopciones;
        private System.Windows.Forms.Button btnMensajes;
        private System.Windows.Forms.Button btnCuidadores;
        private System.Windows.Forms.Panel pnlAnimales;
        private System.Windows.Forms.Button btnOtroRefugio;
        private System.Windows.Forms.Button btnSinAdoptar;
        private System.Windows.Forms.Button btnAdoptados;
        private System.Windows.Forms.Button btnAnimales;
        private System.Windows.Forms.Button btnRefugios;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Panel pnlLogo;
        private System.Windows.Forms.PictureBox picImagen;
    }
}