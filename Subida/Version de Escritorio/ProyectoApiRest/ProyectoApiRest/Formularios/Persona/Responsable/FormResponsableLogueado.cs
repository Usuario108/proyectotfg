﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Formularios.Protectora;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona.Responsable
{
    public partial class frmResponsableLogueado : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        public frmResponsableLogueado(Negocio n, Usuario user)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = user;
            CustomizarDiseño();
            picImagen.Image = _negocio.ByteToImage(_usuario.imagen);
            AbrirFormularioHijo(new frmPerfilUsuario(_negocio, _usuario));
        }

        private void CustomizarDiseño()
        {
            pnlAnimales.Visible = false;
            pnlAdopciones.Visible = false;
        }

        private void OcultarSubMenu()
        {
            if (pnlAnimales.Visible == true)
            {
                pnlAnimales.Visible = false;
            }
            if (pnlAdopciones.Visible == true)
            {
                pnlAdopciones.Visible = false;
            }
        }

        private void MostrarSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                OcultarSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }

        private void picImagen_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmPerfilUsuario(_negocio, _usuario));
            OcultarSubMenu();
        }

        private void btnRefugios_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmRefugios(_negocio));
            OcultarSubMenu();
        }

        private void btnAnimales_Click(object sender, EventArgs e)
        {
            MostrarSubMenu(pnlAnimales);
        }

        private void btnAdoptados_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaAnimales(_negocio, true, (int)_usuario.refugioid));
            OcultarSubMenu();
        }

        private void btnSinAdoptar_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaAnimales(_negocio, false, (int)_usuario.refugioid));
            OcultarSubMenu();
        }

        private void btnOtroRefugio_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaMascotas(_negocio, _usuario));
            OcultarSubMenu();
        }

        private void btnCuidadores_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaTrabajadores(_negocio, _usuario.id));
            OcultarSubMenu();
        }

        private void btnMensajes_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmChat(_negocio, _usuario));
            OcultarSubMenu();
        }

        private void btnAdopciones_Click(object sender, EventArgs e)
        {
            MostrarSubMenu(pnlAdopciones);
        }

        private void btnPendientes_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmAdopciones(_negocio, false, (int)_usuario.refugioid));
            OcultarSubMenu();
        }

        private void btnAceptadas_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmAdopciones(_negocio, true, (int)_usuario.refugioid));
            OcultarSubMenu();
        }

        private void btnPropias_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmHistorialAdopciones(_negocio, _usuario));
            OcultarSubMenu();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void picImagen_MouseEnter(object sender, EventArgs e)
        {
            picImagen.Image = _negocio.ByteToImage(_usuario.imagen);
        }

        private Form frmActivo = null;
        private void AbrirFormularioHijo(Form formHijo)
        {
            if (frmActivo != null)
            {
                frmActivo.Close();
            }

            frmActivo = formHijo;
            formHijo.TopLevel = false;
            formHijo.FormBorderStyle = FormBorderStyle.None;
            formHijo.Dock = DockStyle.Fill;
            pnlFormulario.Controls.Add(formHijo);
            pnlFormulario.Tag = formHijo;
            formHijo.BringToFront();
            formHijo.Show();
        }
    }
}
