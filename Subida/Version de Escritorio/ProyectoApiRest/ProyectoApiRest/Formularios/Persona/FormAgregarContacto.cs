﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona
{
    public partial class frmAgregarContacto : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        public frmAgregarContacto(Negocio n)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = new Usuario();
        }

        public Usuario Contacto()
        {
            return _usuario;
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            _usuario = await _negocio.ObtenerUsuarioPorCorreo(txtCorreo.Text);

            if (string.IsNullOrEmpty(txtCorreo.Text))
            {
                MessageBox.Show("El correo es invalido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (_usuario == null)
            {
                MessageBox.Show("El correo no es conocido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult = DialogResult.OK;
            }
        }
    }
}
