﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona
{
    public partial class frmChat : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        private Usuario _contacto;

        public frmChat(Negocio n, Usuario user)
        {
            InitializeComponent();
            _usuario = user;
            _negocio = n;
            CargarContactos();
        }

        private async void CargarContactos()
        {
            List<Usuario> lista = await _negocio.ObtenerContactos(_usuario.id);

            lvwChat.Items.Clear();
            foreach (Usuario u in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { u.nombre });
                item.Tag = u.id;
                lvwChat.Items.Add(item);
            }
        }

        private async void CargarMensajes()
        {
            txtMensaje.Text = "";
            lstMensaje.Items.Clear();

            int id = (int)lvwChat.SelectedItems[0].Tag;
            _contacto = await _negocio.ObtenerUsuario(id);
            List<Mensaje> lista = await _negocio.ObtenerMensajesDeContacto(_usuario.id, id);

            foreach (Mensaje men in lista)
            {
                if (men.emisorid == _usuario.id)
                {
                    lstMensaje.Items.Add("Yo: " + men.mensaje);
                }
                else
                {
                    lstMensaje.Items.Add("Contacto: " + men.mensaje);
                }
                lstMensaje.Items.Add("");
            }
        }

        private void lvwChat_DoubleClick(object sender, EventArgs e)
        {
            if (lvwChat.SelectedItems.Count == 1)
            {
                CargarMensajes();
            }
        }

        private void lvwChat_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwChat.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwChat.Sort();
        }

        private void tsmiAgregar_Click(object sender, EventArgs e)
        {
            frmAgregarContacto contact = new frmAgregarContacto(_negocio);
            DialogResult dg = contact.ShowDialog();

            if (dg == DialogResult.OK)
            {
                Usuario use = contact.Contacto();
                ListViewItem item = new ListViewItem(new string[] { use.nombre });
                item.Tag = use.id;
                lvwChat.Items.Add(item);
            }
        }

        private async void btnEnviar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtMensaje.Text) && _contacto != null)
            {
                Mensaje mensaje = new Mensaje();
                mensaje.emisorid = _usuario.id;
                mensaje.receptorid = _contacto.id;
                mensaje.mensaje = txtMensaje.Text;
                DateTime horaActual = DateTime.Now;
                mensaje.hora = horaActual.ToString("yyyy-MM-dd HH:mm:ss");

                await _negocio.InsertarMensaje(mensaje);
                CargarMensajes();
            }
        }
    }
}
