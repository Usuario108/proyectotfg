﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona.Admin
{
    public partial class frmAdminMensajes : Form
    {
        private Negocio _negocio;

        public frmAdminMensajes(Negocio n)
        {
            InitializeComponent();
            _negocio = n;
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Mensaje> lista = await _negocio.sacarMensajesAsync();

            lvwMensajes.Items.Clear();
            foreach (Mensaje m in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { await _negocio.ObtenerNombreUsuarioPorId(m.emisorid), 
                    await _negocio.ObtenerNombreUsuarioPorId(m.receptorid), m.hora, m.mensaje });
                item.Tag = m.id;
                lvwMensajes.Items.Add(item);
            }
        }

        private void lvwMensajes_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwMensajes.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwMensajes.Sort();
        }
    }
}
