﻿namespace ProyectoApiRest.Formularios.Persona.Admin
{
    partial class frmAdminAdopciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpFin = new System.Windows.Forms.DateTimePicker();
            this.lblFechaFin = new System.Windows.Forms.Label();
            this.dtpInicio = new System.Windows.Forms.DateTimePicker();
            this.lblFechaInicio = new System.Windows.Forms.Label();
            this.dtpSolicitud = new System.Windows.Forms.DateTimePicker();
            this.lblSolicitud = new System.Windows.Forms.Label();
            this.grpMascota = new System.Windows.Forms.GroupBox();
            this.lblPeso = new System.Windows.Forms.Label();
            this.lblRaza = new System.Windows.Forms.Label();
            this.txtPeso = new System.Windows.Forms.TextBox();
            this.txtRaza = new System.Windows.Forms.TextBox();
            this.txtNombreMas = new System.Windows.Forms.TextBox();
            this.lblNombreMas = new System.Windows.Forms.Label();
            this.grpDueño = new System.Windows.Forms.GroupBox();
            this.lblCorreo = new System.Windows.Forms.Label();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.txtNombreDu = new System.Windows.Forms.TextBox();
            this.lblNombreDue = new System.Windows.Forms.Label();
            this.lvwSolicitudes = new System.Windows.Forms.ListView();
            this.columnUsuario = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnMascota = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnFecha = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.grpMascota.SuspendLayout();
            this.grpDueño.SuspendLayout();
            this.SuspendLayout();
            // 
            // dtpFin
            // 
            this.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpFin.Location = new System.Drawing.Point(364, 154);
            this.dtpFin.Name = "dtpFin";
            this.dtpFin.Size = new System.Drawing.Size(133, 20);
            this.dtpFin.TabIndex = 125;
            // 
            // lblFechaFin
            // 
            this.lblFechaFin.AutoSize = true;
            this.lblFechaFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaFin.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblFechaFin.Location = new System.Drawing.Point(361, 132);
            this.lblFechaFin.Name = "lblFechaFin";
            this.lblFechaFin.Size = new System.Drawing.Size(149, 15);
            this.lblFechaFin.TabIndex = 124;
            this.lblFechaFin.Text = "Fecha de Fin de Adopcion";
            // 
            // dtpInicio
            // 
            this.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpInicio.Location = new System.Drawing.Point(364, 98);
            this.dtpInicio.Name = "dtpInicio";
            this.dtpInicio.Size = new System.Drawing.Size(133, 20);
            this.dtpInicio.TabIndex = 123;
            // 
            // lblFechaInicio
            // 
            this.lblFechaInicio.AutoSize = true;
            this.lblFechaInicio.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechaInicio.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblFechaInicio.Location = new System.Drawing.Point(361, 76);
            this.lblFechaInicio.Name = "lblFechaInicio";
            this.lblFechaInicio.Size = new System.Drawing.Size(161, 15);
            this.lblFechaInicio.TabIndex = 122;
            this.lblFechaInicio.Text = "Fecha de Inicio de Adopcion";
            // 
            // dtpSolicitud
            // 
            this.dtpSolicitud.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSolicitud.Location = new System.Drawing.Point(364, 47);
            this.dtpSolicitud.Name = "dtpSolicitud";
            this.dtpSolicitud.Size = new System.Drawing.Size(133, 20);
            this.dtpSolicitud.TabIndex = 121;
            // 
            // lblSolicitud
            // 
            this.lblSolicitud.AutoSize = true;
            this.lblSolicitud.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSolicitud.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblSolicitud.Location = new System.Drawing.Point(361, 25);
            this.lblSolicitud.Name = "lblSolicitud";
            this.lblSolicitud.Size = new System.Drawing.Size(158, 15);
            this.lblSolicitud.TabIndex = 120;
            this.lblSolicitud.Text = "Fecha de Envio de Solicitud";
            // 
            // grpMascota
            // 
            this.grpMascota.Controls.Add(this.lblPeso);
            this.grpMascota.Controls.Add(this.lblRaza);
            this.grpMascota.Controls.Add(this.txtPeso);
            this.grpMascota.Controls.Add(this.txtRaza);
            this.grpMascota.Controls.Add(this.txtNombreMas);
            this.grpMascota.Controls.Add(this.lblNombreMas);
            this.grpMascota.Location = new System.Drawing.Point(184, 18);
            this.grpMascota.Name = "grpMascota";
            this.grpMascota.Size = new System.Drawing.Size(157, 189);
            this.grpMascota.TabIndex = 119;
            this.grpMascota.TabStop = false;
            this.grpMascota.Text = "Animal";
            // 
            // lblPeso
            // 
            this.lblPeso.AutoSize = true;
            this.lblPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPeso.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblPeso.Location = new System.Drawing.Point(3, 126);
            this.lblPeso.Name = "lblPeso";
            this.lblPeso.Size = new System.Drawing.Size(35, 15);
            this.lblPeso.TabIndex = 97;
            this.lblPeso.Text = "Peso";
            // 
            // lblRaza
            // 
            this.lblRaza.AutoSize = true;
            this.lblRaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRaza.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblRaza.Location = new System.Drawing.Point(3, 71);
            this.lblRaza.Name = "lblRaza";
            this.lblRaza.Size = new System.Drawing.Size(36, 15);
            this.lblRaza.TabIndex = 99;
            this.lblRaza.Text = "Raza";
            // 
            // txtPeso
            // 
            this.txtPeso.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeso.Location = new System.Drawing.Point(6, 146);
            this.txtPeso.Name = "txtPeso";
            this.txtPeso.ReadOnly = true;
            this.txtPeso.Size = new System.Drawing.Size(133, 23);
            this.txtPeso.TabIndex = 96;
            // 
            // txtRaza
            // 
            this.txtRaza.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRaza.Location = new System.Drawing.Point(6, 91);
            this.txtRaza.Name = "txtRaza";
            this.txtRaza.ReadOnly = true;
            this.txtRaza.Size = new System.Drawing.Size(133, 23);
            this.txtRaza.TabIndex = 98;
            // 
            // txtNombreMas
            // 
            this.txtNombreMas.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreMas.Location = new System.Drawing.Point(6, 39);
            this.txtNombreMas.Name = "txtNombreMas";
            this.txtNombreMas.ReadOnly = true;
            this.txtNombreMas.Size = new System.Drawing.Size(133, 23);
            this.txtNombreMas.TabIndex = 94;
            // 
            // lblNombreMas
            // 
            this.lblNombreMas.AutoSize = true;
            this.lblNombreMas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreMas.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNombreMas.Location = new System.Drawing.Point(3, 19);
            this.lblNombreMas.Name = "lblNombreMas";
            this.lblNombreMas.Size = new System.Drawing.Size(52, 15);
            this.lblNombreMas.TabIndex = 95;
            this.lblNombreMas.Text = "Nombre";
            // 
            // grpDueño
            // 
            this.grpDueño.Controls.Add(this.lblCorreo);
            this.grpDueño.Controls.Add(this.lblTelefono);
            this.grpDueño.Controls.Add(this.txtCorreo);
            this.grpDueño.Controls.Add(this.txtTelefono);
            this.grpDueño.Controls.Add(this.txtNombreDu);
            this.grpDueño.Controls.Add(this.lblNombreDue);
            this.grpDueño.Location = new System.Drawing.Point(16, 18);
            this.grpDueño.Name = "grpDueño";
            this.grpDueño.Size = new System.Drawing.Size(153, 189);
            this.grpDueño.TabIndex = 118;
            this.grpDueño.TabStop = false;
            this.grpDueño.Text = "Dueño";
            // 
            // lblCorreo
            // 
            this.lblCorreo.AutoSize = true;
            this.lblCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblCorreo.Location = new System.Drawing.Point(3, 128);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(44, 15);
            this.lblCorreo.TabIndex = 91;
            this.lblCorreo.Text = "Correo";
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefono.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTelefono.Location = new System.Drawing.Point(3, 73);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(55, 15);
            this.lblTelefono.TabIndex = 93;
            this.lblTelefono.Text = "Telefono";
            // 
            // txtCorreo
            // 
            this.txtCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.Location = new System.Drawing.Point(6, 148);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.ReadOnly = true;
            this.txtCorreo.Size = new System.Drawing.Size(133, 23);
            this.txtCorreo.TabIndex = 90;
            // 
            // txtTelefono
            // 
            this.txtTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Location = new System.Drawing.Point(6, 93);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.ReadOnly = true;
            this.txtTelefono.Size = new System.Drawing.Size(133, 23);
            this.txtTelefono.TabIndex = 92;
            // 
            // txtNombreDu
            // 
            this.txtNombreDu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreDu.Location = new System.Drawing.Point(6, 41);
            this.txtNombreDu.Name = "txtNombreDu";
            this.txtNombreDu.ReadOnly = true;
            this.txtNombreDu.Size = new System.Drawing.Size(133, 23);
            this.txtNombreDu.TabIndex = 88;
            // 
            // lblNombreDue
            // 
            this.lblNombreDue.AutoSize = true;
            this.lblNombreDue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreDue.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNombreDue.Location = new System.Drawing.Point(3, 21);
            this.lblNombreDue.Name = "lblNombreDue";
            this.lblNombreDue.Size = new System.Drawing.Size(52, 15);
            this.lblNombreDue.TabIndex = 89;
            this.lblNombreDue.Text = "Nombre";
            // 
            // lvwSolicitudes
            // 
            this.lvwSolicitudes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnUsuario,
            this.columnMascota,
            this.columnFecha});
            this.lvwSolicitudes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lvwSolicitudes.FullRowSelect = true;
            this.lvwSolicitudes.GridLines = true;
            this.lvwSolicitudes.HideSelection = false;
            this.lvwSolicitudes.Location = new System.Drawing.Point(0, 223);
            this.lvwSolicitudes.MultiSelect = false;
            this.lvwSolicitudes.Name = "lvwSolicitudes";
            this.lvwSolicitudes.Size = new System.Drawing.Size(564, 138);
            this.lvwSolicitudes.TabIndex = 117;
            this.lvwSolicitudes.UseCompatibleStateImageBehavior = false;
            this.lvwSolicitudes.View = System.Windows.Forms.View.Details;
            this.lvwSolicitudes.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvwSolicitudes_ColumnClick);
            this.lvwSolicitudes.DoubleClick += new System.EventHandler(this.lvwSolicitudes_DoubleClick);
            // 
            // columnUsuario
            // 
            this.columnUsuario.Text = "Dueño";
            this.columnUsuario.Width = 112;
            // 
            // columnMascota
            // 
            this.columnMascota.Text = "Mascota";
            this.columnMascota.Width = 133;
            // 
            // columnFecha
            // 
            this.columnFecha.Text = "Fecha de Solicitud";
            this.columnFecha.Width = 185;
            // 
            // frmAdminAdopciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 361);
            this.Controls.Add(this.dtpFin);
            this.Controls.Add(this.lblFechaFin);
            this.Controls.Add(this.dtpInicio);
            this.Controls.Add(this.lblFechaInicio);
            this.Controls.Add(this.dtpSolicitud);
            this.Controls.Add(this.lblSolicitud);
            this.Controls.Add(this.grpMascota);
            this.Controls.Add(this.grpDueño);
            this.Controls.Add(this.lvwSolicitudes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmAdminAdopciones";
            this.Text = "TuOjitoDerecho";
            this.grpMascota.ResumeLayout(false);
            this.grpMascota.PerformLayout();
            this.grpDueño.ResumeLayout(false);
            this.grpDueño.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpFin;
        private System.Windows.Forms.Label lblFechaFin;
        private System.Windows.Forms.DateTimePicker dtpInicio;
        private System.Windows.Forms.Label lblFechaInicio;
        private System.Windows.Forms.DateTimePicker dtpSolicitud;
        private System.Windows.Forms.Label lblSolicitud;
        private System.Windows.Forms.GroupBox grpMascota;
        private System.Windows.Forms.Label lblPeso;
        private System.Windows.Forms.Label lblRaza;
        private System.Windows.Forms.TextBox txtPeso;
        private System.Windows.Forms.TextBox txtRaza;
        private System.Windows.Forms.TextBox txtNombreMas;
        private System.Windows.Forms.Label lblNombreMas;
        private System.Windows.Forms.GroupBox grpDueño;
        private System.Windows.Forms.Label lblCorreo;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.TextBox txtCorreo;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.TextBox txtNombreDu;
        private System.Windows.Forms.Label lblNombreDue;
        private System.Windows.Forms.ListView lvwSolicitudes;
        private System.Windows.Forms.ColumnHeader columnUsuario;
        private System.Windows.Forms.ColumnHeader columnMascota;
        private System.Windows.Forms.ColumnHeader columnFecha;
    }
}