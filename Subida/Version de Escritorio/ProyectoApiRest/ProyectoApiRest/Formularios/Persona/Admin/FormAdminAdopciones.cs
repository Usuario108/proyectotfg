﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona.Admin
{
    public partial class frmAdminAdopciones : Form
    {
        private Negocio _negocio;

        private Adopcion _solicitud;

        public frmAdminAdopciones(Negocio n)
        {
            InitializeComponent();
            dtpSolicitud.Enabled = false;
            dtpInicio.Enabled = false;
            dtpFin.Enabled = false;

            dtpInicio.Format = DateTimePickerFormat.Custom;
            dtpInicio.CustomFormat = " ";
            dtpFin.Format = DateTimePickerFormat.Custom;
            dtpFin.CustomFormat = " ";

            _negocio = n;
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Adopcion> lista = await _negocio.sacarAdopcionesAsync();
            lvwSolicitudes.Items.Clear();

            foreach (Adopcion sol in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { await _negocio.ObtenerNombreUsuarioPorId(sol.userid), await _negocio.ObtenerNombreMascotaPorId(sol.mascotaid), sol.solicitudfec.ToString() });
                item.Tag = sol;
                lvwSolicitudes.Items.Add(item);
            }
        }

        private async void lvwSolicitudes_DoubleClick(object sender, EventArgs e)
        {
            if (lvwSolicitudes.SelectedItems.Count == 1)
            {
                _solicitud = (Adopcion)lvwSolicitudes.SelectedItems[0].Tag;
                Usuario dueno = await _negocio.ObtenerUsuario(_solicitud.userid);
                Mascota mas = await _negocio.ObtenerMascota(_solicitud.mascotaid);

                txtNombreDu.Text = dueno.nombre;
                txtTelefono.Text = dueno.telefono;
                txtCorreo.Text = dueno.correo;
                txtNombreMas.Text = mas.nombre;
                txtRaza.Text = mas.raza;
                txtPeso.Text = mas.peso.ToString();

                DateTime fechaHora = DateTime.Parse(_solicitud.solicitudfec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                dtpSolicitud.Value = fechaHora;

                if (_solicitud.iniciofec != null)
                {
                    dtpInicio.Format = DateTimePickerFormat.Custom;
                    dtpInicio.CustomFormat = "dd/MM/yyyy";
                    DateTime fechaInicio = DateTime.Parse(_solicitud.iniciofec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    dtpInicio.Value = fechaInicio;
                }
                else
                {
                    dtpInicio.Format = DateTimePickerFormat.Custom;
                    dtpInicio.CustomFormat = " ";
                }

                if (_solicitud.finfec != null)
                {
                    dtpFin.Format = DateTimePickerFormat.Custom;
                    dtpFin.CustomFormat = "dd/MM/yyyy";
                    DateTime fechaFin = DateTime.Parse(_solicitud.finfec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    dtpFin.Value = fechaFin;
                }
                else
                {
                    dtpFin.Format = DateTimePickerFormat.Custom;
                    dtpFin.CustomFormat = " ";
                }
            }
        }

        private void lvwSolicitudes_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwSolicitudes.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwSolicitudes.Sort();
        }
    }
}
