﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona.Admin
{
    public partial class frmAdminUsuarios : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        public frmAdminUsuarios(Negocio n)
        {
            InitializeComponent();
            _negocio = n;
            dtpFechaNacimiento.Enabled = false;
            CargarLista();
        }

        private async void CargarLista()
        {
            lvwCuidadores.Items.Clear();
            List<Usuario> lista = await _negocio.sacarUsuariosAsync();

            foreach (Usuario u in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { u.nombre, u.telefono, u.correo, u.direccion });
                item.Tag = u.id;
                lvwCuidadores.Items.Add(item);
            }
        }

        private async void lvwCuidadores_DoubleClick(object sender, EventArgs e)
        {
            if (lvwCuidadores.SelectedItems.Count == 1)
            {
                int id = (int)lvwCuidadores.SelectedItems[0].Tag;
                _usuario = await _negocio.ObtenerUsuario(id);
                txtCorreo.Text = _usuario.correo;
                txtNombre.Text = _usuario.nombre;
                txtDireccion.Text = _usuario.direccion;
                txtTelefono.Text = _usuario.telefono;
                DateTime fechaHora = DateTime.Parse(_usuario.nacimiento, null, System.Globalization.DateTimeStyles.RoundtripKind);
                dtpFechaNacimiento.Value = fechaHora;
                picFoto.Image = _negocio.ByteToImage(_usuario.imagen);
            }
        }

        private void lvwCuidadores_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwCuidadores.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwCuidadores.Sort();
        }
    }
}
