﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Persona.Admin
{
    public partial class frmAdminMascotas : Form
    {
        private Negocio _negocio;

        private Mascota _mascota;

        public frmAdminMascotas(Negocio n)
        {
            InitializeComponent();
            _negocio = n;
            dtpFechaNacimiento.Enabled = false;
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Mascota> lista = await _negocio.sacarMascotasAsync();

            lvwMascotas.Items.Clear();
            foreach (Mascota m in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { m.nombre, m.raza, await _negocio.ObtenerNombreRefugioPorId(m.refugioid) });
                item.Tag = m.id;
                lvwMascotas.Items.Add(item);
            }
        }

        private async void lvwMascotas_DoubleClick(object sender, EventArgs e)
        {
            if (lvwMascotas.SelectedItems.Count == 1)
            {
                int id = (int)lvwMascotas.SelectedItems[0].Tag;
                _mascota = await _negocio.ObtenerMascota(id);
                Refugio refu = await _negocio.ObtenerRefugio(_mascota.refugioid);

                txtNombre.Text = _mascota.nombre;
                txtRaza.Text = _mascota.raza;
                txtPeso.Text = _mascota.peso.ToString();
                txtRefugio.Text = refu.nombre;
                Usuario user = await _negocio.ObtenerUsuario(refu.responsableid);
                txtResponsable.Text = user.correo;
                DateTime fechaHora = DateTime.Parse(_mascota.nacimiento, null, System.Globalization.DateTimeStyles.RoundtripKind);
                dtpFechaNacimiento.Value = fechaHora;
                txtDescripcion.Text = _mascota.descripcion;
                picFoto.Image = _negocio.ByteToImage(_mascota.imagen);
            }
        }

        private void lvwMascotas_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwMascotas.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwMascotas.Sort();
        }
    }
}
