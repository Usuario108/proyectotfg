﻿namespace ProyectoApiRest.Formularios.Persona.Admin
{
    partial class frmAdminMensajes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvwMensajes = new System.Windows.Forms.ListView();
            this.Remitente = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Destinatario = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.FechaHora = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Mensaje = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lvwMensajes
            // 
            this.lvwMensajes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Remitente,
            this.Destinatario,
            this.FechaHora,
            this.Mensaje});
            this.lvwMensajes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvwMensajes.FullRowSelect = true;
            this.lvwMensajes.GridLines = true;
            this.lvwMensajes.HideSelection = false;
            this.lvwMensajes.Location = new System.Drawing.Point(0, 0);
            this.lvwMensajes.MultiSelect = false;
            this.lvwMensajes.Name = "lvwMensajes";
            this.lvwMensajes.Size = new System.Drawing.Size(564, 361);
            this.lvwMensajes.TabIndex = 0;
            this.lvwMensajes.UseCompatibleStateImageBehavior = false;
            this.lvwMensajes.View = System.Windows.Forms.View.Details;
            this.lvwMensajes.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.lvwMensajes_ColumnClick);
            // 
            // Remitente
            // 
            this.Remitente.Text = "Remitente";
            this.Remitente.Width = 100;
            // 
            // Destinatario
            // 
            this.Destinatario.Text = "Destinatario";
            this.Destinatario.Width = 121;
            // 
            // FechaHora
            // 
            this.FechaHora.Text = "Fecha y Hora";
            this.FechaHora.Width = 103;
            // 
            // Mensaje
            // 
            this.Mensaje.Text = "Texto";
            this.Mensaje.Width = 226;
            // 
            // frmAdminMensajes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 361);
            this.Controls.Add(this.lvwMensajes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmAdminMensajes";
            this.Text = "TuOjitoDerecho";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvwMensajes;
        private System.Windows.Forms.ColumnHeader Remitente;
        private System.Windows.Forms.ColumnHeader Destinatario;
        private System.Windows.Forms.ColumnHeader FechaHora;
        private System.Windows.Forms.ColumnHeader Mensaje;
    }
}