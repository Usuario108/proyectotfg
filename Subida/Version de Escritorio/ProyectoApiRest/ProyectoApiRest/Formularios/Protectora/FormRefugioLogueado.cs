﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Protectora
{
    public partial class frmRefugioLogueado : Form
    {
        private Negocio _negocio;

        private Refugio _refugio;

        public frmRefugioLogueado(Negocio n, Refugio refu)
        {
            InitializeComponent();
            _refugio = refu;
            _negocio = n;
            CustomizarDiseño();
            picLogo.Image = _negocio.ByteToImage(_refugio.imagen);
            AbrirFormularioHijo(new frmPerfilRefugio(_negocio, _refugio));
        }

        private void CustomizarDiseño()
        {
            pnlAnimales.Visible = false;
            pnlSolicitudes.Visible = false;
        }

        private void OcultarSubMenu()
        {
            if (pnlAnimales.Visible == true)
            {
                pnlAnimales.Visible = false;
            }
            if (pnlSolicitudes.Visible == true)
            {
                pnlSolicitudes.Visible = false;
            }
        }

        private void MostrarSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                OcultarSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
        }

        private void picLogo_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmPerfilRefugio(_negocio, _refugio));
            OcultarSubMenu();
        }

        private void btnAnimales_Click(object sender, EventArgs e)
        {
            MostrarSubMenu(pnlAnimales);
        }

        private void btnAdoptados_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaAnimales(_negocio, true, _refugio.id));
            OcultarSubMenu();
        }

        private void btnSinAdoptar_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaAnimales(_negocio, false, _refugio.id));
            OcultarSubMenu();
        }

        private void btnCuidadores_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmListaCuidadores(_negocio, _refugio.id));
            OcultarSubMenu();
        }

        private void btnSolicitudes_Click(object sender, EventArgs e)
        {
            MostrarSubMenu(pnlSolicitudes);
        }

        private void btnAceptadas_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmAdopciones(_negocio, true, _refugio.id));
            OcultarSubMenu();
        }

        private void btnPendientes_Click(object sender, EventArgs e)
        {
            AbrirFormularioHijo(new frmAdopciones(_negocio, false, _refugio.id));
            OcultarSubMenu();
        }

        private void picLogo_MouseEnter(object sender, EventArgs e)
        {
            picLogo.Image = _negocio.ByteToImage(_refugio.imagen);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private Form frmActivo = null;
        private void AbrirFormularioHijo(Form formHijo)
        {
            if (frmActivo != null)
            {
                frmActivo.Close();
            }

            frmActivo = formHijo;
            formHijo.TopLevel = false;
            formHijo.FormBorderStyle = FormBorderStyle.None;
            formHijo.Dock = DockStyle.Fill;
            pnlFormulario.Controls.Add(formHijo);
            pnlFormulario.Tag = formHijo;
            formHijo.BringToFront();
            formHijo.Show();
        }
    }
}
