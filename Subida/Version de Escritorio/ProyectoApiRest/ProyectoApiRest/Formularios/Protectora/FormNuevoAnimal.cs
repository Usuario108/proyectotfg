﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Protectora
{
    public partial class frmNuevoAnimal : Form
    {
        private Negocio _negocio;

        private Mascota _mascota;

        public frmNuevoAnimal(Negocio n, Mascota mascota)
        {
            InitializeComponent();
            _negocio = n;
            _mascota = mascota;
        }

        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult rs = ofd.ShowDialog();
            if (rs == DialogResult.OK)
            {
                picFoto.Image = Image.FromFile(ofd.FileName);
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtRaza.Text))
            {
                MessageBox.Show("La raza no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtPeso.Text))
            {
                MessageBox.Show("El peso no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpFechaNacimiento.Value > DateTime.Now)
            {
                MessageBox.Show("La fecha no puede ser posterior al dia de hoy.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                _mascota.nombre = txtNombre.Text;
                DateTime fechaParseada = DateTime.ParseExact(dtpFechaNacimiento.Value.ToShortDateString(), "dd/MM/yyyy", null);
                _mascota.nacimiento = fechaParseada.ToString("yyyy-MM-dd");
                _mascota.raza = txtRaza.Text;
                _mascota.peso = Convert.ToSingle(txtPeso.Text);
                _mascota.descripcion = txtDescripcion.Text;
                _mascota.imagen = _negocio.ImageToByteArray(picFoto.Image);

                DialogResult = DialogResult.OK;
            }
        }
    }
}
