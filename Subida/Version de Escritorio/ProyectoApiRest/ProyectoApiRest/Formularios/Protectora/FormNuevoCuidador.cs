﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Protectora
{
    public partial class frmNuevoCuidador : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        public frmNuevoCuidador(Negocio n, Usuario usuario)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = usuario;
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult rs = ofd.ShowDialog();
            if (rs == DialogResult.OK)
            {
                picFoto.Image = Image.FromFile(ofd.FileName);
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtCorreo.Text))
            {
                MessageBox.Show("El correo no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrEmpty(txtConfirmar.Text))
            {
                MessageBox.Show("La contraseña no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!txtPassword.Text.Equals(txtConfirmar.Text))
            {
                MessageBox.Show("La contraseña no coincide.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtTelefono.Text))
            {
                MessageBox.Show("El telefono no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtDireccion.Text))
            {
                MessageBox.Show("La direccion no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpFechaNacimiento.Value >= DateTime.Now)
            {
                MessageBox.Show("La fecha no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            int difFechas = DateTime.Now.Year - dtpFechaNacimiento.Value.Year;
            if (difFechas < 18)
            {
                MessageBox.Show("No puede registrarse menores de 18 años.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                bool correoValido = await _negocio.ComprobarCorreo(txtCorreo.Text);
                if (!correoValido)
                {
                    MessageBox.Show("El correo le pertenece a otra cuenta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _usuario.nombre = txtNombre.Text;
                    DateTime fechaParseada = DateTime.ParseExact(dtpFechaNacimiento.Value.ToShortDateString(), "dd/MM/yyyy", null);
                    _usuario.nacimiento = fechaParseada.ToString("yyyy-MM-dd");
                    _usuario.telefono = txtTelefono.Text;
                    _usuario.correo = txtCorreo.Text;
                    _usuario.password = txtPassword.Text;
                    _usuario.direccion = txtDireccion.Text;
                    _usuario.imagen = _negocio.ImageToByteArray(picFoto.Image);

                    DialogResult = DialogResult.OK;
                }
            }
        }
    }
}
