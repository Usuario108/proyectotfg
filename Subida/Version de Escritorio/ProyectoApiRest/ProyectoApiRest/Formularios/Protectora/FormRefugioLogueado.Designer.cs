﻿namespace ProyectoApiRest.Formularios.Protectora
{
    partial class frmRefugioLogueado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRefugioLogueado));
            this.pnlFormulario = new System.Windows.Forms.Panel();
            this.pnlMenu = new System.Windows.Forms.Panel();
            this.btnSalir = new System.Windows.Forms.Button();
            this.pnlSolicitudes = new System.Windows.Forms.Panel();
            this.btnPendientes = new System.Windows.Forms.Button();
            this.btnAceptadas = new System.Windows.Forms.Button();
            this.btnSolicitudes = new System.Windows.Forms.Button();
            this.btnCuidadores = new System.Windows.Forms.Button();
            this.pnlAnimales = new System.Windows.Forms.Panel();
            this.btnSinAdoptar = new System.Windows.Forms.Button();
            this.btnAdoptados = new System.Windows.Forms.Button();
            this.btnAnimales = new System.Windows.Forms.Button();
            this.pnlLogo = new System.Windows.Forms.Panel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.pnlMenu.SuspendLayout();
            this.pnlSolicitudes.SuspendLayout();
            this.pnlAnimales.SuspendLayout();
            this.pnlLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlFormulario
            // 
            this.pnlFormulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFormulario.Location = new System.Drawing.Point(150, 0);
            this.pnlFormulario.Name = "pnlFormulario";
            this.pnlFormulario.Size = new System.Drawing.Size(534, 411);
            this.pnlFormulario.TabIndex = 7;
            // 
            // pnlMenu
            // 
            this.pnlMenu.AutoScroll = true;
            this.pnlMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.pnlMenu.Controls.Add(this.btnSalir);
            this.pnlMenu.Controls.Add(this.pnlSolicitudes);
            this.pnlMenu.Controls.Add(this.btnSolicitudes);
            this.pnlMenu.Controls.Add(this.btnCuidadores);
            this.pnlMenu.Controls.Add(this.pnlAnimales);
            this.pnlMenu.Controls.Add(this.btnAnimales);
            this.pnlMenu.Controls.Add(this.pnlLogo);
            this.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlMenu.Location = new System.Drawing.Point(0, 0);
            this.pnlMenu.Name = "pnlMenu";
            this.pnlMenu.Size = new System.Drawing.Size(150, 411);
            this.pnlMenu.TabIndex = 6;
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSalir.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnSalir.FlatAppearance.BorderSize = 0;
            this.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnSalir.Location = new System.Drawing.Point(0, 388);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnSalir.Size = new System.Drawing.Size(150, 23);
            this.btnSalir.TabIndex = 7;
            this.btnSalir.Text = "Cerrar Sesion";
            this.btnSalir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // pnlSolicitudes
            // 
            this.pnlSolicitudes.Controls.Add(this.btnPendientes);
            this.pnlSolicitudes.Controls.Add(this.btnAceptadas);
            this.pnlSolicitudes.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSolicitudes.Location = new System.Drawing.Point(0, 221);
            this.pnlSolicitudes.Name = "pnlSolicitudes";
            this.pnlSolicitudes.Size = new System.Drawing.Size(150, 52);
            this.pnlSolicitudes.TabIndex = 6;
            // 
            // btnPendientes
            // 
            this.btnPendientes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnPendientes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPendientes.FlatAppearance.BorderSize = 0;
            this.btnPendientes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPendientes.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnPendientes.Location = new System.Drawing.Point(0, 23);
            this.btnPendientes.Name = "btnPendientes";
            this.btnPendientes.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnPendientes.Size = new System.Drawing.Size(150, 23);
            this.btnPendientes.TabIndex = 3;
            this.btnPendientes.Text = "Pendientes";
            this.btnPendientes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPendientes.UseVisualStyleBackColor = false;
            this.btnPendientes.Click += new System.EventHandler(this.btnPendientes_Click);
            // 
            // btnAceptadas
            // 
            this.btnAceptadas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnAceptadas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAceptadas.FlatAppearance.BorderSize = 0;
            this.btnAceptadas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceptadas.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnAceptadas.Location = new System.Drawing.Point(0, 0);
            this.btnAceptadas.Name = "btnAceptadas";
            this.btnAceptadas.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnAceptadas.Size = new System.Drawing.Size(150, 23);
            this.btnAceptadas.TabIndex = 2;
            this.btnAceptadas.Text = "Aceptadas";
            this.btnAceptadas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAceptadas.UseVisualStyleBackColor = false;
            this.btnAceptadas.Click += new System.EventHandler(this.btnAceptadas_Click);
            // 
            // btnSolicitudes
            // 
            this.btnSolicitudes.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSolicitudes.FlatAppearance.BorderSize = 0;
            this.btnSolicitudes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSolicitudes.Location = new System.Drawing.Point(0, 198);
            this.btnSolicitudes.Name = "btnSolicitudes";
            this.btnSolicitudes.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnSolicitudes.Size = new System.Drawing.Size(150, 23);
            this.btnSolicitudes.TabIndex = 5;
            this.btnSolicitudes.Text = "Solicitudes";
            this.btnSolicitudes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSolicitudes.UseVisualStyleBackColor = true;
            this.btnSolicitudes.Click += new System.EventHandler(this.btnSolicitudes_Click);
            // 
            // btnCuidadores
            // 
            this.btnCuidadores.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCuidadores.FlatAppearance.BorderSize = 0;
            this.btnCuidadores.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCuidadores.Location = new System.Drawing.Point(0, 175);
            this.btnCuidadores.Name = "btnCuidadores";
            this.btnCuidadores.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnCuidadores.Size = new System.Drawing.Size(150, 23);
            this.btnCuidadores.TabIndex = 3;
            this.btnCuidadores.Text = "Cuidadores";
            this.btnCuidadores.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCuidadores.UseVisualStyleBackColor = true;
            this.btnCuidadores.Click += new System.EventHandler(this.btnCuidadores_Click);
            // 
            // pnlAnimales
            // 
            this.pnlAnimales.Controls.Add(this.btnSinAdoptar);
            this.pnlAnimales.Controls.Add(this.btnAdoptados);
            this.pnlAnimales.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlAnimales.Location = new System.Drawing.Point(0, 123);
            this.pnlAnimales.Name = "pnlAnimales";
            this.pnlAnimales.Size = new System.Drawing.Size(150, 52);
            this.pnlAnimales.TabIndex = 2;
            // 
            // btnSinAdoptar
            // 
            this.btnSinAdoptar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnSinAdoptar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSinAdoptar.FlatAppearance.BorderSize = 0;
            this.btnSinAdoptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSinAdoptar.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnSinAdoptar.Location = new System.Drawing.Point(0, 23);
            this.btnSinAdoptar.Name = "btnSinAdoptar";
            this.btnSinAdoptar.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnSinAdoptar.Size = new System.Drawing.Size(150, 23);
            this.btnSinAdoptar.TabIndex = 3;
            this.btnSinAdoptar.Text = "Sin Adoptar";
            this.btnSinAdoptar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSinAdoptar.UseVisualStyleBackColor = false;
            this.btnSinAdoptar.Click += new System.EventHandler(this.btnSinAdoptar_Click);
            // 
            // btnAdoptados
            // 
            this.btnAdoptados.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnAdoptados.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAdoptados.FlatAppearance.BorderSize = 0;
            this.btnAdoptados.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAdoptados.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btnAdoptados.Location = new System.Drawing.Point(0, 0);
            this.btnAdoptados.Name = "btnAdoptados";
            this.btnAdoptados.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btnAdoptados.Size = new System.Drawing.Size(150, 23);
            this.btnAdoptados.TabIndex = 2;
            this.btnAdoptados.Text = "Adoptados";
            this.btnAdoptados.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdoptados.UseVisualStyleBackColor = false;
            this.btnAdoptados.Click += new System.EventHandler(this.btnAdoptados_Click);
            // 
            // btnAnimales
            // 
            this.btnAnimales.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnAnimales.FlatAppearance.BorderSize = 0;
            this.btnAnimales.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnimales.Location = new System.Drawing.Point(0, 100);
            this.btnAnimales.Name = "btnAnimales";
            this.btnAnimales.Padding = new System.Windows.Forms.Padding(15, 0, 0, 0);
            this.btnAnimales.Size = new System.Drawing.Size(150, 23);
            this.btnAnimales.TabIndex = 1;
            this.btnAnimales.Text = "Animales";
            this.btnAnimales.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAnimales.UseVisualStyleBackColor = true;
            this.btnAnimales.Click += new System.EventHandler(this.btnAnimales_Click);
            // 
            // pnlLogo
            // 
            this.pnlLogo.Controls.Add(this.picLogo);
            this.pnlLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlLogo.Location = new System.Drawing.Point(0, 0);
            this.pnlLogo.Margin = new System.Windows.Forms.Padding(5);
            this.pnlLogo.Name = "pnlLogo";
            this.pnlLogo.Size = new System.Drawing.Size(150, 100);
            this.pnlLogo.TabIndex = 0;
            // 
            // picLogo
            // 
            this.picLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.picLogo.Location = new System.Drawing.Point(0, 0);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(150, 100);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picLogo.TabIndex = 0;
            this.picLogo.TabStop = false;
            this.picLogo.Click += new System.EventHandler(this.picLogo_Click);
            this.picLogo.MouseEnter += new System.EventHandler(this.picLogo_MouseEnter);
            // 
            // frmRefugioLogueado
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 411);
            this.Controls.Add(this.pnlFormulario);
            this.Controls.Add(this.pnlMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRefugioLogueado";
            this.Text = "TuOjitoDerecho";
            this.pnlMenu.ResumeLayout(false);
            this.pnlSolicitudes.ResumeLayout(false);
            this.pnlAnimales.ResumeLayout(false);
            this.pnlLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlFormulario;
        private System.Windows.Forms.Panel pnlMenu;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Panel pnlSolicitudes;
        private System.Windows.Forms.Button btnPendientes;
        private System.Windows.Forms.Button btnAceptadas;
        private System.Windows.Forms.Button btnSolicitudes;
        private System.Windows.Forms.Button btnCuidadores;
        private System.Windows.Forms.Panel pnlAnimales;
        private System.Windows.Forms.Button btnSinAdoptar;
        private System.Windows.Forms.Button btnAdoptados;
        private System.Windows.Forms.Button btnAnimales;
        private System.Windows.Forms.Panel pnlLogo;
        private System.Windows.Forms.PictureBox picLogo;
    }
}