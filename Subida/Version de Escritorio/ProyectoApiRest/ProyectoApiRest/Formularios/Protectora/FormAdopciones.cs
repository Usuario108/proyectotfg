﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Protectora
{
    public partial class frmAdopciones : Form
    {
        private Negocio _negocio;

        private bool _aceptadas;

        private int _idRefugio;

        private Adopcion _solicitud;

        public frmAdopciones(Negocio n, bool aceptadas, int idRefugio)
        {
            InitializeComponent();
            _negocio = n;
            _aceptadas = aceptadas;
            _idRefugio = idRefugio;
            dtpSolicitud.Enabled = false;

            dtpInicio.Format = DateTimePickerFormat.Custom;
            dtpInicio.CustomFormat = " ";
            dtpFin.Format = DateTimePickerFormat.Custom;
            dtpFin.CustomFormat = " ";

            btnEliminar.Enabled = false;
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Adopcion> lista;
            if (_aceptadas)
            {
                lista = await _negocio.ObtenerAdopcionesConfirmadas(_idRefugio);
            }
            else
            {
                lista = await _negocio.ObtenerAdopcionesPendientes(_idRefugio);
            }

            lvwSolicitudes.Items.Clear();
            foreach (Adopcion sol in lista)
            {
                ListViewItem item = new ListViewItem(new string[] { await _negocio.ObtenerNombreUsuarioPorId(sol.userid), await _negocio.ObtenerNombreMascotaPorId(sol.mascotaid), sol.solicitudfec.ToString() });
                item.Tag = sol;
                lvwSolicitudes.Items.Add(item);
            }
        }

        private void dtpInicio_ValueChanged(object sender, EventArgs e)
        {
            dtpInicio.Format = DateTimePickerFormat.Custom;
            dtpInicio.CustomFormat = "dd/MM/yyyy";
        }

        private void dtpFin_ValueChanged(object sender, EventArgs e)
        {
            dtpFin.Format = DateTimePickerFormat.Custom;
            dtpFin.CustomFormat = "dd/MM/yyyy";
        }

        private async void lvwSolicitudes_DoubleClick(object sender, EventArgs e)
        {
            if (lvwSolicitudes.SelectedItems.Count == 1)
            {
                _solicitud = (Adopcion)lvwSolicitudes.SelectedItems[0].Tag;
                Usuario dueno = await _negocio.ObtenerUsuario(_solicitud.userid);
                Mascota mas = await _negocio.ObtenerMascota(_solicitud.mascotaid);

                txtNombreDu.Text = dueno.nombre;
                txtTelefono.Text = dueno.telefono;
                txtCorreo.Text = dueno.correo;
                txtNombreMas.Text = mas.nombre;
                txtRaza.Text = mas.raza;
                txtPeso.Text = mas.peso.ToString();

                DateTime fechaHora = DateTime.Parse(_solicitud.solicitudfec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                dtpSolicitud.Value = fechaHora;

                if (_solicitud.iniciofec != null)
                {
                    dtpInicio.Format = DateTimePickerFormat.Custom;
                    dtpInicio.CustomFormat = "dd/MM/yyyy";
                    DateTime fechaInicio = DateTime.Parse(_solicitud.iniciofec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    dtpInicio.Value = fechaInicio;
                }

                if (_solicitud.finfec != null)
                {
                    dtpFin.Format = DateTimePickerFormat.Custom;
                    dtpFin.CustomFormat = "dd/MM/yyyy";
                    DateTime fechaFin = DateTime.Parse(_solicitud.finfec, null, System.Globalization.DateTimeStyles.RoundtripKind);
                    dtpFin.Value = fechaFin;
                }

                btnEliminar.Enabled = true;
            }
        }

        private void lvwSolicitudes_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwSolicitudes.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwSolicitudes.Sort();
        }

        private bool ValidarFormulario()
        {
            if (dtpFin.CustomFormat == "dd/MM/yyyy" && dtpInicio.CustomFormat == " ")
            {
                MessageBox.Show("La fecha de inicio no puede ser nula si hay establecida una fecha de fin.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpFin.CustomFormat == "dd/MM/yyyy" && dtpInicio.CustomFormat == "dd/MM/yyyy" && dtpInicio.Value > dtpFin.Value)
            {
                MessageBox.Show("La fecha de fin de la adopcion no puede ser previa a la fecha de inicio.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario() && _solicitud != null)
            {
                _solicitud.iniciofec = null;
                _solicitud.finfec = null;

                if (dtpInicio.CustomFormat == "dd/MM/yyyy")
                {
                    DateTime fechaParseada = DateTime.ParseExact(dtpInicio.Value.ToShortDateString(), "dd/MM/yyyy", null);
                    _solicitud.iniciofec = fechaParseada.ToString("yyyy-MM-dd");
                }
                if (dtpFin.CustomFormat == "dd/MM/yyyy")
                {
                    DateTime fechaParseada = DateTime.ParseExact(dtpFin.Value.ToShortDateString(), "dd/MM/yyyy", null);
                    _solicitud.finfec = fechaParseada.ToString("yyyy-MM-dd");
                }

                await _negocio.ModificarAdopcion(_solicitud);
                CargarLista();

                txtNombreDu.Text = null;
                txtTelefono.Text = null;
                txtCorreo.Text = null;
                txtNombreMas.Text = null;
                txtRaza.Text = null;
                txtPeso.Text = null;
                dtpSolicitud.Value = DateTime.Now;
                dtpInicio.Format = DateTimePickerFormat.Custom;
                dtpInicio.CustomFormat = " ";
                dtpFin.Format = DateTimePickerFormat.Custom;
                dtpFin.CustomFormat = " ";
            }
        }

        private async void btnEliminar_Click(object sender, EventArgs e)
        {
            DialogResult dg = MessageBox.Show("¿Esta seguro que desea eliminar esta adopcion?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (dg == DialogResult.OK)
            {
                await _negocio.EliminarAdopcion(_solicitud.id);
                CargarLista();

                txtNombreDu.Text = null;
                txtTelefono.Text = null;
                txtCorreo.Text = null;
                txtNombreMas.Text = null;
                txtRaza.Text = null;
                txtPeso.Text = null;
                dtpSolicitud.Value = DateTime.Now;
                dtpInicio.Format = DateTimePickerFormat.Custom;
                dtpInicio.CustomFormat = " ";
                dtpFin.Format = DateTimePickerFormat.Custom;
                dtpFin.CustomFormat = " ";
            }
        }
    }
}
