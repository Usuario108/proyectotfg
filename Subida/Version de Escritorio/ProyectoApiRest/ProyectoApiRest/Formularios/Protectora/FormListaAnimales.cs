﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using ProyectoApiRest.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Protectora
{
    public partial class frmListaAnimales : Form
    {
        private Negocio _negocio;

        private bool _adoptados;

        private int _idRefugio;

        private Mascota _mascota;

        public frmListaAnimales(Negocio n, bool adoptados, int idRefugio)
        {
            InitializeComponent();
            _negocio = n;
            _adoptados = adoptados;
            _idRefugio = idRefugio;
            CargarLista();
        }

        private async void CargarLista()
        {
            List<Mascota> lista;
            if (_adoptados)
            {
                lista = await _negocio.ObtenerMascotasDeRefugioAdoptadas(_idRefugio);
            }
            else
            {
                lista = await _negocio.ObtenerMascotasDeRefugioSinAdoptar(_idRefugio);
            }

            lvwAnimales.Items.Clear();
            foreach (Mascota m in lista)
            {
                DateTime fechaHora = DateTime.Parse(m.nacimiento,null,System.Globalization.DateTimeStyles.RoundtripKind);

                ListViewItem item = new ListViewItem(new string[] { m.nombre, m.raza, fechaHora.ToString() });
                item.Tag = m.id;
                lvwAnimales.Items.Add(item);
            }
        }

        private void txtPeso_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private async void lvwAnimales_DoubleClick(object sender, EventArgs e)
        {
            if (lvwAnimales.SelectedItems.Count == 1)
            {
                int id = (int)lvwAnimales.SelectedItems[0].Tag;
                _mascota = await _negocio.ObtenerMascota(id);
                txtNombre.Text = _mascota.nombre;
                txtRaza.Text = _mascota.raza;
                txtDescripcion.Text = _mascota.descripcion;
                txtPeso.Text = _mascota.peso.ToString();
                DateTime fechaHora = DateTime.Parse(_mascota.nacimiento, null, System.Globalization.DateTimeStyles.RoundtripKind);
                dtpFechaNacimiento.Value = fechaHora;
                picFoto.Image = _negocio.ByteToImage(_mascota.imagen);
            }
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult rs = ofd.ShowDialog();
            if (rs == DialogResult.OK)
            {
                picFoto.Image = Image.FromFile(ofd.FileName);
            }
        }

        private void cmsAnimales_Opening(object sender, CancelEventArgs e)
        {
            tsmiEliminar.Enabled = false;
            if (lvwAnimales.SelectedItems.Count == 1 && !_adoptados)
            {
                tsmiEliminar.Enabled = true;
            }
        }

        private void lvwAnimales_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            lvwAnimales.ListViewItemSorter = new ListViewItemComparer(e.Column);
            lvwAnimales.Sort();
        }

        private async void tsmiNuevo_Click(object sender, EventArgs e)
        {
            Mascota mascota = new Mascota();
            frmNuevoAnimal form = new frmNuevoAnimal(_negocio, mascota);
            DialogResult dg = form.ShowDialog();

            if (dg == DialogResult.OK)
            {
                mascota.refugioid = _idRefugio;
                await _negocio.InsertarMascota(mascota);
                CargarLista();
            }
        }

        private async void tsmiEliminar_Click(object sender, EventArgs e)
        {
            int idMascota = (int)lvwAnimales.SelectedItems[0].Tag;
            DialogResult dg = MessageBox.Show("¿Esta seguro que desea eliminar este animal?", "Advertencia", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);

            if (dg == DialogResult.OK)
            {
                await _negocio.EliminarMascota(idMascota);
                CargarLista();

                txtNombre.Text = null;
                txtRaza.Text = null;
                txtDescripcion.Text = null;
                txtPeso.Text = null;
                dtpFechaNacimiento.Value = DateTime.Now;
                picFoto.Image = Resources.im_mascota;
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtRaza.Text))
            {
                MessageBox.Show("La raza no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtPeso.Text))
            {
                MessageBox.Show("El peso no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpFechaNacimiento.Value > DateTime.Now)
            {
                MessageBox.Show("La fecha no puede ser posterior al dia de hoy.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario() && _mascota != null)
            {
                _mascota.nombre = txtNombre.Text;
                DateTime fechaParseada = DateTime.ParseExact(dtpFechaNacimiento.Value.ToShortDateString(), "dd/MM/yyyy", null);
                _mascota.nacimiento = fechaParseada.ToString("yyyy-MM-dd");
                _mascota.raza = txtRaza.Text;
                _mascota.peso = Convert.ToSingle(txtPeso.Text);
                _mascota.descripcion = txtDescripcion.Text;
                _mascota.imagen = null;
                _mascota.imagen = _negocio.ImageToByteArray(picFoto.Image);

                await _negocio.ModificarMascota(_mascota);
                CargarLista();

                txtNombre.Text = null;
                txtRaza.Text = null;
                txtPeso.Text = null;
                txtDescripcion.Text = null;
                dtpFechaNacimiento.Value = DateTime.Now;
                picFoto.Image = Resources.im_mascota;
            }
        }
    }
}
