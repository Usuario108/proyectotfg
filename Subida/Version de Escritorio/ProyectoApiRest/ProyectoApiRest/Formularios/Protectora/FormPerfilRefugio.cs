﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Protectora
{
    public partial class frmPerfilRefugio : Form
    {
        private Negocio _negocio;

        private Refugio _refugio;

        public frmPerfilRefugio(Negocio n, Refugio refu)
        {
            InitializeComponent();
            _refugio = refu;
            _negocio = n;

            txtCorreo.Text = _refugio.correo;
            txtNombre.Text = _refugio.nombre;
            txtTelefono.Text = _refugio.telefono;
            txtDireccion.Text = _refugio.direccion;
            txtMaxAnimales.Text = _refugio.maximoanimales.ToString();
            picFoto.Image = _negocio.ByteToImage(_refugio.imagen);
            cargarLista();
        }

        public async void cargarLista()
        {
            List<Usuario> usuarios = await _negocio.ObtenerUsuariosDeRefugio(_refugio.id);
            foreach (Usuario us in usuarios)
            {
                cmbResponsable.Items.Add(us.id + "-" + us.nombre);
                if (_refugio.responsableid == us.id)
                {
                    int indice = cmbResponsable.Items.Count - 1;
                    cmbResponsable.SelectedIndex = indice;
                }
            }
            cmbResponsable.Tag = _refugio.responsableid;
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtMaxAnimales_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnFoto_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult rs = ofd.ShowDialog();
            if (rs == DialogResult.OK)
            {
                picFoto.Image = Image.FromFile(ofd.FileName);
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtTelefono.Text))
            {
                MessageBox.Show("El telefono no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtDireccion.Text))
            {
                MessageBox.Show("La direccion no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtMaxAnimales.Text))
            {
                MessageBox.Show("El maximo de animales no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                _refugio.nombre = txtNombre.Text;
                _refugio.direccion = txtDireccion.Text;
                _refugio.telefono = txtTelefono.Text;
                _refugio.maximoanimales = Convert.ToInt32(txtMaxAnimales.Text);
                _refugio.imagen = _negocio.ImageToByteArray(picFoto.Image);

                int index = cmbResponsable.SelectedItem.ToString().IndexOf("-");
                string id = cmbResponsable.SelectedItem.ToString().Substring(0, index);
                _refugio.responsableid = Convert.ToInt32(id);

                await _negocio.ModificarRefugio(_refugio);
            }
        }
    }
}
