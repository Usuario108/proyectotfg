﻿namespace ProyectoApiRest.Formularios.Login
{
    partial class frmSinCuenta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSinCuenta));
            this.lblInformacion = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.tabRegistro = new System.Windows.Forms.TabControl();
            this.tpRefugio = new System.Windows.Forms.TabPage();
            this.txtResponsable = new System.Windows.Forms.TextBox();
            this.btnResponsable = new System.Windows.Forms.Button();
            this.txtMaxAnimales = new System.Windows.Forms.TextBox();
            this.lblMaxAnimales = new System.Windows.Forms.Label();
            this.tpUsuario = new System.Windows.Forms.TabPage();
            this.dtNacimiento = new System.Windows.Forms.DateTimePicker();
            this.lblFecha = new System.Windows.Forms.Label();
            this.lblDireccion = new System.Windows.Forms.Label();
            this.txtDireccion = new System.Windows.Forms.TextBox();
            this.lblTelefono = new System.Windows.Forms.Label();
            this.txtTelefono = new System.Windows.Forms.TextBox();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblConfirmacion = new System.Windows.Forms.Label();
            this.txtConfirmacion = new System.Windows.Forms.TextBox();
            this.lblDatos = new System.Windows.Forms.Label();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblCorreo = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtCorreo = new System.Windows.Forms.TextBox();
            this.tabRegistro.SuspendLayout();
            this.tpRefugio.SuspendLayout();
            this.tpUsuario.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblInformacion
            // 
            this.lblInformacion.AutoSize = true;
            this.lblInformacion.Location = new System.Drawing.Point(189, 177);
            this.lblInformacion.Name = "lblInformacion";
            this.lblInformacion.Size = new System.Drawing.Size(208, 13);
            this.lblInformacion.TabIndex = 69;
            this.lblInformacion.Text = "¿Se registra como usuario o como refugio?";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(305, 333);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 85;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(402, 333);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 84;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // tabRegistro
            // 
            this.tabRegistro.Controls.Add(this.tpRefugio);
            this.tabRegistro.Controls.Add(this.tpUsuario);
            this.tabRegistro.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabRegistro.Location = new System.Drawing.Point(188, 193);
            this.tabRegistro.Name = "tabRegistro";
            this.tabRegistro.SelectedIndex = 0;
            this.tabRegistro.Size = new System.Drawing.Size(289, 118);
            this.tabRegistro.TabIndex = 83;
            // 
            // tpRefugio
            // 
            this.tpRefugio.Controls.Add(this.txtResponsable);
            this.tpRefugio.Controls.Add(this.btnResponsable);
            this.tpRefugio.Controls.Add(this.txtMaxAnimales);
            this.tpRefugio.Controls.Add(this.lblMaxAnimales);
            this.tpRefugio.Location = new System.Drawing.Point(4, 25);
            this.tpRefugio.Name = "tpRefugio";
            this.tpRefugio.Padding = new System.Windows.Forms.Padding(3);
            this.tpRefugio.Size = new System.Drawing.Size(281, 89);
            this.tpRefugio.TabIndex = 0;
            this.tpRefugio.Text = "Refugio";
            this.tpRefugio.UseVisualStyleBackColor = true;
            // 
            // txtResponsable
            // 
            this.txtResponsable.Location = new System.Drawing.Point(152, 50);
            this.txtResponsable.Name = "txtResponsable";
            this.txtResponsable.ReadOnly = true;
            this.txtResponsable.Size = new System.Drawing.Size(100, 23);
            this.txtResponsable.TabIndex = 3;
            // 
            // btnResponsable
            // 
            this.btnResponsable.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnResponsable.Location = new System.Drawing.Point(9, 45);
            this.btnResponsable.Name = "btnResponsable";
            this.btnResponsable.Size = new System.Drawing.Size(116, 33);
            this.btnResponsable.TabIndex = 2;
            this.btnResponsable.Text = "Crear Responsable";
            this.btnResponsable.UseVisualStyleBackColor = true;
            this.btnResponsable.Click += new System.EventHandler(this.btnResponsable_Click);
            // 
            // txtMaxAnimales
            // 
            this.txtMaxAnimales.Location = new System.Drawing.Point(152, 16);
            this.txtMaxAnimales.Name = "txtMaxAnimales";
            this.txtMaxAnimales.Size = new System.Drawing.Size(47, 23);
            this.txtMaxAnimales.TabIndex = 1;
            this.txtMaxAnimales.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMaxAnimales_KeyPress);
            // 
            // lblMaxAnimales
            // 
            this.lblMaxAnimales.AutoSize = true;
            this.lblMaxAnimales.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxAnimales.Location = new System.Drawing.Point(6, 19);
            this.lblMaxAnimales.Name = "lblMaxAnimales";
            this.lblMaxAnimales.Size = new System.Drawing.Size(91, 15);
            this.lblMaxAnimales.TabIndex = 0;
            this.lblMaxAnimales.Text = "Max. Animales:";
            // 
            // tpUsuario
            // 
            this.tpUsuario.Controls.Add(this.dtNacimiento);
            this.tpUsuario.Controls.Add(this.lblFecha);
            this.tpUsuario.Location = new System.Drawing.Point(4, 25);
            this.tpUsuario.Name = "tpUsuario";
            this.tpUsuario.Padding = new System.Windows.Forms.Padding(3);
            this.tpUsuario.Size = new System.Drawing.Size(281, 89);
            this.tpUsuario.TabIndex = 1;
            this.tpUsuario.Text = "Usuario";
            this.tpUsuario.UseVisualStyleBackColor = true;
            // 
            // dtNacimiento
            // 
            this.dtNacimiento.Location = new System.Drawing.Point(9, 42);
            this.dtNacimiento.Name = "dtNacimiento";
            this.dtNacimiento.Size = new System.Drawing.Size(257, 23);
            this.dtNacimiento.TabIndex = 20;
            // 
            // lblFecha
            // 
            this.lblFecha.AutoSize = true;
            this.lblFecha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFecha.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblFecha.Location = new System.Drawing.Point(6, 9);
            this.lblFecha.Name = "lblFecha";
            this.lblFecha.Size = new System.Drawing.Size(124, 15);
            this.lblFecha.TabIndex = 19;
            this.lblFecha.Text = "Fecha de Nacimiento";
            // 
            // lblDireccion
            // 
            this.lblDireccion.AutoSize = true;
            this.lblDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDireccion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblDireccion.Location = new System.Drawing.Point(23, 114);
            this.lblDireccion.Name = "lblDireccion";
            this.lblDireccion.Size = new System.Drawing.Size(59, 15);
            this.lblDireccion.TabIndex = 82;
            this.lblDireccion.Text = "Direccion";
            // 
            // txtDireccion
            // 
            this.txtDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDireccion.Location = new System.Drawing.Point(26, 134);
            this.txtDireccion.Name = "txtDireccion";
            this.txtDireccion.Size = new System.Drawing.Size(294, 23);
            this.txtDireccion.TabIndex = 81;
            // 
            // lblTelefono
            // 
            this.lblTelefono.AutoSize = true;
            this.lblTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTelefono.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblTelefono.Location = new System.Drawing.Point(23, 249);
            this.lblTelefono.Name = "lblTelefono";
            this.lblTelefono.Size = new System.Drawing.Size(55, 15);
            this.lblTelefono.TabIndex = 80;
            this.lblTelefono.Text = "Telefono";
            // 
            // txtTelefono
            // 
            this.txtTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTelefono.Location = new System.Drawing.Point(26, 269);
            this.txtTelefono.Name = "txtTelefono";
            this.txtTelefono.Size = new System.Drawing.Size(133, 23);
            this.txtTelefono.TabIndex = 79;
            this.txtTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTelefono_KeyPress);
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblNombre.Location = new System.Drawing.Point(23, 178);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(52, 15);
            this.lblNombre.TabIndex = 78;
            this.lblNombre.Text = "Nombre";
            // 
            // txtNombre
            // 
            this.txtNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombre.Location = new System.Drawing.Point(26, 198);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(133, 23);
            this.txtNombre.TabIndex = 77;
            // 
            // lblConfirmacion
            // 
            this.lblConfirmacion.AutoSize = true;
            this.lblConfirmacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConfirmacion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblConfirmacion.Location = new System.Drawing.Point(344, 114);
            this.lblConfirmacion.Name = "lblConfirmacion";
            this.lblConfirmacion.Size = new System.Drawing.Size(127, 15);
            this.lblConfirmacion.TabIndex = 76;
            this.lblConfirmacion.Text = "Confirmar Contraseña";
            // 
            // txtConfirmacion
            // 
            this.txtConfirmacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtConfirmacion.Location = new System.Drawing.Point(347, 134);
            this.txtConfirmacion.Name = "txtConfirmacion";
            this.txtConfirmacion.Size = new System.Drawing.Size(133, 23);
            this.txtConfirmacion.TabIndex = 75;
            // 
            // lblDatos
            // 
            this.lblDatos.AutoSize = true;
            this.lblDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatos.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblDatos.Location = new System.Drawing.Point(23, 20);
            this.lblDatos.Name = "lblDatos";
            this.lblDatos.Size = new System.Drawing.Size(117, 17);
            this.lblDatos.TabIndex = 74;
            this.lblDatos.Text = "Datos de registro";
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPassword.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblPassword.Location = new System.Drawing.Point(344, 56);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(70, 15);
            this.lblPassword.TabIndex = 73;
            this.lblPassword.Text = "Contraseña";
            // 
            // lblCorreo
            // 
            this.lblCorreo.AutoSize = true;
            this.lblCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorreo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblCorreo.Location = new System.Drawing.Point(26, 53);
            this.lblCorreo.Name = "lblCorreo";
            this.lblCorreo.Size = new System.Drawing.Size(44, 15);
            this.lblCorreo.TabIndex = 72;
            this.lblCorreo.Text = "Correo";
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(347, 76);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(133, 23);
            this.txtPassword.TabIndex = 71;
            // 
            // txtCorreo
            // 
            this.txtCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCorreo.Location = new System.Drawing.Point(26, 76);
            this.txtCorreo.Name = "txtCorreo";
            this.txtCorreo.Size = new System.Drawing.Size(294, 23);
            this.txtCorreo.TabIndex = 70;
            // 
            // frmSinCuenta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 369);
            this.Controls.Add(this.lblInformacion);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.tabRegistro);
            this.Controls.Add(this.lblDireccion);
            this.Controls.Add(this.txtDireccion);
            this.Controls.Add(this.lblTelefono);
            this.Controls.Add(this.txtTelefono);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.txtNombre);
            this.Controls.Add(this.lblConfirmacion);
            this.Controls.Add(this.txtConfirmacion);
            this.Controls.Add(this.lblDatos);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.lblCorreo);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.txtCorreo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmSinCuenta";
            this.Text = "TuOjitoDerecho";
            this.tabRegistro.ResumeLayout(false);
            this.tpRefugio.ResumeLayout(false);
            this.tpRefugio.PerformLayout();
            this.tpUsuario.ResumeLayout(false);
            this.tpUsuario.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInformacion;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.TabControl tabRegistro;
        private System.Windows.Forms.TabPage tpRefugio;
        private System.Windows.Forms.TextBox txtResponsable;
        private System.Windows.Forms.Button btnResponsable;
        private System.Windows.Forms.TextBox txtMaxAnimales;
        private System.Windows.Forms.Label lblMaxAnimales;
        private System.Windows.Forms.TabPage tpUsuario;
        private System.Windows.Forms.DateTimePicker dtNacimiento;
        private System.Windows.Forms.Label lblFecha;
        private System.Windows.Forms.Label lblDireccion;
        private System.Windows.Forms.TextBox txtDireccion;
        private System.Windows.Forms.Label lblTelefono;
        private System.Windows.Forms.TextBox txtTelefono;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblConfirmacion;
        private System.Windows.Forms.TextBox txtConfirmacion;
        private System.Windows.Forms.Label lblDatos;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.Label lblCorreo;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtCorreo;
    }
}