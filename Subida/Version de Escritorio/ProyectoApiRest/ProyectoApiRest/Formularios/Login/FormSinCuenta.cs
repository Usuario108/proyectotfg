﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using ProyectoApiRest.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Login
{
    public partial class frmSinCuenta : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        private Refugio _refugio;

        public frmSinCuenta(Negocio n, Usuario usuario, Refugio refugio)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = usuario;
            _refugio = refugio;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtMaxAnimales_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnResponsable_Click(object sender, EventArgs e)
        {
            frmRegistrarResponsable formul = new frmRegistrarResponsable(_negocio);
            DialogResult dg = formul.ShowDialog();

            if (dg == DialogResult.OK)
            {
                Usuario us = formul.GetUsuario();
                txtResponsable.Text = us.nombre;
                txtResponsable.Tag = us;
            }
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                bool correoValido = await _negocio.ComprobarCorreo(txtCorreo.Text);
                if (!correoValido)
                {
                    MessageBox.Show("El correo le pertenece a otra cuenta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    if (tabRegistro.SelectedTab.Text.Equals("Usuario"))
                    {
                        _usuario.refugioid = null;
                        _usuario.nombre = txtNombre.Text;
                        DateTime fecha = DateTime.ParseExact(dtNacimiento.Value.ToShortDateString(), "dd/MM/yyyy", null);
                        string fechaFormateada = fecha.ToString("yyyy-MM-dd");
                        _usuario.nacimiento = fechaFormateada;
                        _usuario.telefono = txtTelefono.Text;
                        _usuario.correo = txtCorreo.Text;
                        _usuario.password = txtPassword.Text;
                        _usuario.direccion = txtDireccion.Text;
                        _usuario.imagen = _negocio.ImageToByteArray(Resources.im_usuario);

                        DialogResult = DialogResult.OK;
                    }
                    else
                    {
                        _refugio.nombre = txtNombre.Text;
                        _refugio.direccion = txtDireccion.Text;
                        _refugio.telefono = txtTelefono.Text;
                        _refugio.correo = txtCorreo.Text;
                        _refugio.password = txtPassword.Text;
                        _refugio.maximoanimales = Convert.ToInt32(txtMaxAnimales.Text);
                        _refugio.imagen = _negocio.ImageToByteArray(Resources.im_refugio);

                        Usuario userAux = (Usuario)txtResponsable.Tag;
                        _usuario.nombre = userAux.nombre;
                        _usuario.nacimiento = userAux.nacimiento;
                        _usuario.telefono = userAux.telefono;
                        _usuario.correo = userAux.correo;
                        _usuario.password = userAux.password;
                        _usuario.direccion = userAux.direccion;
                        _usuario.imagen = _negocio.ImageToByteArray(Resources.im_usuario);

                        DialogResult = DialogResult.OK;
                    }
                }
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtCorreo.Text))
            {
                MessageBox.Show("El correo no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtDireccion.Text))
            {
                MessageBox.Show("La direccion no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtTelefono.Text))
            {
                MessageBox.Show("El telefono no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtPassword.Text) || string.IsNullOrEmpty(txtConfirmacion.Text))
            {
                MessageBox.Show("La contraseña no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!txtPassword.Text.Equals(txtConfirmacion.Text))
            {
                MessageBox.Show("La contraseña no coincide.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (tabRegistro.SelectedTab.Text.Equals("Usuario"))
            {
                if (dtNacimiento.Value >= DateTime.Now)
                {
                    MessageBox.Show("La fecha no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                int difFechas = DateTime.Now.Year - dtNacimiento.Value.Year;
                if (difFechas < 18)
                {
                    MessageBox.Show("No puede registrarse menores de 18 años.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            else
            {
                if (string.IsNullOrEmpty(txtMaxAnimales.Text))
                {
                    MessageBox.Show("El numero de animales no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (string.IsNullOrEmpty(txtResponsable.Text))
                {
                    MessageBox.Show("El refugio no tiene responsable.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }
    }
}
