﻿namespace ProyectoApiRest.Formularios.Login
{
    partial class frmRegistrarResponsable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegistrarResponsable));
            this.lblDatos = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.lblResDireccion = new System.Windows.Forms.Label();
            this.dtpResNacimiento = new System.Windows.Forms.DateTimePicker();
            this.txtResDireccion = new System.Windows.Forms.TextBox();
            this.txtResCorreo = new System.Windows.Forms.TextBox();
            this.lblResTelefono = new System.Windows.Forms.Label();
            this.lblResCorreo = new System.Windows.Forms.Label();
            this.txtResTelefono = new System.Windows.Forms.TextBox();
            this.txtResPassword = new System.Windows.Forms.TextBox();
            this.lblResNombre = new System.Windows.Forms.Label();
            this.lblResPassword = new System.Windows.Forms.Label();
            this.txtResNombre = new System.Windows.Forms.TextBox();
            this.lblResNacimiento = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblDatos
            // 
            this.lblDatos.AutoSize = true;
            this.lblDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDatos.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblDatos.Location = new System.Drawing.Point(25, 19);
            this.lblDatos.Name = "lblDatos";
            this.lblDatos.Size = new System.Drawing.Size(155, 17);
            this.lblDatos.TabIndex = 106;
            this.lblDatos.Text = "Datos del Responsable";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(123, 255);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 105;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(204, 255);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 104;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // lblResDireccion
            // 
            this.lblResDireccion.AutoSize = true;
            this.lblResDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResDireccion.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblResDireccion.Location = new System.Drawing.Point(28, 192);
            this.lblResDireccion.Name = "lblResDireccion";
            this.lblResDireccion.Size = new System.Drawing.Size(52, 13);
            this.lblResDireccion.TabIndex = 102;
            this.lblResDireccion.Text = "Direccion";
            // 
            // dtpResNacimiento
            // 
            this.dtpResNacimiento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpResNacimiento.Location = new System.Drawing.Point(140, 119);
            this.dtpResNacimiento.Name = "dtpResNacimiento";
            this.dtpResNacimiento.Size = new System.Drawing.Size(139, 20);
            this.dtpResNacimiento.TabIndex = 103;
            // 
            // txtResDireccion
            // 
            this.txtResDireccion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResDireccion.Location = new System.Drawing.Point(28, 212);
            this.txtResDireccion.Name = "txtResDireccion";
            this.txtResDireccion.Size = new System.Drawing.Size(251, 23);
            this.txtResDireccion.TabIndex = 101;
            // 
            // txtResCorreo
            // 
            this.txtResCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResCorreo.Location = new System.Drawing.Point(28, 66);
            this.txtResCorreo.Name = "txtResCorreo";
            this.txtResCorreo.Size = new System.Drawing.Size(251, 23);
            this.txtResCorreo.TabIndex = 92;
            // 
            // lblResTelefono
            // 
            this.lblResTelefono.AutoSize = true;
            this.lblResTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResTelefono.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblResTelefono.Location = new System.Drawing.Point(137, 146);
            this.lblResTelefono.Name = "lblResTelefono";
            this.lblResTelefono.Size = new System.Drawing.Size(49, 13);
            this.lblResTelefono.TabIndex = 100;
            this.lblResTelefono.Text = "Telefono";
            // 
            // lblResCorreo
            // 
            this.lblResCorreo.AutoSize = true;
            this.lblResCorreo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResCorreo.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblResCorreo.Location = new System.Drawing.Point(28, 50);
            this.lblResCorreo.Name = "lblResCorreo";
            this.lblResCorreo.Size = new System.Drawing.Size(38, 13);
            this.lblResCorreo.TabIndex = 94;
            this.lblResCorreo.Text = "Correo";
            // 
            // txtResTelefono
            // 
            this.txtResTelefono.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResTelefono.Location = new System.Drawing.Point(140, 166);
            this.txtResTelefono.Name = "txtResTelefono";
            this.txtResTelefono.Size = new System.Drawing.Size(139, 23);
            this.txtResTelefono.TabIndex = 99;
            this.txtResTelefono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtResTelefono_KeyPress);
            // 
            // txtResPassword
            // 
            this.txtResPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResPassword.Location = new System.Drawing.Point(28, 116);
            this.txtResPassword.Name = "txtResPassword";
            this.txtResPassword.Size = new System.Drawing.Size(95, 23);
            this.txtResPassword.TabIndex = 93;
            // 
            // lblResNombre
            // 
            this.lblResNombre.AutoSize = true;
            this.lblResNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResNombre.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblResNombre.Location = new System.Drawing.Point(25, 146);
            this.lblResNombre.Name = "lblResNombre";
            this.lblResNombre.Size = new System.Drawing.Size(44, 13);
            this.lblResNombre.TabIndex = 98;
            this.lblResNombre.Text = "Nombre";
            // 
            // lblResPassword
            // 
            this.lblResPassword.AutoSize = true;
            this.lblResPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResPassword.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblResPassword.Location = new System.Drawing.Point(25, 96);
            this.lblResPassword.Name = "lblResPassword";
            this.lblResPassword.Size = new System.Drawing.Size(61, 13);
            this.lblResPassword.TabIndex = 95;
            this.lblResPassword.Text = "Contraseña";
            // 
            // txtResNombre
            // 
            this.txtResNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtResNombre.Location = new System.Drawing.Point(28, 166);
            this.txtResNombre.Name = "txtResNombre";
            this.txtResNombre.Size = new System.Drawing.Size(95, 23);
            this.txtResNombre.TabIndex = 97;
            // 
            // lblResNacimiento
            // 
            this.lblResNacimiento.AutoSize = true;
            this.lblResNacimiento.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResNacimiento.ForeColor = System.Drawing.SystemColors.WindowText;
            this.lblResNacimiento.Location = new System.Drawing.Point(133, 96);
            this.lblResNacimiento.Name = "lblResNacimiento";
            this.lblResNacimiento.Size = new System.Drawing.Size(108, 13);
            this.lblResNacimiento.TabIndex = 96;
            this.lblResNacimiento.Text = "Fecha de Nacimiento";
            // 
            // frmRegistrarResponsable
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 312);
            this.Controls.Add(this.lblDatos);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.lblResDireccion);
            this.Controls.Add(this.dtpResNacimiento);
            this.Controls.Add(this.txtResDireccion);
            this.Controls.Add(this.txtResCorreo);
            this.Controls.Add(this.lblResTelefono);
            this.Controls.Add(this.lblResCorreo);
            this.Controls.Add(this.txtResTelefono);
            this.Controls.Add(this.txtResPassword);
            this.Controls.Add(this.lblResNombre);
            this.Controls.Add(this.lblResPassword);
            this.Controls.Add(this.txtResNombre);
            this.Controls.Add(this.lblResNacimiento);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRegistrarResponsable";
            this.Text = "TuOjitoDerecho";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblDatos;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Label lblResDireccion;
        private System.Windows.Forms.DateTimePicker dtpResNacimiento;
        private System.Windows.Forms.TextBox txtResDireccion;
        private System.Windows.Forms.TextBox txtResCorreo;
        private System.Windows.Forms.Label lblResTelefono;
        private System.Windows.Forms.Label lblResCorreo;
        private System.Windows.Forms.TextBox txtResTelefono;
        private System.Windows.Forms.TextBox txtResPassword;
        private System.Windows.Forms.Label lblResNombre;
        private System.Windows.Forms.Label lblResPassword;
        private System.Windows.Forms.TextBox txtResNombre;
        private System.Windows.Forms.Label lblResNacimiento;
    }
}