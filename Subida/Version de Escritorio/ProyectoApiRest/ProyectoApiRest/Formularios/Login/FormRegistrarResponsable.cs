﻿using ProyectoApiRest.Archivos;
using ProyectoApiRest.Modelo;
using ProyectoApiRest.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoApiRest.Formularios.Login
{
    public partial class frmRegistrarResponsable : Form
    {
        private Negocio _negocio;

        private Usuario _usuario;

        public frmRegistrarResponsable(Negocio n)
        {
            InitializeComponent();
            _negocio = n;
            _usuario = new Usuario();
        }

        public Usuario GetUsuario()
        {
            return _usuario;
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtResTelefono_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (ValidarFormulario())
            {
                bool correoValido = await _negocio.ComprobarCorreo(txtResCorreo.Text);
                if (!correoValido)
                {
                    MessageBox.Show("El correo le pertenece a otra cuenta.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _usuario.refugioid = null;
                    _usuario.nombre = txtResNombre.Text;
                    DateTime fecha = DateTime.ParseExact(dtpResNacimiento.Value.ToShortDateString(), "dd/MM/yyyy", null);
                    string fechaFormateada = fecha.ToString("yyyy-MM-dd");
                    _usuario.nacimiento = fechaFormateada;
                    _usuario.telefono = txtResTelefono.Text;
                    _usuario.correo = txtResCorreo.Text;
                    _usuario.password = txtResPassword.Text;
                    _usuario.direccion = txtResDireccion.Text;
                    _usuario.imagen = _negocio.ImageToByteArray(Resources.im_usuario);

                    DialogResult = DialogResult.OK;
                }
            }
        }

        private bool ValidarFormulario()
        {
            if (string.IsNullOrEmpty(txtResCorreo.Text))
            {
                MessageBox.Show("El correo no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtResPassword.Text))
            {
                MessageBox.Show("La contraseña no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (dtpResNacimiento.Value >= DateTime.Now)
            {
                MessageBox.Show("La fecha no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            int difFechas = DateTime.Now.Year - dtpResNacimiento.Value.Year;
            if (difFechas < 18)
            {
                MessageBox.Show("No puede registrarse menores de 18 años.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtResNombre.Text))
            {
                MessageBox.Show("El nombre no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtResTelefono.Text))
            {
                MessageBox.Show("El telefono no es valido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (string.IsNullOrEmpty(txtResDireccion.Text))
            {
                MessageBox.Show("La direccion no es valida.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            return true;
        }
    }
}
