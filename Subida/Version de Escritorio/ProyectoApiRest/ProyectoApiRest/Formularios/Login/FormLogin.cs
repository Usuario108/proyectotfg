﻿using Newtonsoft.Json;
using ProyectoApiRest.Archivos;
using ProyectoApiRest.Formularios.Login;
using ProyectoApiRest.Formularios.Protectora;
using ProyectoApiRest.Formularios.Persona.Dueño;
using ProyectoApiRest.Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ProyectoApiRest.Formularios.Persona.Responsable;
using ProyectoApiRest.Formularios.Persona.Cuidador;
using ProyectoApiRest.Formularios.Persona.Admin;

namespace ProyectoApiRest
{
    public partial class frmLogin : Form
    {
        private Negocio _negocio;
        public frmLogin()
        {
            InitializeComponent();
            _negocio = new Negocio();
        }

        private async void lblEnlace_Click(object sender, EventArgs e)
        {
            Usuario user = new Usuario();
            Refugio refu = new Refugio();
            frmSinCuenta formul = new frmSinCuenta(_negocio, user, refu);
            DialogResult dg = formul.ShowDialog();

            if (dg == DialogResult.OK)
            {
                if (string.IsNullOrEmpty(refu.nombre))
                {
                    await _negocio.InsertarUsuario(user);
                    frmDueñoLogueado form = new frmDueñoLogueado(_negocio, user);
                    DialogResult dgAux = form.ShowDialog();
                }
                else
                {
                    await _negocio.InsertarUsuario(user);

                    Usuario userAux = await _negocio.ObtenerUsuarioPorCorreo(user.correo);
                    refu.responsableid = userAux.id;
                    await _negocio.InsertarRefugio(refu);

                    Refugio refuAux = await _negocio.ObtenerRefugioPorCorreo(refu.correo);
                    userAux.refugioid = refuAux.id;
                    await _negocio.ModificarUsuario(userAux);

                    frmRefugioLogueado form = new frmRefugioLogueado(_negocio, refuAux);
                    DialogResult dgAux = form.ShowDialog();
                }
            }
        }

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            string correo = this.txtCorreo.Text;
            string password = this.txtPassword.Text;

            Refugio refugio = await _negocio.LoginRefugio(correo, password);
            Usuario user = await _negocio.LoginUsuario(correo, password);

            if (refugio != null)
            {
                frmRefugioLogueado formul = new frmRefugioLogueado(_negocio, refugio);
                DialogResult dg = formul.ShowDialog();
            }
            else if (user != null && user.refugioid == null)
            {
                if (user.id == 1)
                {
                    frmAdminLogueado formul = new frmAdminLogueado(_negocio, user);
                    DialogResult dg = formul.ShowDialog();
                }
                else
                {
                    frmDueñoLogueado formul = new frmDueñoLogueado(_negocio, user);
                    DialogResult dg = formul.ShowDialog();
                }
            }
            else if (user != null && user.refugioid != null)
            {
                Refugio refAux = await _negocio.ObtenerRefugio((int)user.refugioid);

                if (refAux.responsableid == user.id)
                {
                    frmResponsableLogueado formul = new frmResponsableLogueado(_negocio, user);
                    DialogResult dg = formul.ShowDialog();
                }
                else
                {
                    frmCuidadorLogueado formul = new frmCuidadorLogueado(_negocio, user);
                    DialogResult dg = formul.ShowDialog();
                }
            }
            else
            {
                MessageBox.Show("El correo o la contraseña no son validos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
