package com.rubenecheverria.tuojitoderechoservice.view.persona.responsable

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentListaTrabajadoresBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.view.protectora.CuidadorAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentListaTrabajadores : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentListaTrabajadoresBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentListaTrabajadoresBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarCuidadores(auxViewModel.usuarioLogueado.refugioid!!)

            refugioViewModel.listaCuidadores.observe(viewLifecycleOwner, Observer { usuarios ->
                var ax=usuarios.toMutableList()
                ax.remove(auxViewModel.usuarioLogueado)
                with(recyclerViewListaTrabajadores) {
                    adapter = CuidadorAdapter(ax) { usuario -> onClickTrabajador(usuario) }
                    layoutManager = LinearLayoutManager(context)
                }
            })
        }

        binding.botonFlotante.setOnClickListener { navController.navigate(R.id.action_fragmentListaTrabajadores_to_fragmentNuevoTrabajador) }
    }

    private fun onClickTrabajador(trabajador: Usuario)
    {
        val accion = FragmentListaTrabajadoresDirections.actionFragmentListaTrabajadoresToFragmentPerfilTrabajador(trabajador=trabajador)
        navController.navigate(accion)
    }
}