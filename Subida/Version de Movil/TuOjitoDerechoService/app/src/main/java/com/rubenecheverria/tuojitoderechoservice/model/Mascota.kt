package com.rubenecheverria.tuojitoderechoservice.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Mascota (
    @SerializedName("id")
    val id:Int,
    @SerializedName("refugioid")
    val refugioid:Int,
    @SerializedName("nombre")
    val nombre:String,
    @SerializedName("nacimiento")
    val nacimiento:String,
    @SerializedName("raza")
    val raza:String,
    @SerializedName("peso")
    val peso:Float,
    @SerializedName("descripcion")
    val descripcion:String?,
    @SerializedName("imagen")
    val imagen: String
) : Serializable