package com.rubenecheverria.tuojitoderechoservice.view.persona.cuidador

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentPerfilAnimalCopiaBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.time.LocalDate

class FragmentPerfilAnimalCopia : Fragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentPerfilAnimalCopiaBinding
    private var eliminacion:Boolean = true
    private lateinit var mascota: Mascota
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            eliminacion = it.get("eliminacion") as Boolean
            mascota = it.get("mascota") as Mascota
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentPerfilAnimalCopiaBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        if(eliminacion){
            binding.imageEliminar.isEnabled = true
            binding.imageEliminar.isVisible = true
        }else{
            binding.imageEliminar.isEnabled = false
            binding.imageEliminar.isVisible = false
        }

        binding.apply {
            val longblobBytes: ByteArray = Base64.decode(mascota.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            imageViewPerfil.setImageBitmap(bitmap)
            imageViewPerfil.setScaleType(ImageView.ScaleType.FIT_CENTER)

            etNombre.setText(mascota.nombre)
            etPeso.setText(mascota.peso.toString())
            etRaza.setText(mascota.raza)
            etDescripcion.setText(mascota.descripcion)

            val fec=mascota.nacimiento
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val str=ax.substring(posAx+1)
            val posStr=str.indexOf("T")
            val dia=str.substring(0,posStr)
            etFecha.setText(dia+"-"+mes.toString()+"-" +anho.toString())

            etFecha.setOnClickListener {
                context?.let { it1 -> DatePickerDialog(it1,this@FragmentPerfilAnimalCopia,anho,(mes-1),dia.toInt()).show() }
            }
            botonCancelar.setOnClickListener { navController.navigate(R.id.action_fragmentPerfilAnimalCopia_to_fragmentListaAnimalesCopia) }

            botonAceptar.setOnClickListener {
                if(ValidarFormulario()){
                    val posX=etFecha.text.toString().indexOf("-")
                    val diaX=etFecha.text.toString().substring(0,posX).toInt()
                    val axX= etFecha.text.toString().substring(posX+1)
                    val posAxX=axX.indexOf("-")
                    val mesX=axX.substring(0,posAxX).toInt()
                    val anhoX=axX.substring(posAxX+1).toInt()
                    val date= LocalDate.of(anhoX,mesX,diaX)

                    var update= Mascota(0,auxViewModel.usuarioLogueado.refugioid!!,etNombre.text.toString(),date.toString(),etRaza.text.toString(),
                        etPeso.text.toString().toFloat(),etDescripcion.text.toString(),mascota.imagen)

                    refugioViewModel.updateMascota(mascota.id, update)
                    refugioViewModel.respuestaModificarMascota.observe(viewLifecycleOwner, Observer {
                        Toast.makeText(context, "Se han modificado los datos de la mascota.", Toast.LENGTH_LONG).show()
                        navController.navigate(R.id.action_fragmentPerfilAnimalCopia_to_fragmentListaAnimalesCopia)
                    })
                }
            }

            imageEliminar.setOnClickListener{
                AlertDialog.Builder(requireContext())
                    .setTitle("Eliminacion")
                    .setMessage("¿Esta seguro de que desea eliminar a ${mascota.nombre}?")
                    .setCancelable(false)
                    .setPositiveButton(R.string.si) { _, _ ->

                        refugioViewModel.deleteMascota(mascota.id)
                        refugioViewModel.respuestaEliminarMascota.observe(viewLifecycleOwner, Observer {
                            Toast.makeText(context, "Animal Elimnado", Toast.LENGTH_SHORT).show()
                            navController.navigate(R.id.action_fragmentPerfilAnimalCopia_to_fragmentListaAnimalesCopia)
                        })
                    }
                    .setNegativeButton(R.string.no) { _, _ -> }.show()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(p0: DatePicker?, anho: Int, mes: Int, dia: Int) {
        binding.etFecha.setText(dia.toString()+"-"+(mes+1).toString()+"-"+anho.toString())
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (etNombre.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etRaza.text.toString().isEmpty())
            {
                Toast.makeText(context, "La raza no es valida.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etPeso.text.toString().isEmpty())
            {
                Toast.makeText(context, "El peso no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            val pos=etFecha.text.toString().indexOf("-")
            val dia=etFecha.text.toString().substring(0,pos).toInt()
            val ax= etFecha.text.toString().substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val anho=ax.substring(posAx+1).toInt()

            val date= LocalDate.of(anho,mes,dia)
            if (LocalDate.now() < date)
            {
                Toast.makeText(context, "La fecha no puede ser posterior al dia de hoy.", Toast.LENGTH_LONG).show()
                return false
            }

            return true
        }
    }
}