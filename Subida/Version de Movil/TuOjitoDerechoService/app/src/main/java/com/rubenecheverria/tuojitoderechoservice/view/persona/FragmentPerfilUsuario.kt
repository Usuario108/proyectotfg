package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentPerfilUsuarioBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.time.LocalDate

class FragmentPerfilUsuario : Fragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var binding: FragmentPerfilUsuarioBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels  { AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentPerfilUsuarioBinding.inflate(inflater,container,false)
        return binding.root
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            if(auxViewModel.usuarioLogueado.refugioid!=null){
                textViewRefugio.isVisible = true
                textViewRefugio.isEnabled = true
                etRefugio.isVisible = true
                etRefugio.isEnabled = true
                refugioViewModel.getRefugioPorId(auxViewModel.usuarioLogueado.refugioid!!)
                refugioViewModel.refugio.observe(viewLifecycleOwner, Observer { refugio ->
                    etRefugio.setText(refugio.nombre)
                })
            }else{
                textViewRefugio.isVisible = false
                textViewRefugio.isEnabled = false
                etRefugio.isVisible = false
                etRefugio.isEnabled = false
            }

            val longblobBytes: ByteArray = Base64.decode(auxViewModel.usuarioLogueado.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            imageViewPerfil.setImageBitmap(bitmap)
            imageViewPerfil.setScaleType(ImageView.ScaleType.FIT_CENTER)

            tvEmail.text = auxViewModel.usuarioLogueado.correo
            etNombre.setText(auxViewModel.usuarioLogueado.nombre)
            etDireccion.setText(auxViewModel.usuarioLogueado.direccion)
            etTelefono.setText(auxViewModel.usuarioLogueado.telefono)

            val fec=auxViewModel.usuarioLogueado.nacimiento
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val dia=ax.substring(posAx+1).toInt()

            etFecha.setText(dia.toString()+"-"+mes.toString()+"-" +anho.toString())
            etFecha.setOnClickListener {
                context?.let { it1 -> DatePickerDialog(it1,this@FragmentPerfilUsuario,anho,(mes-1),dia).show() }
            }


            botonAceptar.setOnClickListener {
                if(ValidarFormulario()){
                    val posX=etFecha.text.toString().indexOf("-")
                    val diaX=etFecha.text.toString().substring(0,posX).toInt()
                    val axX= etFecha.text.toString().substring(posX+1)
                    val posAxX=axX.indexOf("-")
                    val mesX=axX.substring(0,posAxX).toInt()
                    val anhoX=axX.substring(posAxX+1).toInt()
                    val fecX= anhoX.toString()+"-"+mesX+"-"+diaX


                    val update= Usuario(0,auxViewModel.usuarioLogueado.refugioid,etNombre.text.toString(),fecX,etTelefono.text.toString(),
                        auxViewModel.usuarioLogueado.correo,auxViewModel.usuarioLogueado.password,etDireccion.text.toString(),auxViewModel.usuarioLogueado.imagen)

                    refugioViewModel.updateUsuario(auxViewModel.usuarioLogueado.id, update)
                    refugioViewModel.respuestaModificarUsuario.observe(viewLifecycleOwner, Observer {
                        Toast.makeText(context, "Se han modificado los datos del usuario.", Toast.LENGTH_LONG).show()
                        val aux =auxViewModel.usuarioLogueado
                        auxViewModel.usuarioLogueado = Usuario(aux.id,aux.refugioid,etNombre.text.toString(),fecX,etTelefono.text.toString(),
                            aux.correo,aux.password,etDireccion.text.toString(),aux.imagen)
                    })
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(p0: DatePicker?, anho: Int, mes: Int, dia: Int) {
        binding.etFecha.setText(dia.toString()+"-"+(mes+1).toString()+"-"+anho.toString())
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (etNombre.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etTelefono.text.toString().isEmpty())
            {
                Toast.makeText(context, "El telefono no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etDireccion.text.toString().isEmpty())
            {
                Toast.makeText(context, "La direccion no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            val pos=etFecha.text.toString().indexOf("-")
            val dia=etFecha.text.toString().substring(0,pos).toInt()
            val ax= etFecha.text.toString().substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val anho=ax.substring(posAx+1).toInt()

            val date= LocalDate.of(anho,mes-1,dia)
            if (LocalDate.now() < date)
            {
                Toast.makeText(context, "La fecha no es valida.", Toast.LENGTH_LONG).show()
                return false
            }

            val difFechas = LocalDate.now().year - date.year
            if (difFechas < 18)
            {
                Toast.makeText(context, "No puede registrarse menores de 18 años.", Toast.LENGTH_LONG).show()
                return false
            }

            return true
        }
    }
}