package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminPerfilAdopBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminPerfilAdop : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminPerfilAdopBinding
    private lateinit var adopcion: Adopcion
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            adopcion = it.get("adopcion") as Adopcion
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminPerfilAdopBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.getUsuarioPorId(adopcion.userid)
            refugioViewModel.getMascotaPorId(adopcion.mascotaid)
            refugioViewModel.usuario.observe(viewLifecycleOwner, Observer { usu ->
                refugioViewModel.mascota.observe(viewLifecycleOwner, Observer { mas ->

                    val longblobBytes1: ByteArray = Base64.decode(usu.imagen, Base64.DEFAULT)
                    val bitmap1: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes1, 0, longblobBytes1.size)
                    imageViewPerfilUsu.setImageBitmap(bitmap1)
                    imageViewPerfilUsu.setScaleType(ImageView.ScaleType.FIT_CENTER)
                    textViewNombreUsu.text = usu.nombre
                    textViewTelefonoUsu.text = usu.telefono

                    val longblobBytes2: ByteArray = Base64.decode(mas.imagen, Base64.DEFAULT)
                    val bitmap2: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes2, 0, longblobBytes2.size)
                    imageViewPerfilMas.setImageBitmap(bitmap2)
                    imageViewPerfilMas.setScaleType(ImageView.ScaleType.FIT_CENTER)
                    textViewNombreMas.text = mas.nombre
                    textViewRazaMas.text = mas.raza

                    val fecX=devolverCadena(adopcion.solicitudfec)
                    etFechaEnvio.setText(fecX)

                    if(adopcion.iniciofec!=null){
                        etFechaInicio.setText(devolverCadena(adopcion.iniciofec!!))
                    }
                    if(adopcion.finfec!=null){
                        etFechaFin.setText(devolverCadena(adopcion.finfec!!))
                    }
                })
            })
        }
    }

    private fun devolverCadena(fec:String): String{
        val pos=fec.indexOf("-")
        val anho=fec.substring(0,pos).toInt()
        val ax= fec.substring(pos+1)
        val posAx=ax.indexOf("-")
        val mes=ax.substring(0,posAx).toInt()
        val dia=ax.substring(posAx+1).toInt()

        return "$dia-$mes-$anho"
    }
}