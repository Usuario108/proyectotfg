package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentListaMascotasBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.view.protectora.AnimalAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentListaMascotas : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentListaMascotasBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentListaMascotasBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {

            refugioViewModel.sacarMascotasSinAdoptar()
            refugioViewModel.listaMascotas.observe(viewLifecycleOwner, Observer { mascotas ->
                with(recyclerViewListaMascotas) {
                    adapter = AnimalAdapter(mascotas) { mascota -> onClickMascota(mascota) }
                    layoutManager = LinearLayoutManager(context)
                }

                if(mascotas.isEmpty()){
                    textSin.isVisible = true
                }
            })
        }
    }

    private fun onClickMascota(animal: Mascota)
    {
        val accion = FragmentListaMascotasDirections.actionFragmentListaMascotasToFragmentPerfilMascota(animal=animal)
        navController.navigate(accion)
    }
}