package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminMascotasBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.view.protectora.AnimalAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminMascotas : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminMascotasBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminMascotasBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {

            refugioViewModel.sacarMascotas()
            refugioViewModel.listaMascotas.observe(viewLifecycleOwner, Observer { mascotas ->
                with(recyclerViewAdminMascotas) {
                    adapter = AnimalAdapter(mascotas) { mascota -> onClickMascota(mascota) }
                    layoutManager = LinearLayoutManager(context)
                }

                if(mascotas.isEmpty()){
                    textSin.isVisible = true
                }
            })
        }
    }

    private fun onClickMascota(mascota: Mascota)
    {
        val accion = FragmentAdminMascotasDirections.actionFragmentAdminMascotasToFragmentAdminPerfilMas(mascota=mascota)
        navController.navigate(accion)
    }
}