package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.model.Mensaje

class MensajeAdapter(private val id:Int,private val listaMensajes: MutableList<Mensaje>? ):
    RecyclerView.Adapter<MensajeAdapter.MensajeViewHolder>() {

    class MensajeViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val mensajeEmisor = view.findViewById<TextView>(R.id.textViewMensajeDeEmisor)
        val mensajeReceptor = view.findViewById<TextView>(R.id.textViewMensajeDeReceptor)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MensajeViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_mensaje, parent, false)

        return MensajeViewHolder(adapterLayout)
    }

    @SuppressLint("RtlHardcoded")
    override fun onBindViewHolder(holder: MensajeViewHolder, position: Int) {
        val mensaje = listaMensajes?.get(position)
        if (mensaje != null) {

            if(mensaje.emisorid==id){
                holder.mensajeEmisor.text = mensaje.mensaje
                holder.mensajeReceptor.isVisible = false
            }else{
                holder.mensajeReceptor.text = mensaje.mensaje
                holder.mensajeEmisor.isVisible = false
            }
        }
    }

    override fun getItemCount(): Int {
        return if(listaMensajes?.size!=null){
            listaMensajes.size
        }else{
            0
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addMensaje(mensaje: Mensaje) {
        listaMensajes?.add(mensaje)
        notifyDataSetChanged()
    }
}