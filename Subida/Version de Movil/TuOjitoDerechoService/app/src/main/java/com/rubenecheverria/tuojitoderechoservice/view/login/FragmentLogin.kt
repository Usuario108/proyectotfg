package com.rubenecheverria.tuojitoderechoservice.view.login

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentLoginBinding
import com.rubenecheverria.tuojitoderechoservice.view.persona.admin.AdminActivity
import com.rubenecheverria.tuojitoderechoservice.view.persona.cuidador.CuidadorActivity
import com.rubenecheverria.tuojitoderechoservice.view.persona.dueno.DuenoActivity
import com.rubenecheverria.tuojitoderechoservice.view.persona.responsable.ResponsableActivity
import com.rubenecheverria.tuojitoderechoservice.view.protectora.ProtectoraActivity
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentLogin : Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var navController: NavController
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentLoginBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()

        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.textEnlace.setOnClickListener {
            navController.navigate(R.id.action_fragmentLogin_to_fragmentRegistro)
        }

        binding.apply {
            botonAceptar.setOnClickListener {
                val correo=editTextEmail.text.toString()
                val contra=editTextPassword.text.toString()

                refugioViewModel.getLogin(correo,contra)
                refugioViewModel.loginComplete.observe(viewLifecycleOwner, Observer { loginComplete ->
                    if (loginComplete) {
                        val ref = refugioViewModel.refugioLogueado
                        val usu = refugioViewModel.usuarioLogueado

                        if (ref != null)
                        {
                            val intent = Intent(context, ProtectoraActivity::class.java)
                            intent.putExtra("refugio", ref)
                            startActivity(intent)
                        }
                        else if (usu != null && usu.refugioid == null )
                        {
                            if (usu.id == 1) {
                                val intent = Intent(context, AdminActivity::class.java)
                                intent.putExtra("admin", usu)
                                startActivity(intent)
                            }
                            else
                            {
                                val intent = Intent(context, DuenoActivity::class.java)
                                intent.putExtra("dueño", usu)
                                startActivity(intent)
                            }
                        }
                        else if (usu != null)
                        {
                            refugioViewModel.getRefugioPorId(usu.refugioid!!)
                            refugioViewModel.refugio.observe(viewLifecycleOwner, Observer { refugio ->

                                if (refugio!!.responsableid == usu.id)
                                {
                                    val intent = Intent(context, ResponsableActivity::class.java)
                                    intent.putExtra("responsable", usu)
                                    startActivity(intent)
                                }
                                else
                                {
                                    val intent = Intent(context, CuidadorActivity::class.java)
                                    intent.putExtra("cuidador", usu)
                                    startActivity(intent)
                                }
                            })
                        }
                        else
                        {
                            Toast.makeText(context, "El email o la contraseña no son validos.", Toast.LENGTH_LONG).show()
                        }
                    }
                })
            }
        }
    }
}