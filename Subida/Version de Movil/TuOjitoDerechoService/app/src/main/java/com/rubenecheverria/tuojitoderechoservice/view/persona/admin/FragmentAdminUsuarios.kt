package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminUsuariosBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.view.protectora.CuidadorAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminUsuarios : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminUsuariosBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminUsuariosBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarUsuarios()

            refugioViewModel.listaCuidadores.observe(viewLifecycleOwner, Observer { usuarios ->
                with(binding.recyclerViewAdminUsuarios) {
                    adapter = CuidadorAdapter(usuarios) { usuario -> onClickUsuario(usuario) }
                    layoutManager = LinearLayoutManager(context)
                }
            })
        }
    }

    private fun onClickUsuario(usuario: Usuario)
    {
        val accion = FragmentAdminUsuariosDirections.actionFragmentAdminUsuariosToFragmentAdminPerfilUsu(usuario=usuario)
        navController.navigate(accion)
    }
}