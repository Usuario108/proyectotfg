package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.app.AlertDialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentListaContactosBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentListaContactos : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentListaContactosBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels {AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentListaContactosBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarContactos(auxViewModel.usuarioLogueado)
            refugioViewModel.listaContactos.observe(viewLifecycleOwner , Observer{ contactos ->

                with(recyclerViewListaContactos) {
                    adapter = ContactoAdapter(contactos.toMutableList(), onClickContacto = { onClickContacto(it) } )
                    layoutManager = LinearLayoutManager(context)
                }
            })


            botonFlotante.setOnClickListener {
                var edit: EditText = EditText(requireContext())
                AlertDialog.Builder(requireContext())
                    .setTitle("Inserte el correo del nuevo contacto")
                    .setView(edit)
                    .setCancelable(false)
                    .setPositiveButton(R.string.si) { _, _ ->

                        refugioViewModel.sacarUsuarioPorCorreo(edit.text.toString())
                        refugioViewModel.contacto.observe(viewLifecycleOwner, Observer{ usNuevo ->
                            if(usNuevo!=null){
                                (recyclerViewListaContactos.adapter as ContactoAdapter).addContacto(usNuevo)
                            }else{
                                Toast.makeText(context, "El correo no es conocido.", Toast.LENGTH_LONG).show()
                            }
                        })
                    }
                    .setNegativeButton(R.string.no) { _, _ -> }.show()
            }

        }
    }

    private fun onClickContacto(contacto: Usuario)
    {
        val accion= FragmentListaContactosDirections.actionFragmentListaContactosToFragmentChat(contacto=contacto)
        navController.navigate(accion)
    }
}