package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentNuevoAnimalBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.io.ByteArrayOutputStream
import java.time.LocalDate

class FragmentNuevoAnimal : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentNuevoAnimalBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentNuevoAnimalBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            botonAceptar.setOnClickListener {
                if(ValidarFormulario()){
                    refugioViewModel.nuevoIdMascota()
                    val date= LocalDate.of(dateFecha.year,dateFecha.month+1,dateFecha.dayOfMonth)

                    val bitmap = BitmapFactory.decodeResource(resources, R.drawable.im_mascota)
                    val outputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                    val byteArray = outputStream.toByteArray()
                    val imagenMascota = Base64.encodeToString(byteArray, Base64.DEFAULT)

                    refugioViewModel.nuevoIdMascota.observe(viewLifecycleOwner, Observer { nuevoIdMas ->
                        val mascota = Mascota(nuevoIdMas,auxViewModel.refugioLogueado.id,
                            etNombre.text.toString(),date.toString(),etRaza.text.toString(),etPeso.text.toString().toFloat(),
                            etDescripcion.text.toString(), imagenMascota)

                        refugioViewModel.insertarMascota(mascota)
                        refugioViewModel.respuestaInsertarMascota.observe(viewLifecycleOwner, Observer {
                            Toast.makeText(context, "La mascota se ha insertado correctamente.", Toast.LENGTH_LONG).show()
                            navController.navigate(R.id.action_fragmentNuevoAnimal_to_fragmentListaAnimales)
                        })
                    })
                }
            }

            botonCancelar.setOnClickListener { navController.navigate(R.id.action_fragmentNuevoAnimal_to_fragmentListaAnimales) }
        }
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (etNombre.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etPeso.text.toString().isEmpty())
            {
                Toast.makeText(context, "El peso no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etRaza.text.toString().isEmpty())
            {
                Toast.makeText(context, "La raza no es valida.", Toast.LENGTH_LONG).show()
                return false
            }

            val date= LocalDate.of(dateFecha.year,dateFecha.month+1,dateFecha.dayOfMonth)
            if (LocalDate.now() < date)
            {
                Toast.makeText(context, "La fecha no es valida.", Toast.LENGTH_LONG).show()
                return false
            }

            return true
        }
    }
}