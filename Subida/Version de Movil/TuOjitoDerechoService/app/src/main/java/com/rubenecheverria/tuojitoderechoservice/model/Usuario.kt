package com.rubenecheverria.tuojitoderechoservice.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Usuario (
    @SerializedName("id")
    val id:Int,
    @SerializedName("refugioid")
    val refugioid:Int?,
    @SerializedName("nombre")
    val nombre:String,
    @SerializedName("nacimiento")
    val nacimiento: String,
    @SerializedName("telefono")
    val telefono:String,
    @SerializedName("correo")
    val correo:String,
    @SerializedName("password")
    val password:String,
    @SerializedName("direccion")
    val direccion:String,
    @SerializedName("imagen")
    val imagen: String
) : Serializable