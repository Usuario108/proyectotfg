package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemCuidadorBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario

class CuidadorAdapter (
    private val listaCuidadores:List<Usuario>?,
    private val onClickUsuario: (Usuario) ->Unit
): RecyclerView.Adapter<CuidadorAdapter.CuidadorViewHolder>()
{

    class CuidadorViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val binding = ItemCuidadorBinding.bind(view)

        fun bind(cuidador: Usuario, onClickUsuario: (Usuario) -> Unit){
            binding.textViewNombre.text = cuidador.nombre
            binding.textViewTelefono.text = cuidador.telefono
            binding.textViewCorreo.text = cuidador.correo

            val longblobBytes: ByteArray = Base64.decode(cuidador.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            binding.imagenCuidador.setImageBitmap(bitmap)

            binding.cardView.setOnClickListener { onClickUsuario (cuidador) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CuidadorViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_cuidador,parent,false)

        return CuidadorViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: CuidadorViewHolder, position: Int) {
        if(listaCuidadores?.size!=null) {
            val cuidador = listaCuidadores[position]
            holder.bind(cuidador, onClickUsuario)
        }
    }

    override fun getItemCount(): Int {
        return if(listaCuidadores?.size!=null){
            listaCuidadores.size
        }else{
            0
        }
    }
}