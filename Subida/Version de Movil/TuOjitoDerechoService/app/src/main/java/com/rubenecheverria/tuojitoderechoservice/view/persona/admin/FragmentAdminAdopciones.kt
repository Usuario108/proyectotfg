package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminAdopcionesBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.view.protectora.SolicitudAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminAdopciones : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminAdopcionesBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminAdopcionesBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarMascotas()
            refugioViewModel.sacarUsuarios()

            refugioViewModel.listaCuidadores.observe(viewLifecycleOwner , Observer{ usuarios ->
                refugioViewModel.listaMascotas.observe(viewLifecycleOwner , Observer{ mascotas ->

                    refugioViewModel.sacarAdopciones()
                    refugioViewModel.listaAdop.observe(viewLifecycleOwner, Observer { adopciones ->

                            with(binding.recyclerViewAdminAdopciones) {
                                adapter = SolicitudAdapter(usuarios,mascotas, adopciones, onClickAdopcion = { onClickAdopcion(it) } )
                                layoutManager = LinearLayoutManager(context)
                            }

                            if(adopciones.isEmpty()){
                                binding.textSin.isVisible = true
                            }
                        })
                })
            })
        }
    }

    private fun onClickAdopcion(adopcion: Adopcion)
    {
        val accion = FragmentAdminAdopcionesDirections.actionFragmentAdminAdopcionesToFragmentAdminPerfilAdop(adopcion=adopcion)
        navController.navigate(accion)
    }
}