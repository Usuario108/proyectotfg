package com.rubenecheverria.tuojitoderechoservice.model

import com.rubenecheverria.tuojitoderechoservice.data.api.RefugiosApi
import retrofit2.Response

class RefugioRepository {
    suspend fun getRefugios(): List<Refugio> = RefugiosApi.retrofitService.getRefugios()

    suspend fun getRefugioPorId(id:Int): Refugio = RefugiosApi.retrofitService.getRefugioPorId(id)

    suspend fun insertarRefugio(refugio: Refugio): Response<Refugio> = RefugiosApi.retrofitService.insertarRefugio(refugio)

    suspend fun updateRefugio(id: Int, refugio: Refugio): Response<Refugio> = RefugiosApi.retrofitService.updateRefugio(id, refugio)
}