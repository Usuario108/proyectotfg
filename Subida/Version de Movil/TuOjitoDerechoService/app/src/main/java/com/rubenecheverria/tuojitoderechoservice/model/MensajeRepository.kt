package com.rubenecheverria.tuojitoderechoservice.model

import com.rubenecheverria.tuojitoderechoservice.data.api.RefugiosApi
import retrofit2.Response

class MensajeRepository {
    suspend fun getMensajes(): List<Mensaje> = RefugiosApi.retrofitService.getMensajes()

    suspend fun insertarMensaje(mensaje: Mensaje): Response<Mensaje> = RefugiosApi.retrofitService.insertarMensaje(mensaje)
}