package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentPerfilMascotaBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.time.LocalDate

class FragmentPerfilMascota : Fragment() {

    private lateinit var navController: NavController
    private lateinit var animal: Mascota
    private lateinit var binding: FragmentPerfilMascotaBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            animal = it.get("animal") as Mascota
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentPerfilMascotaBinding.inflate(inflater,container,false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            val longblobBytes: ByteArray = Base64.decode(animal.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            imageViewPerfil.setImageBitmap(bitmap)
            imageViewPerfil.setScaleType(ImageView.ScaleType.FIT_CENTER)

            etNombre.setText(animal.nombre)
            etRaza.setText(animal.raza)
            etDescripcion.setText(animal.descripcion)

            val fec=animal.nacimiento
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val str=ax.substring(posAx+1)
            val posStr=str.indexOf("T")
            val dia=str.substring(0,posStr)
            etFecha.setText(dia+"-"+mes.toString()+"-" +anho.toString())

            refugioViewModel.getRefugioPorId(animal.refugioid)
            refugioViewModel.refugio.observe(viewLifecycleOwner, Observer { ref ->
                etRefugio.setText(ref.nombre)
                refugioViewModel.getUsuarioPorId(ref.responsableid)
                refugioViewModel.usuario.observe(viewLifecycleOwner, Observer { resp ->
                    etResponsable.setText(resp.nombre)
                })
            })

            botonAceptar.setOnClickListener {
                refugioViewModel.comprobacionNuevaAdopcion(auxViewModel.usuarioLogueado.id,animal.id)
                refugioViewModel.comprobacionInsercionAdop.observe(viewLifecycleOwner, Observer { comp ->
                    if(comp){
                        Toast.makeText(context, "Ya has enviado la adopcion de este animal.", Toast.LENGTH_LONG).show()
                    } else {

                        refugioViewModel.nuevoIdAdopcion()
                        refugioViewModel.nuevoIdAdopcion.observe(viewLifecycleOwner, Observer { nuevoIdAdop ->

                            val local= LocalDate.now()
                            var diaX=local.dayOfMonth.toString()
                            var mesX=local.monthValue.toString()
                            val anhoX=local.year

                            if(diaX.length==1){
                                diaX= "0$diaX"
                            }
                            if(mesX.length==1){
                                mesX= "0$mesX"
                            }

                            val date="$anhoX-$mesX-$diaX"
                            val adopcion = Adopcion(nuevoIdAdop,auxViewModel.usuarioLogueado.id,animal.id,date,null,null)

                            refugioViewModel.insertarAdopcion(adopcion)
                            refugioViewModel.respuestaInsertarAdopcion.observe(viewLifecycleOwner, Observer {
                                Toast.makeText(context, "Se ha enviado la solicitud.", Toast.LENGTH_LONG).show()
                                navController.navigate(R.id.action_fragmentPerfilMascota_to_fragmentListaMascotas)
                            })
                        })
                    }
                })
            }
        }
    }
}