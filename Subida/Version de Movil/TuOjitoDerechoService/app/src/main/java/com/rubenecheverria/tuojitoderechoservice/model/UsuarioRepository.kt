package com.rubenecheverria.tuojitoderechoservice.model

import com.rubenecheverria.tuojitoderechoservice.data.api.RefugiosApi
import retrofit2.Response

class UsuarioRepository {
    suspend fun getUsuarios(): List<Usuario> = RefugiosApi.retrofitService.getUsuarios()

    suspend fun getUsuarioPorId(id:Int): Usuario = RefugiosApi.retrofitService.getUsuarioPorId(id)

    suspend fun insertarUsuario(usuario: Usuario): Response<Usuario> = RefugiosApi.retrofitService.insertarUsuario(usuario)

    suspend fun updateUsuario(id: Int, usuario: Usuario): Response<Usuario> = RefugiosApi.retrofitService.updateUsuario(id, usuario)

    suspend fun deleteUsuario(id: Int): Response<Usuario> = RefugiosApi.retrofitService.deleteUsuario(id)
}