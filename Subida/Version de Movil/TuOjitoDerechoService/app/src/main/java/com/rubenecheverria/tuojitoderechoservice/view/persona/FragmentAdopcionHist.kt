package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdopcionHistBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdopcionHist : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdopcionHistBinding
    private lateinit var adopcion: Adopcion
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            adopcion = it.get("adopcion") as Adopcion
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdopcionHistBinding.inflate(inflater,container,false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {

            refugioViewModel.getMascotaPorId(adopcion.mascotaid)
            refugioViewModel.mascota.observe(viewLifecycleOwner, Observer { mascota ->

                val longblobBytes: ByteArray = Base64.decode(mascota.imagen, Base64.DEFAULT)
                val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
                imageViewMasFoto.setImageBitmap(bitmap)

                etNombreAdop.setText(mascota.nombre)
                etPesoAdop.setText(mascota.peso.toString())
                etDescripcionAdop.setText(mascota.descripcion)

                if(adopcion.iniciofec!=null){
                    val fec=adopcion.iniciofec!!
                    val pos=fec.indexOf("-")
                    val anho=fec.substring(0,pos).toInt()
                    val ax= fec.substring(pos+1)
                    val posAx=ax.indexOf("-")
                    val mes=ax.substring(0,posAx).toInt()
                    val dia=ax.substring(posAx+1).toInt()

                    etFechaInicioAdop.setText(dia.toString()+"-"+mes.toString()+"-"+anho.toString())
                }
                if(adopcion.finfec!=null){
                    val fec=adopcion.finfec!!
                    val pos=fec.indexOf("-")
                    val anho=fec.substring(0,pos).toInt()
                    val ax= fec.substring(pos+1)
                    val posAx=ax.indexOf("-")
                    val mes=ax.substring(0,posAx).toInt()
                    val dia=ax.substring(posAx+1).toInt()

                    etFechaFinAdop.setText(dia.toString()+"-"+mes.toString()+"-"+anho.toString())
                }

                if(adopcion.iniciofec==null || adopcion.finfec!=null){
                    etNombreAdop.focusable = View.NOT_FOCUSABLE
                    etPesoAdop.focusable = View.NOT_FOCUSABLE
                    etDescripcionAdop.focusable = View.NOT_FOCUSABLE
                    botonAceptar.isClickable = false
                }

                botonCancelar.setOnClickListener { navController.navigate(R.id.action_fragmentAdopcionHist_to_fragmentHistorialAdopciones) }

                botonAceptar.setOnClickListener {
                    if(ValidarFormulario()){
                        var update= Mascota(mascota.id,mascota.refugioid,etNombreAdop.text.toString(),mascota.nacimiento,mascota.raza,etPesoAdop.text.toString().toFloat(),
                            etDescripcionAdop.text.toString(),mascota.imagen)

                        refugioViewModel.updateMascota(mascota.id, update)
                        refugioViewModel.respuestaModificarMascota.observe(viewLifecycleOwner, Observer {
                            Toast.makeText(context, "Se han modificado los datos de la mascota.", Toast.LENGTH_LONG).show()
                            navController.navigate(R.id.action_fragmentAdopcionHist_to_fragmentHistorialAdopciones)
                        })
                    }
                }
            })
        }
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (etNombreAdop.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (etPesoAdop.text.toString().isEmpty())
            {
                Toast.makeText(context, "El peso no es valido.", Toast.LENGTH_LONG).show()
                return false;
            }

            return true
        }
    }
}