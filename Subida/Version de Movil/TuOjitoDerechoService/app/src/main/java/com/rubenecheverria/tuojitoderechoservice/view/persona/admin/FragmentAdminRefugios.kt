package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminRefugiosBinding
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.view.persona.RefugioAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminRefugios : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminRefugiosBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminRefugiosBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarUsuarios()
            refugioViewModel.listaCuidadores.observe(viewLifecycleOwner , Observer{ usuarios ->

                refugioViewModel.sacarRefugios()
                refugioViewModel.listaRefugios.observe(viewLifecycleOwner, Observer { refugios ->
                    with(binding.recyclerViewAdminRefugios) {
                        adapter = RefugioAdapter(usuarios, refugios, onClickRefugio = { onClickRefugio(it) } )
                        layoutManager = LinearLayoutManager(context)
                    }

                    if(refugios.isEmpty()){
                        textSin.isVisible = true
                    }
                })
            })
        }
    }

    private fun onClickRefugio(refugio: Refugio)
    {
        val accion = FragmentAdminRefugiosDirections.actionFragmentAdminRefugiosToFragmentAdminPerfilRef(refugio=refugio)
        navController.navigate(accion)
    }
}