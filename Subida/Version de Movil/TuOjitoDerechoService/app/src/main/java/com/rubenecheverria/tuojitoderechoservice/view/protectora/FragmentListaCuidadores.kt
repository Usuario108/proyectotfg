package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentListaCuidadoresBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentListaCuidadores : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentListaCuidadoresBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels {AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentListaCuidadoresBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarCuidadores(auxViewModel.refugioLogueado.id)

            refugioViewModel.listaCuidadores.observe(viewLifecycleOwner, Observer { usuarios ->
                with(binding.recyclerViewListaCuidadores) {
                    adapter = CuidadorAdapter(usuarios) { usuario -> onClickCuidador(usuario) }
                    layoutManager = LinearLayoutManager(context)
                }
            })
        }

        binding.botonFlotante.setOnClickListener { navController.navigate(R.id.action_fragmentListaCuidadores_to_fragmentNuevoCuidador) }
    }

    private fun onClickCuidador(cuidador: Usuario)
    {
        val accion = FragmentListaCuidadoresDirections.actionFragmentListaCuidadoresToFragmentPerfilCuidador(cuidador=cuidador)
        navController.navigate(accion)
    }
}