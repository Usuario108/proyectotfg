package com.rubenecheverria.tuojitoderechoservice.model

import com.rubenecheverria.tuojitoderechoservice.data.api.RefugiosApi
import retrofit2.Response

class MascotaRepository {
    suspend fun getMascotas(): List<Mascota> = RefugiosApi.retrofitService.getMascotas()

    suspend fun getMascotaPorId(id:Int): Mascota = RefugiosApi.retrofitService.getMascotaPorId(id)

    suspend fun insertarMascota(mascota: Mascota): Response<Mascota> = RefugiosApi.retrofitService.insertarMascota(mascota)

    suspend fun updateMascota(id: Int, mascota: Mascota): Response<Mascota> = RefugiosApi.retrofitService.updateMascota(id, mascota)

    suspend fun deleteMascota(id: Int): Response<Mascota> = RefugiosApi.retrofitService.deleteMascota(id)
}