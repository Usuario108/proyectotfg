package com.rubenecheverria.tuojitoderechoservice.model

import com.rubenecheverria.tuojitoderechoservice.data.api.RefugiosApi
import retrofit2.Response

class AdopcionRepository {
    suspend fun getAdopciones(): List<Adopcion> = RefugiosApi.retrofitService.getAdopciones()

    suspend fun insertarAdopcion(adopcion: Adopcion): Response<Adopcion> = RefugiosApi.retrofitService.insertarAdopcion(adopcion)

    suspend fun updateAdopcion(id: Int, adopcion: Adopcion): Response<Adopcion> = RefugiosApi.retrofitService.updateAdopcion(id, adopcion)

    suspend fun deleteAdopcion(id: Int): Response<Adopcion> = RefugiosApi.retrofitService.deleteAdopcion(id)
}