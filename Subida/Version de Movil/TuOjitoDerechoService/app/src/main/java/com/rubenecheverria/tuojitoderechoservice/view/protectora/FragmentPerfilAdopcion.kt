package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentPerfilAdopcionBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.sql.Date
import java.time.LocalDate

class FragmentPerfilAdopcion : Fragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentPerfilAdopcionBinding
    private var fecInicio:Boolean=true
    private lateinit var adopcion: Adopcion
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            adopcion = it.get("adopcion") as Adopcion
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentPerfilAdopcionBinding.inflate(inflater,container,false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.getUsuarioPorId(adopcion.userid)
            refugioViewModel.getMascotaPorId(adopcion.mascotaid)
            refugioViewModel.usuario.observe(viewLifecycleOwner, Observer { usu ->
                refugioViewModel.mascota.observe(viewLifecycleOwner, Observer { mas ->

                    val longblobBytes1: ByteArray = Base64.decode(usu.imagen, Base64.DEFAULT)
                    val bitmap1: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes1, 0, longblobBytes1.size)
                    imageViewPerfilUsu.setImageBitmap(bitmap1)
                    imageViewPerfilUsu.setScaleType(ImageView.ScaleType.FIT_CENTER)
                    textViewNombreUsu.text = usu.nombre
                    textViewTelefonoUsu.text = usu.telefono

                    val longblobBytes2: ByteArray = Base64.decode(mas.imagen, Base64.DEFAULT)
                    val bitmap2: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes2, 0, longblobBytes2.size)
                    imageViewPerfilMas.setImageBitmap(bitmap2)
                    imageViewPerfilMas.setScaleType(ImageView.ScaleType.FIT_CENTER)
                    textViewNombreMas.text = mas.nombre
                    textViewRazaMas.text = mas.raza

                    val fecX=devolverCadena(adopcion.solicitudfec)
                    etFechaEnvio.setText(fecX)

                    if(adopcion.iniciofec!=null){
                        etFechaInicio.setText(devolverCadena(adopcion.iniciofec!!))
                    }
                    if(adopcion.finfec!=null){
                        etFechaFin.setText(devolverCadena(adopcion.finfec!!))
                    }

                    etFechaInicio.setOnClickListener {
                        fecInicio=true
                        if(adopcion.iniciofec!=null){
                            val fec=adopcion.iniciofec!!
                            val pos=fec.indexOf("-")
                            val anho=fec.substring(0,pos).toInt()
                            val ax= fec.substring(pos+1)
                            val posAx=ax.indexOf("-")
                            val mes=ax.substring(0,posAx).toInt()
                            val dia=ax.substring(posAx+1).toInt()

                            context?.let { it1 -> DatePickerDialog(it1,this@FragmentPerfilAdopcion,anho,mes,dia).show() }
                        }else{
                            context?.let { it1 -> DatePickerDialog(it1,this@FragmentPerfilAdopcion,
                                LocalDate.now().year,
                                LocalDate.now().monthValue-1,
                                LocalDate.now().dayOfMonth).show() }
                        }
                    }
                    etFechaFin.setOnClickListener {
                        fecInicio=false
                        if(adopcion.finfec!=null){
                            val fec=adopcion.finfec!!
                            val pos=fec.indexOf("-")
                            val anho=fec.substring(0,pos).toInt()
                            val ax= fec.substring(pos+1)
                            val posAx=ax.indexOf("-")
                            val mes=ax.substring(0,posAx).toInt()
                            val dia=ax.substring(posAx+1).toInt()

                            context?.let { it1 -> DatePickerDialog(it1,this@FragmentPerfilAdopcion,anho,mes,dia).show() }
                        }else{
                            context?.let { it1 -> DatePickerDialog(it1,this@FragmentPerfilAdopcion,
                                LocalDate.now().year,
                                LocalDate.now().monthValue-1,
                                LocalDate.now().dayOfMonth).show() }
                        }
                    }

                    botonAceptar.setOnClickListener {
                        if(ValidarFormulario()){
                            var dateIni: Date?= null
                            var dateFin: Date?= null

                            if (!etFechaInicio.text.toString().isEmpty() && !etFechaFin.text.toString().isEmpty()){
                                dateIni= devolverDate(etFechaInicio.text.toString())
                                dateFin= devolverDate(etFechaFin.text.toString())
                            }

                            refugioViewModel.comprobacionUpdateAdopcion(adopcion,dateIni,dateFin)
                            refugioViewModel.listaComprobaciones.observe(viewLifecycleOwner, Observer { comprobaciones ->
                                if(comprobaciones[0]){
                                    Toast.makeText(context, "Esta usuario ya ha adoptado la mascota.", Toast.LENGTH_LONG).show()
                                } else if ((dateFin== null && comprobaciones[1] && comprobaciones[5]) || comprobaciones[2] || comprobaciones[3] || comprobaciones[4]) {
                                    Toast.makeText(context, "La mascota ha sido adoptada por otro.", Toast.LENGTH_LONG).show()
                                }else{
                                    var stringIni: String?= null
                                    var stringFin: String?= null

                                    if (!etFechaInicio.text.toString().isEmpty()){
                                        stringIni = devolverCadena2(etFechaInicio.text.toString())
                                    }
                                    if(!etFechaFin.text.toString().isEmpty()){
                                        stringFin = devolverCadena2(etFechaFin.text.toString())
                                    }

                                    var update = Adopcion(0,0,0,"2000-01-01",stringIni,stringFin)

                                    refugioViewModel.updateAdopcion(adopcion.id, update)
                                    refugioViewModel.respuestaModificarAdopcion.observe(viewLifecycleOwner, Observer {
                                        Toast.makeText(context, "Se han modificado los datos de la adopcion.", Toast.LENGTH_LONG).show()
                                        navController.navigate(R.id.action_fragmentPerfilAdopcion_to_fragmentListaAdopciones)
                                    })
                                }
                            })
                        }
                    }

                    botonEliminar.setOnClickListener{
                        AlertDialog.Builder(requireContext())
                            .setTitle("Eliminacion")
                            .setMessage("¿Esta seguro de que desea eliminar esta adopcion?")
                            .setCancelable(false)
                            .setPositiveButton(R.string.si) { _, _ ->
                                if(ValidarEliminacion()){
                                    refugioViewModel.deleteAdopcion(adopcion.id)
                                    refugioViewModel.respuestaEliminarAdopcion.observe(viewLifecycleOwner, Observer {
                                        Toast.makeText(context, "Adopcion Eliminada", Toast.LENGTH_SHORT).show()
                                        navController.navigate(R.id.action_fragmentPerfilAdopcion_to_fragmentListaAdopciones)
                                    })
                                }
                            }
                            .setNegativeButton(R.string.no) { _, _ -> }.show()
                    }
                })
            })
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(p0: DatePicker?, anho: Int, mes: Int, dia: Int) {
        if(fecInicio){
            binding.etFechaInicio.setText(dia.toString()+"-"+(mes+1).toString()+"-"+anho.toString())
        }else{
            binding.etFechaFin.setText(dia.toString()+"-"+(mes+1).toString()+"-"+anho.toString())
        }
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (etFechaInicio.text.toString().isEmpty() && !etFechaFin.text.toString().isEmpty())
            {
                Toast.makeText(context, "La fecha de inicio no puede ser nula si hay establecida una fecha de fin.", Toast.LENGTH_LONG).show()
                return false
            }

            if (!etFechaInicio.text.toString().isEmpty() && !etFechaFin.text.toString().isEmpty())
            {
                val dateIni= devolverLocal(etFechaInicio.text.toString())
                val dateFin= devolverLocal(etFechaFin.text.toString())
                if(dateIni>dateFin){
                    Toast.makeText(context, "La fecha de fin de la adopcion no puede ser previa a la fecha de inicio.", Toast.LENGTH_LONG).show()
                    return false
                }
            }
            return true
        }
    }

    private fun ValidarEliminacion():Boolean{
        binding.apply {
            if(adopcion.iniciofec!=null){
                Toast.makeText(context, "No se puede eliminar una adopcion establecida.", Toast.LENGTH_LONG).show()
                return false
            }
            return true
        }
    }

    private fun devolverCadena(fec:String): String{
        val pos=fec.indexOf("-")
        val anho=fec.substring(0,pos).toInt()
        val ax= fec.substring(pos+1)
        val posAx=ax.indexOf("-")
        val mes=ax.substring(0,posAx).toInt()
        val dia=ax.substring(posAx+1).toInt()

        return "$dia-$mes-$anho"
    }

    private fun devolverCadena2(fec:String): String{
        val pos=fec.indexOf("-")
        var dia=fec.substring(0,pos)
        val ax= fec.substring(pos+1)
        val posAx=ax.indexOf("-")
        var mes=ax.substring(0,posAx)
        val anho=ax.substring(posAx+1).toInt()

        if(dia.length==1){
            dia= "0$dia"
        }
        if(mes.length==1){
            mes= "0$mes"
        }

        return "$anho-$mes-$dia"
    }

    private fun devolverLocal(texto:String): LocalDate {
        val pos=texto.indexOf("-")
        val dia=texto.substring(0,pos).toInt()
        val ax= texto.substring(pos+1)
        val posAx=ax.indexOf("-")
        val mes=ax.substring(0,posAx).toInt()
        val anho=ax.substring(posAx+1).toInt()

        return LocalDate.of(anho,mes,dia)
    }

    private fun devolverDate(texto:String): Date {
        val pos=texto.indexOf("-")
        val dia=texto.substring(0,pos).toInt()
        val ax= texto.substring(pos+1)
        val posAx=ax.indexOf("-")
        val mes=ax.substring(0,posAx).toInt()
        val anho=ax.substring(posAx+1).toInt()

        return Date(anho,mes-1,dia)
    }
}