package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemHistAdopcionBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.model.Mascota

class HistorialAdopcionAdapter (
    private val listaMascotas: List<Mascota>,
    private val listaAdopciones:List<Adopcion>?,
    private val onClickAdopcion: (Adopcion) ->Unit
): RecyclerView.Adapter<HistorialAdopcionAdapter.HistorialAdopcionViewHolder>(){

    class HistorialAdopcionViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val binding = ItemHistAdopcionBinding.bind(view)

        @SuppressLint("SetTextI18n")
        fun bind(listaMascotas: List<Mascota>, adopcion: Adopcion, onClickAdopcion: (Adopcion) -> Unit) {

            listaMascotas.forEach { mascota ->
                if(mascota.id==adopcion.mascotaid){
                    binding.textViewNombre.text=mascota.nombre

                    val fec=adopcion.solicitudfec
                    val pos=fec.indexOf("-")
                    val anho=fec.substring(0,pos).toInt()
                    val ax= fec.substring(pos+1)
                    val posAx=ax.indexOf("-")
                    val mes=ax.substring(0,posAx).toInt()
                    val dia=ax.substring(posAx+1).toInt()

                    binding.textViewFecha.text ="Fecha de Solicitud: " + dia.toString()+"-"+mes.toString()+"-"+anho.toString()

                    val longblobBytes: ByteArray = Base64.decode(mascota.imagen, Base64.DEFAULT)
                    val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
                    binding.imagenAnimal.setImageBitmap(bitmap)

                    binding.cardView.setOnClickListener { onClickAdopcion (adopcion) }
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistorialAdopcionViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_hist_adopcion,parent,false)

        return HistorialAdopcionViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: HistorialAdopcionViewHolder, position: Int) {
        if(listaAdopciones?.size!=null) {
            val solicitud = listaAdopciones[position]
            holder.bind(listaMascotas, solicitud, onClickAdopcion)
        }
    }

    override fun getItemCount(): Int {
        return if(listaAdopciones?.size!=null){
            listaAdopciones.size
        }else{
            0
        }
    }
}