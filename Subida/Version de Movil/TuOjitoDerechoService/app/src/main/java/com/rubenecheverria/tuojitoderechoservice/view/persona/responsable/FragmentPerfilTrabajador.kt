package com.rubenecheverria.tuojitoderechoservice.view.persona.responsable

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentPerfilTrabajadorBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.time.LocalDate

class FragmentPerfilTrabajador : Fragment(), DatePickerDialog.OnDateSetListener {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentPerfilTrabajadorBinding
    private lateinit var usuario: Usuario
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            usuario = it.get("trabajador") as Usuario
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentPerfilTrabajadorBinding.inflate(inflater,container,false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            val longblobBytes: ByteArray = Base64.decode(usuario.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            imageViewPerfil.setImageBitmap(bitmap)
            imageViewPerfil.setScaleType(ImageView.ScaleType.FIT_CENTER)

            tvEmail.text = usuario.correo
            etNombre.setText(usuario.nombre)
            etTelefono.setText(usuario.telefono)
            etDireccion.setText(usuario.direccion)

            val fec=usuario.nacimiento
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val dia=ax.substring(posAx+1).toInt()
            etFecha.setText(dia.toString()+"-"+mes.toString()+"-" +anho.toString())

            etFecha.setOnClickListener {
                context?.let { it1 -> DatePickerDialog(it1,this@FragmentPerfilTrabajador,anho,(mes-1),dia.toInt()).show() }
            }
            botonCancelar.setOnClickListener { navController.navigate(R.id.action_fragmentPerfilTrabajador_to_fragmentListaTrabajadores) }

            botonAceptar.setOnClickListener {
                if(ValidarFormulario()){
                    val posX=etFecha.text.toString().indexOf("-")
                    val diaX=etFecha.text.toString().substring(0,posX).toInt()
                    val axX= etFecha.text.toString().substring(posX+1)
                    val posAxX=axX.indexOf("-")
                    val mesX=axX.substring(0,posAxX).toInt()
                    val anhoX=axX.substring(posAxX+1).toInt()
                    val fecX= anhoX.toString()+"-"+mesX+"-"+diaX

                    var update= Usuario(0,auxViewModel.usuarioLogueado.refugioid!!,etNombre.text.toString(),fecX,etTelefono.text.toString(),
                        usuario.correo,usuario.password,etDireccion.text.toString(),usuario.imagen)

                    refugioViewModel.updateUsuario(usuario.id, update)
                    refugioViewModel.respuestaModificarUsuario.observe(viewLifecycleOwner, Observer {
                        Toast.makeText(context, "Se han modificado los datos del cuidador.", Toast.LENGTH_LONG).show()
                    })
                }
            }

            imageEliminar.setOnClickListener{
                AlertDialog.Builder(requireContext())
                    .setTitle("Eliminacion")
                    .setMessage("¿Esta seguro de que desea eliminar al cuidador ${usuario.nombre}?")
                    .setCancelable(false)
                    .setPositiveButton(R.string.si) { _, _ ->

                        refugioViewModel.deleteUsuario(usuario.id)
                        refugioViewModel.respuestaEliminarUsuario.observe(viewLifecycleOwner, Observer {
                            Toast.makeText(context, "Cuidador Eliminado", Toast.LENGTH_SHORT).show()
                            navController.navigate(R.id.action_fragmentPerfilTrabajador_to_fragmentListaTrabajadores)
                        })
                    }
                    .setNegativeButton(R.string.no) { _, _ -> }.show()
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onDateSet(p0: DatePicker?, anho: Int, mes: Int, dia: Int) {
        binding.etFecha.setText(dia.toString()+"-"+(mes+1).toString()+"-"+anho.toString())
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (etNombre.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (etDireccion.text.toString().isEmpty())
            {
                Toast.makeText(context, "La direccion no es valida.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (etTelefono.text.toString().isEmpty())
            {
                Toast.makeText(context, "El telefono no es valido.", Toast.LENGTH_LONG).show()
                return false;
            }

            val pos=etFecha.text.toString().indexOf("-")
            val dia=etFecha.text.toString().substring(0,pos).toInt()
            val ax= etFecha.text.toString().substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val anho=ax.substring(posAx+1).toInt()

            val date= LocalDate.of(anho,mes,dia)
            if (LocalDate.now() < date)
            {
                Toast.makeText(context, "La fecha no es valida.", Toast.LENGTH_LONG).show()
                return false;
            }

            val difFechas = LocalDate.now().year - date.year
            if (difFechas < 18)
            {
                Toast.makeText(context, "No puede registrarse menores de 18 años.", Toast.LENGTH_LONG).show()
                return false;
            }

            return true;
        }
    }
}