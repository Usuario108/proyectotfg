package com.rubenecheverria.tuojitoderechoservice.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.model.AdopcionRepository
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.model.MascotaRepository
import com.rubenecheverria.tuojitoderechoservice.model.Mensaje
import com.rubenecheverria.tuojitoderechoservice.model.MensajeRepository
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.model.RefugioRepository
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.model.UsuarioRepository
import kotlinx.coroutines.launch
import retrofit2.Response
import java.sql.Date

class RefugioViewModel: ViewModel()
{
    //Llamada a repositorios
    private val repRefugio = RefugioRepository()
    private val repMascota = MascotaRepository()
    private val repUsuario = UsuarioRepository()
    private val repMensaje = MensajeRepository()
    private val repAdopcion = AdopcionRepository()

    //Login completo
    private val _loginComplete = MutableLiveData<Boolean>()
    val loginComplete: LiveData<Boolean> get() = _loginComplete

    //Variables de login
    var refugioLogueado: Refugio? = null
    var usuarioLogueado: Usuario? = null

    //Variables para sacar refugio por id
    private val _refugio = MutableLiveData<Refugio>()
    val refugio: LiveData<Refugio> get() = _refugio
    //Variables para sacar mascota por id
    private val _mascota = MutableLiveData<Mascota>()
    val mascota: LiveData<Mascota> get() = _mascota
    //Variables para sacar usuario por id
    private val _usuario = MutableLiveData<Usuario>()
    val usuario: LiveData<Usuario> get() = _usuario
    //Variables para sacar contacto
    private val _contacto = MutableLiveData<Usuario>()
    val contacto: LiveData<Usuario> get() = _contacto

    //Variables de insercion de refugio
    private val _respuestaInsertarRefugio = MutableLiveData<Response<Refugio>>()
    val respuestaInsertarRefugio: LiveData<Response<Refugio>> get() = _respuestaInsertarRefugio
    //Variables de insercion de usuario
    private val _respuestaInsertarUsuario = MutableLiveData<Response<Usuario>>()
    val respuestaInsertarUsuario: LiveData<Response<Usuario>> get() = _respuestaInsertarUsuario
    //Variables de insercion de mascota
    private val _respuestaInsertarMascota = MutableLiveData<Response<Mascota>>()
    val respuestaInsertarMascota: LiveData<Response<Mascota>> get() = _respuestaInsertarMascota
    //Variables de insercion de adopcion
    private val _respuestaInsertarAdopcion = MutableLiveData<Response<Adopcion>>()
    val respuestaInsertarAdopcion: LiveData<Response<Adopcion>> get() = _respuestaInsertarAdopcion
    //Variables de insercion de mensaje
    private val _respuestaInsertarMensaje = MutableLiveData<Response<Mensaje>>()
    val respuestaInsertarMensaje: LiveData<Response<Mensaje>> get() = _respuestaInsertarMensaje

    //Nuevo id de refugio
    private val _nuevoIdRefugio = MutableLiveData<Int>()
    val nuevoIdRefugio: LiveData<Int> get() = _nuevoIdRefugio
    //Nuevo id de usuario
    private val _nuevoIdUsuario = MutableLiveData<Int>()
    val nuevoIdUsuario: LiveData<Int> get() = _nuevoIdUsuario
    //Comprobacion de correos para el registro
    var correos: MutableList<String> = mutableListOf()
    //Nuevo id de mascota
    private val _nuevoIdMascota = MutableLiveData<Int>()
    val nuevoIdMascota: LiveData<Int> get() = _nuevoIdMascota
    //Nuevo id de adopcion
    private val _nuevoIdAdopcion = MutableLiveData<Int>()
    val nuevoIdAdopcion: LiveData<Int> get() = _nuevoIdAdopcion
    //Nuevo id de mensaje
    private val _nuevoIdMensaje = MutableLiveData<Int>()
    val nuevoIdMensaje: LiveData<Int> get() = _nuevoIdMensaje

    //Lista de refugios
    private val _listaRefugios = MutableLiveData<List<Refugio>>()
    val listaRefugios: LiveData<List<Refugio>> get() = _listaRefugios
    //Lista de usuarios
    private val _listaCuidadores = MutableLiveData<List<Usuario>>()
    val listaCuidadores: LiveData<List<Usuario>> get() = _listaCuidadores
    //Lista de mensajes
    private val _listaMens = MutableLiveData<List<Mensaje>>()
    val listaMens: LiveData<List<Mensaje>> get() = _listaMens
    //Lista de adopciones
    private val _listaAdop = MutableLiveData<List<Adopcion>>()
    val listaAdop: LiveData<List<Adopcion>> get() = _listaAdop

    //Lista de mascotas
    private val _listaMascotas = MutableLiveData<List<Mascota>>()
    val listaMascotas: LiveData<List<Mascota>> get() = _listaMascotas
    //Lista de adopciones de refugio
    private val _listaAdopciones = MutableLiveData<List<Adopcion>>()
    val listaAdopciones: LiveData<List<Adopcion>> get() = _listaAdopciones
    //Lista de comprobaciones de modificacion de adopciones
    private val _listaComprobaciones = MutableLiveData<List<Boolean>>()
    val listaComprobaciones: LiveData<List<Boolean>> get() = _listaComprobaciones

    //Variables de modificacion de refugio
    private val _respuestaModificarRefugio = MutableLiveData<Response<Refugio>>()
    val respuestaModificarRefugio: LiveData<Response<Refugio>> get() = _respuestaModificarRefugio
    //Variables de modificacion de usuario
    private val _respuestaModificarUsuario = MutableLiveData<Response<Usuario>>()
    val respuestaModificarUsuario: LiveData<Response<Usuario>> get() = _respuestaModificarUsuario
    //Variables de modificacion de mascota
    private val _respuestaModificarMascota = MutableLiveData<Response<Mascota>>()
    val respuestaModificarMascota: LiveData<Response<Mascota>> get() = _respuestaModificarMascota
    //Variables de modificacion de adopcion
    private val _respuestaModificarAdopcion = MutableLiveData<Response<Adopcion>>()
    val respuestaModificarAdopcion: LiveData<Response<Adopcion>> get() = _respuestaModificarAdopcion

    //Variables de eliminacion de mascota
    private val _respuestaEliminarMascota = MutableLiveData<Response<Mascota>>()
    val respuestaEliminarMascota: LiveData<Response<Mascota>> get() = _respuestaEliminarMascota
    //Variables de eliminacion de usuario
    private val _respuestaEliminarUsuario = MutableLiveData<Response<Usuario>>()
    val respuestaEliminarUsuario: LiveData<Response<Usuario>> get() = _respuestaEliminarUsuario
    //Variables de eliminacion de adopcion
    private val _respuestaEliminarAdopcion = MutableLiveData<Response<Adopcion>>()
    val respuestaEliminarAdopcion: LiveData<Response<Adopcion>> get() = _respuestaEliminarAdopcion

    //Lista de comprobaciones de modificacion de adopciones
    private val _comprobacionInsercionAdop = MutableLiveData<Boolean>()
    val comprobacionInsercionAdop: LiveData<Boolean> get() = _comprobacionInsercionAdop

    //Lista de contactos de usuario
    private val _listaContactos = MutableLiveData<List<Usuario>>()
    val listaContactos: LiveData<List<Usuario>> get() = _listaContactos
    //Lista de mensaje entre contactos
    private val _listaMensajes = MutableLiveData<List<Mensaje>>()
    val listaMensajes: LiveData<List<Mensaje>> get() = _listaMensajes
    //Lista de adopciones de usuario
    private val _listaHistorial = MutableLiveData<List<Adopcion>>()
    val listaHistorial: LiveData<List<Adopcion>> get() = _listaHistorial

    fun getLogin(correo: String, password: String) {
        viewModelScope.launch {
            refugioLogueado = null
            usuarioLogueado = null

            repRefugio.getRefugios().forEach { refugio ->
                if (refugio.correo == correo && refugio.password == password) {
                    refugioLogueado = refugio
                }
            }

            repUsuario.getUsuarios().forEach { usuario ->
                if (usuario.correo == correo && usuario.password == password) {
                    usuarioLogueado = usuario
                }
            }
            _loginComplete.postValue(true)
        }
    }

    fun sacarRefugios() {
        viewModelScope.launch {
            _listaRefugios.value = repRefugio.getRefugios()
        }
    }

    fun sacarUsuarios() {
        viewModelScope.launch {
            _listaCuidadores.value= repUsuario.getUsuarios()
        }
    }

    fun sacarMascotas() {
        viewModelScope.launch {
            _listaMascotas.value= repMascota.getMascotas()
        }
    }

    fun sacarMensajes() {
        viewModelScope.launch {
            _listaMens.value= repMensaje.getMensajes()
        }
    }

    fun sacarAdopciones() {
        viewModelScope.launch {
            _listaAdop.value= repAdopcion.getAdopciones()
        }
    }

    fun getRefugioPorId(id: Int) {
        viewModelScope.launch {
            _refugio.value = repRefugio.getRefugioPorId(id)
        }
    }

    fun getMascotaPorId(id: Int) {
        viewModelScope.launch {
            _mascota.value = repMascota.getMascotaPorId(id)
        }
    }

    fun getUsuarioPorId(id: Int) {
        viewModelScope.launch {
            _usuario.value = repUsuario.getUsuarioPorId(id)
        }
    }

    fun insertarRefugio(refugio: Refugio) {
        viewModelScope.launch {
            _respuestaInsertarRefugio.value = repRefugio.insertarRefugio(refugio)
        }
    }

    fun insertarUsuario(usuario: Usuario) {
        viewModelScope.launch {
            _respuestaInsertarUsuario.value = repUsuario.insertarUsuario(usuario)
        }
    }

    fun insertarMascota(mascota: Mascota) {
        viewModelScope.launch {
            _respuestaInsertarMascota.value = repMascota.insertarMascota(mascota)
        }
    }

    fun insertarAdopcion(adopcion: Adopcion) {
        viewModelScope.launch {
            _respuestaInsertarAdopcion.value = repAdopcion.insertarAdopcion(adopcion)
        }
    }

    fun insertarMensaje(mensaje: Mensaje) {
        viewModelScope.launch {
            _respuestaInsertarMensaje.value = repMensaje.insertarMensaje(mensaje)
        }
    }


    fun nuevoIdRefugio() {
        viewModelScope.launch {
            repRefugio.getRefugios().forEach { refugio ->
                correos.add(refugio.correo)
            }
            _nuevoIdRefugio.value = repRefugio.getRefugios().size+1
        }
    }

    fun nuevoIdUsuario() {
        viewModelScope.launch {
            repUsuario.getUsuarios().forEach { usuario ->
                correos.add(usuario.correo)
            }
            _nuevoIdUsuario.value = repUsuario.getUsuarios().size+1
        }
    }

    fun nuevoIdMascota() {
        viewModelScope.launch {
            _nuevoIdMascota.value = repMascota.getMascotas().size+1
        }
    }

    fun nuevoIdAdopcion() {
        viewModelScope.launch {
            _nuevoIdAdopcion.value = repAdopcion.getAdopciones().size+1
        }
    }

    fun nuevoIdMensaje() {
        viewModelScope.launch {
            _nuevoIdMensaje.value = repMensaje.getMensajes().size+1
        }
    }

    fun sacarCuidadores(idRefugio: Int) {
        viewModelScope.launch {
            val lista: MutableList<Usuario> = mutableListOf()

            repUsuario.getUsuarios().forEach { usuario ->
                if(usuario.refugioid == idRefugio){
                    lista.add(usuario)
                }
            }

            _listaCuidadores.value = lista
        }
    }

    fun sacarMascotasDeRefugio(idRefugio: Int) {
        viewModelScope.launch {
            val lista: MutableList<Mascota> = mutableListOf()

            repMascota.getMascotas().forEach { mascota ->
                if(mascota.refugioid == idRefugio){
                    lista.add(mascota)
                }
            }

            _listaMascotas.value = lista
        }
    }

    fun updateRefugio(id: Int, refugio: Refugio) {
        viewModelScope.launch {
            _respuestaModificarRefugio.value = repRefugio.updateRefugio(id,refugio)
        }
    }

    fun updateUsuario(id: Int, usuario: Usuario) {
        viewModelScope.launch {
            _respuestaModificarUsuario.value = repUsuario.updateUsuario(id,usuario)
        }
    }

    fun updateMascota(id: Int, mascota: Mascota) {
        viewModelScope.launch {
            _respuestaModificarMascota.value = repMascota.updateMascota(id,mascota)
        }
    }

    fun updateAdopcion(id: Int, adopcion: Adopcion) {
        viewModelScope.launch {
            _respuestaModificarAdopcion.value = repAdopcion.updateAdopcion(id,adopcion)
        }
    }

    fun sacarMascotasAdoptadasDeRefugio(idRefugio:Int) {
        viewModelScope.launch {
            val lista: MutableList<Mascota> = mutableListOf()

            repMascota.getMascotas().forEach { mascota ->
                if(mascota.refugioid == idRefugio){
                    repAdopcion.getAdopciones().forEach { adopcion ->
                        if (adopcion.mascotaid == mascota.id && adopcion.iniciofec!=null && adopcion.finfec==null) {
                            lista.add(mascota)
                        }
                    }
                }
            }

            _listaMascotas.value = lista
        }
    }

    fun sacarMascotasSinAdoptarDeRefugio(idRefugio:Int){
        viewModelScope.launch {
            val lista = repMascota.getMascotas().toMutableList()

            repMascota.getMascotas().forEach { mascota ->
                if(mascota.refugioid == idRefugio){
                    var valor=false
                    repAdopcion.getAdopciones().forEach { adopcion ->
                        if (adopcion.mascotaid == mascota.id && adopcion.iniciofec!=null && adopcion.finfec==null) {
                            valor=true
                        }
                    }

                    if(valor){
                        lista.remove(mascota)
                    }
                }
                else
                {
                    lista.remove(mascota)
                }
            }

            _listaMascotas.value = lista
        }
    }

    fun deleteMascota(id: Int) {
        viewModelScope.launch {
            _respuestaEliminarMascota.value = repMascota.deleteMascota(id)
        }
    }

    fun deleteUsuario(id: Int) {
        viewModelScope.launch {
            _respuestaEliminarUsuario.value = repUsuario.deleteUsuario(id)
        }
    }

    fun deleteAdopcion(id: Int) {
        viewModelScope.launch {
            _respuestaEliminarAdopcion.value = repAdopcion.deleteAdopcion(id)
        }
    }

    fun sacarAdopcionesConfirmadas(idRefugio: Int) {
        viewModelScope.launch {
            val lista: MutableList<Adopcion> = mutableListOf()

            repMascota.getMascotas().forEach { mascota ->
                if (mascota.refugioid == idRefugio) {
                    repAdopcion.getAdopciones().forEach { adopcion ->
                        if (adopcion.mascotaid == mascota.id && adopcion.iniciofec!=null) {
                            lista.add(adopcion)
                        }
                    }
                }
            }

            _listaAdopciones.value = lista
        }
    }

    fun sacarAdopcionesPendientes(idRefugio: Int) {
        viewModelScope.launch {
            val lista: MutableList<Adopcion> = mutableListOf()

            repMascota.getMascotas().forEach { mascota ->
                if (mascota.refugioid == idRefugio) {
                    repAdopcion.getAdopciones().forEach { adopcion ->
                        if (adopcion.mascotaid == mascota.id && adopcion.iniciofec==null) {
                            lista.add(adopcion)
                        }
                    }
                }
            }

            _listaAdopciones.value = lista
        }
    }

    fun comprobacionUpdateAdopcion (adopcion: Adopcion, dateIni: Date?, dateFin: Date?) {
        viewModelScope.launch {
            val lista: MutableList<Boolean> = mutableListOf()

            var comp1 = false
            var comp2 = false
            var comp2Ax = false
            var comp3 = false
            var comp4 = false
            var comp5 = false

            repAdopcion.getAdopciones().forEach { adop ->
                if(adop.mascotaid==adopcion.mascotaid && adop.userid==adopcion.userid && adop.solicitudfec!=adopcion.solicitudfec
                    && adop.iniciofec!=null && adop.finfec==null){
                    comp1 = true
                }

                if(adop.mascotaid==adopcion.mascotaid && adop.iniciofec!=null && adop.finfec==null){
                    comp2 = true
                    if(comp2) {
                        comp2Ax = true
                    }
                }

                var dIni: Date?=null
                var dFin: Date?=null

                if(adop.iniciofec!=null){
                    val fec=adop.iniciofec
                    val pos=fec.indexOf("-")
                    val anho=fec.substring(0,pos).toInt()
                    val ax= fec.substring(pos+1)
                    val posAx=ax.indexOf("-")
                    val mes=ax.substring(0,posAx).toInt()
                    val dia=ax.substring(posAx+1).toInt()
                    dIni= Date(anho,mes-1,dia)
                }

                if(adop.finfec!=null){
                    val fecX=adop.finfec
                    val posX=fecX.indexOf("-")
                    val anhoX=fecX.substring(0,posX).toInt()
                    val axX= fecX.substring(posX+1)
                    val posAxX=axX.indexOf("-")
                    val mesX=axX.substring(0,posAxX).toInt()
                    val diaX=axX.substring(posAxX+1).toInt()
                    dFin= Date(anhoX,mesX-1,diaX)
                }

                if (dIni!=null && dFin!=null){

                    if (dateIni!=null && dateFin!=null) {
                        if(adop.mascotaid==adopcion.mascotaid && adop.iniciofec!=null && dIni>dateIni && dFin<dateFin){
                            comp3 = true
                        }
                    }
                    if (dateIni!=null) {
                        if(adop.mascotaid==adopcion.mascotaid && adop.iniciofec!=null && dFin>dateIni && dIni<dateIni){
                            comp4 = true
                        }
                    }
                    if (dateFin!=null) {
                        if(adop.mascotaid==adopcion.mascotaid && adop.iniciofec!=null && dFin>dateFin && dIni<dateFin){
                            comp5 = true
                        }
                    }
                }
            }

            lista.add(comp1)
            lista.add(comp2)
            lista.add(comp3)
            lista.add(comp4)
            lista.add(comp5)
            lista.add(comp2Ax)

            _listaComprobaciones.value = lista
        }
    }

    fun sacarMascotasSinAdoptar () {
        viewModelScope.launch {
            val lista: MutableList<Mascota> = repMascota.getMascotas().toMutableList()

            repMascota.getMascotas().forEach { mascota ->
                repAdopcion.getAdopciones().forEach { adopcion ->
                    if (mascota.id == adopcion.mascotaid && adopcion.iniciofec!=null && adopcion.finfec==null) {
                        lista.remove(mascota)
                    }
                }
            }

            _listaMascotas.value = lista
        }
    }

    fun comprobacionNuevaAdopcion (idUser: Int, idMas:Int) {
        viewModelScope.launch {
            var comp = false

            repAdopcion.getAdopciones().forEach { adopcion ->
                if (adopcion.mascotaid==idMas && adopcion.userid==idUser && adopcion.finfec==null) {
                    comp=true
                }
            }

            _comprobacionInsercionAdop.value = comp
        }
    }

    fun sacarContactos (user: Usuario) {
        viewModelScope.launch {
            val lista: MutableList<Usuario> = mutableListOf()

            repMensaje.getMensajes().forEach { mensaje ->
                if (mensaje.emisorid==user.id || mensaje.receptorid==user.id) {
                    val usu1= repUsuario.getUsuarioPorId(mensaje.emisorid)
                    val usu2= repUsuario.getUsuarioPorId(mensaje.receptorid)

                    lista.add(usu1)
                    lista.add(usu2)
                }
            }

            val lis=lista.distinct().toMutableList()
            lis.remove(user)

            _listaContactos.value = lis
        }
    }

    fun sacarUsuarioPorCorreo (correo: String) {
        viewModelScope.launch {
            repUsuario.getUsuarios().forEach { user ->
                if(user.correo== correo){
                    _contacto.value = user
                }
            }
        }
    }

    fun sacarMensajesDeContacto (id1:Int, id2: Int) {
        viewModelScope.launch {
            val lista: MutableList<Mensaje> = mutableListOf()
            repMensaje.getMensajes().forEach { mensaje ->
                if( (mensaje.emisorid==id1 && mensaje.receptorid==id2)  ||  (mensaje.emisorid==id2 && mensaje.receptorid==id1)   ){
                    lista.add(mensaje)
                }
            }
            _listaMensajes.value = lista
        }
    }

    fun sacarAdopcionesdeUsuario(idUser: Int) {
        viewModelScope.launch {
            val lista: MutableList<Adopcion> = mutableListOf()
            repAdopcion.getAdopciones().forEach { adopcion ->
                if (adopcion.userid == idUser) {
                    lista.add(adopcion)
                }
            }

            _listaHistorial.value = lista
        }
    }
}