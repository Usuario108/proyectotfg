package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminPerfilMasBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminPerfilMas : Fragment() {

    private lateinit var navController: NavController
    private lateinit var animal: Mascota
    private lateinit var binding: FragmentAdminPerfilMasBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            animal = it.get("mascota") as Mascota
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminPerfilMasBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            val longblobBytes: ByteArray = Base64.decode(animal.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            imageViewPerfil.setImageBitmap(bitmap)
            imageViewPerfil.setScaleType(ImageView.ScaleType.FIT_CENTER)

            etNombre.setText(animal.nombre)
            etRaza.setText(animal.raza)
            etPeso.setText(animal.peso.toString())
            etDescripcion.setText(animal.descripcion)

            val fec=animal.nacimiento
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val str=ax.substring(posAx+1)
            val posStr=str.indexOf("T")
            val dia=str.substring(0,posStr)
            etFecha.setText(dia+"-"+mes.toString()+"-" +anho.toString())

            refugioViewModel.getRefugioPorId(animal.refugioid)
            refugioViewModel.refugio.observe(viewLifecycleOwner, Observer { ref ->
                etRefugio.setText(ref.nombre)
            })
        }
    }
}