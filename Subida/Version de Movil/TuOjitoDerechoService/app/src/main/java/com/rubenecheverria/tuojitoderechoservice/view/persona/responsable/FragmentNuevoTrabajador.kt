package com.rubenecheverria.tuojitoderechoservice.view.persona.responsable

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentNuevoTrabajadorBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.io.ByteArrayOutputStream
import java.time.LocalDate

class FragmentNuevoTrabajador : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentNuevoTrabajadorBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentNuevoTrabajadorBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            botonAceptar.setOnClickListener {
                if(ValidarFormulario()){
                    var idRef:Int = 0
                    var idUsu:Int = 0
                    refugioViewModel.nuevoIdRefugio()
                    refugioViewModel.nuevoIdUsuario()
                    refugioViewModel.nuevoIdRefugio.observe(viewLifecycleOwner, Observer { nuevoIdRef ->
                        idRef = nuevoIdRef
                        refugioViewModel.nuevoIdUsuario.observe(viewLifecycleOwner, Observer { nuevoIdUsu ->
                            idUsu = nuevoIdUsu

                            if (refugioViewModel.correos.contains(editTextCorreo.text.toString()))
                            {
                                Toast.makeText(context, "El correo le pertenece a otra cuenta.", Toast.LENGTH_LONG).show()
                            }
                            else
                            {
                                val local= LocalDate.of(dateFecha.year,dateFecha.month+1,dateFecha.dayOfMonth)

                                val bitmap3 = BitmapFactory.decodeResource(resources, R.drawable.im_usuario)
                                val outputStream3 = ByteArrayOutputStream()
                                bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, outputStream3)
                                val byteArray3 = outputStream3.toByteArray()
                                val imagenUsuario = Base64.encodeToString(byteArray3, Base64.DEFAULT)

                                val usu= Usuario(idUsu,auxViewModel.usuarioLogueado.refugioid!!,editTextNombre.text.toString(),local.toString(),editTextTelefono.text.toString(),
                                    editTextCorreo.text.toString(),editTextPassword.text.toString(),editTextDireccion.text.toString(),imagenUsuario)

                                refugioViewModel.insertarUsuario(usu)
                                refugioViewModel.respuestaInsertarUsuario.observe(viewLifecycleOwner, Observer {
                                    Toast.makeText(context, "El cuidador se ha insertado correctamente.", Toast.LENGTH_LONG).show()
                                    navController.navigate(R.id.action_fragmentNuevoTrabajador_to_fragmentListaTrabajadores)
                                })
                            }
                        })
                    })
                }
            }

            botonCancelar.setOnClickListener { navController.navigate(R.id.action_fragmentNuevoTrabajador_to_fragmentListaTrabajadores) }
        }
    }

    private fun ValidarFormulario():Boolean {
        binding.apply {
            if (editTextCorreo.text.toString().isEmpty())
            {
                Toast.makeText(context, "El correo no es valido.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (editTextPassword.text.toString().isEmpty() || editTextConfirm.text.toString().isEmpty())
            {
                Toast.makeText(context, "La contraseña no es valida.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (editTextPassword.text.toString() != editTextConfirm.text.toString())
            {
                Toast.makeText(context, "La contraseña no coincide.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (editTextNombre.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (editTextTelefono.text.toString().isEmpty())
            {
                Toast.makeText(context, "El telefono no es valido.", Toast.LENGTH_LONG).show()
                return false;
            }

            if (editTextDireccion.text.toString().isEmpty())
            {
                Toast.makeText(context, "La direccion no es valida.", Toast.LENGTH_LONG).show()
                return false;
            }

            val date= LocalDate.of(dateFecha.year,dateFecha.month+1,dateFecha.dayOfMonth)
            if (LocalDate.now() < date)
            {
                Toast.makeText(context, "La fecha no es valida.", Toast.LENGTH_LONG).show()
                return false;
            }

            val difFechas = LocalDate.now().year - date.year
            if (difFechas < 18)
            {
                Toast.makeText(context, "No puede registrarse menores de 18 años.", Toast.LENGTH_LONG).show()
                return false;
            }

            return  true
        }
    }
}