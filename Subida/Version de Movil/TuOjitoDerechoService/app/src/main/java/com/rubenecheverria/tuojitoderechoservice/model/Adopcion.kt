package com.rubenecheverria.tuojitoderechoservice.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Adopcion (
    @SerializedName("id")
    val id:Int,
    @SerializedName("userid")
    val userid:Int,
    @SerializedName("mascotaid")
    val mascotaid:Int,
    @SerializedName("solicitudfec")
    val solicitudfec: String,
    @SerializedName("iniciofec")
    val iniciofec: String?,
    @SerializedName("finfec")
    val finfec: String?
) : Serializable