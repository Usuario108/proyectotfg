package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentHistorialAdopcionesBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentHistorialAdopciones : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentHistorialAdopcionesBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentHistorialAdopcionesBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {

            refugioViewModel.sacarMascotas()
            refugioViewModel.listaMascotas.observe(viewLifecycleOwner, Observer { mascotas ->

                refugioViewModel.sacarAdopcionesdeUsuario(auxViewModel.usuarioLogueado.id)
                refugioViewModel.listaHistorial.observe(viewLifecycleOwner, Observer { adopciones ->

                    with(recyclerViewHistorialAdopciones) {
                        adapter = HistorialAdopcionAdapter(mascotas, adopciones, onClickAdopcion = { onClickAdopcion(it) } )
                        layoutManager = LinearLayoutManager(context)
                    }

                    if(adopciones.isEmpty()){
                        textSin.isVisible = true
                    }
                })
            })
        }
    }

    private fun onClickAdopcion(adopcion: Adopcion)
    {
        val accion = FragmentHistorialAdopcionesDirections.actionFragmentHistorialAdopcionesToFragmentAdopcionHist(adopcion=adopcion)
        navController.navigate(accion)
    }
}