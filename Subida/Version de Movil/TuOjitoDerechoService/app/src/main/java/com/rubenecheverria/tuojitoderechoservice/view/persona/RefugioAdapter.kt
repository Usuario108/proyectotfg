package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemRefugioBinding
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.model.Usuario

class RefugioAdapter(
    private val listaUsuarios:List<Usuario>,
    private val listaRefugios:List<Refugio>?,
    private val onClickRefugio: (Refugio) ->Unit
): RecyclerView.Adapter<RefugioAdapter.RefugioViewHolder>()
{

    class RefugioViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val binding = ItemRefugioBinding.bind(view)

        fun bind(listaUsuarios: List<Usuario>, refugio:Refugio, onClickRefugio: (Refugio) -> Unit){

            listaUsuarios.forEach { usuario ->
                if (usuario.id == refugio.responsableid) {
                    binding.textViewResponsable.text = usuario.nombre
                }
            }
            binding.textViewNombre.text= refugio.nombre
            binding.textViewCorreo.text = refugio.correo

            val longblobBytes: ByteArray = Base64.decode(refugio.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            binding.imagenRefugio.setImageBitmap(bitmap)

            binding.cardView.setOnClickListener { onClickRefugio (refugio) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RefugioViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_refugio,parent,false)

        return RefugioViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: RefugioViewHolder, position: Int) {
        if(listaRefugios?.size!=null) {
            val refugio = listaRefugios[position]
            holder.bind(listaUsuarios,refugio, onClickRefugio)
        }
    }

    override fun getItemCount(): Int {
        return if(listaRefugios?.size!=null){
            listaRefugios.size
        }else{
            0
        }
    }
}