package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentChatBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mensaje
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class FragmentChat : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentChatBinding
    private lateinit var contacto: Usuario
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            contacto = it.get("contacto") as Usuario
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentChatBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            textContacto.text = contacto.nombre

            refugioViewModel.sacarMensajesDeContacto(auxViewModel.usuarioLogueado.id, contacto.id)
            refugioViewModel.listaMensajes.observe(viewLifecycleOwner , Observer{ mensajes ->
                with(recyclerViewListaMensajes) {
                    adapter = MensajeAdapter(auxViewModel.usuarioLogueado.id, mensajes.toMutableList() )
                    layoutManager = LinearLayoutManager(context)
                }
            })

            botonFlotante.setOnClickListener {
                if(!etMensaje.text.toString().isEmpty()){

                    refugioViewModel.nuevoIdMensaje()
                    refugioViewModel.nuevoIdMensaje.observe(viewLifecycleOwner, Observer { idMen ->

                        val date= LocalDateTime.now()
                        val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                        val formattedDate = date.format(formatter)

                        var mensaje = Mensaje(idMen,auxViewModel.usuarioLogueado.id,contacto.id, formattedDate,etMensaje.text.toString())
                        refugioViewModel.insertarMensaje(mensaje)
                        refugioViewModel.respuestaInsertarMensaje.observe(viewLifecycleOwner, Observer {
                            (recyclerViewListaMensajes.adapter as MensajeAdapter).addMensaje(mensaje)
                        })
                        etMensaje.setText("")
                    })
                }
            }

            imagenAtras.setOnClickListener {
                navController.navigate(R.id.action_fragmentChat_to_fragmentListaContactos)
            }
        }
    }
}