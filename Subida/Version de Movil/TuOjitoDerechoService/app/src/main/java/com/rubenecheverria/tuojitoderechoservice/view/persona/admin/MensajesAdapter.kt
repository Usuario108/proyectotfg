package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemConversacionBinding
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemRefugioBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mensaje
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.model.Usuario

class MensajesAdapter (
    private val listaMensajes:List<Mensaje>,
    private val listaUsuarios:List<Usuario>
    ): RecyclerView.Adapter<MensajesAdapter.MensajesViewHolder>() {

    class MensajesViewHolder(view: View): RecyclerView.ViewHolder(view)
    {
        private val binding = ItemConversacionBinding.bind(view)

        @SuppressLint("SetTextI18n")
        fun bind(listaUsuarios: List<Usuario>, mensaje: Mensaje){

            binding.textViewFecha.text = mensaje.hora

            listaUsuarios.forEach { usuario ->
                if (usuario.id == mensaje.emisorid) {
                    binding.textViewRemitente.text = "De: "+usuario.nombre
                }

                if (usuario.id == mensaje.receptorid) {
                    binding.textViewDestinatario.text = "Para: "+usuario.nombre
                }
            }

            binding.textViewMensaje.text = mensaje.mensaje
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MensajesViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_conversacion, parent, false)

        return MensajesViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: MensajesViewHolder, position: Int) {
        val mensaje = listaMensajes[position]
        holder.bind(listaUsuarios,mensaje)
    }

    override fun getItemCount(): Int = listaMensajes.size

}