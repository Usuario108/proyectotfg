package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminPerfilRefBinding
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminPerfilRef : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminPerfilRefBinding
    private lateinit var refugio: Refugio
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            refugio = it.get("refugio") as Refugio
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminPerfilRefBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {

            refugioViewModel.getUsuarioPorId(refugio.responsableid)
            refugioViewModel.usuario.observe(viewLifecycleOwner, Observer { usuario ->

                refugioViewModel.sacarMascotasDeRefugio(refugio.id)
                refugioViewModel.listaMascotas.observe(viewLifecycleOwner, Observer { mascotas ->

                    val longblobBytes: ByteArray = Base64.decode(refugio.imagen, Base64.DEFAULT)
                    val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
                    imageViewPerfil.setImageBitmap(bitmap)
                    tvEmail.text = refugio.correo
                    etNombre.setText(refugio.nombre)

                    etNumMaxAnimales.setText(mascotas.size.toString())
                    etTelefono.setText(refugio.telefono)
                    etDireccion.setText(refugio.direccion)
                    etResponsable.setText(usuario.nombre)
                })
            })
        }
    }
}