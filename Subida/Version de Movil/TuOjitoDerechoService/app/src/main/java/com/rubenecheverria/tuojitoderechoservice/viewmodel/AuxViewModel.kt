package com.rubenecheverria.tuojitoderechoservice.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.model.Usuario

class AuxViewModel (context: Context): ViewModel()
{
    lateinit var refugioLogueado : Refugio
    lateinit var usuarioLogueado : Usuario

    companion object {
        val Factory: ViewModelProvider.Factory = viewModelFactory {
            initializer {
                val contexto = (this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as Context)
                AuxViewModel(
                    context = contexto
                )
            }
        }
    }
}