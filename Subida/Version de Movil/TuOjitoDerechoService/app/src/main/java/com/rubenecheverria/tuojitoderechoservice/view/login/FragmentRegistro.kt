package com.rubenecheverria.tuojitoderechoservice.view.login

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentRegistroBinding
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.view.persona.dueno.DuenoActivity
import com.rubenecheverria.tuojitoderechoservice.view.protectora.ProtectoraActivity
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel
import java.io.ByteArrayOutputStream
import java.time.LocalDate

class FragmentRegistro : Fragment() {

    private lateinit var binding: FragmentRegistroBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentRegistroBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            opcionRefugio.isVisible = false
            opcionUsuario.isVisible = false

            radioGrupo.setOnCheckedChangeListener { p0, p1 ->
                if (p1 == R.id.radioButtonRefugio) {
                    opcionRefugio.isVisible = true
                    opcionUsuario.isVisible = false
                }
                if (p1 == R.id.radioButtonUsuario) {
                    opcionRefugio.isVisible = false
                    opcionUsuario.isVisible = true
                }
            }

            botonAceptar.setOnClickListener {
                if(ValidarFormulario()){
                    var idRef:Int = 0
                    var idUsu:Int = 0
                    refugioViewModel.nuevoIdRefugio()
                    refugioViewModel.nuevoIdUsuario()
                    refugioViewModel.nuevoIdRefugio.observe(viewLifecycleOwner, Observer { nuevoIdRef ->
                        idRef = nuevoIdRef
                        refugioViewModel.nuevoIdUsuario.observe(viewLifecycleOwner, Observer { nuevoIdUsu ->
                            idUsu = nuevoIdUsu

                            if (refugioViewModel.correos.contains(editTextEmail.text.toString()))
                            {
                                Toast.makeText(context, "El correo le pertenece a otra cuenta.", Toast.LENGTH_LONG).show()
                            }
                            else
                            {
                                if (radioGrupo.checkedRadioButtonId == R.id.radioButtonRefugio)
                                {

                                    if (refugioViewModel.correos.contains(editTextCorreoUsu.text.toString())) {
                                        Toast.makeText(context, "El correo del responsable le pertenece a otra cuenta.", Toast.LENGTH_LONG).show()
                                    }
                                    else
                                    {
                                        val bitmap = BitmapFactory.decodeResource(resources, R.drawable.im_refugio)
                                        val outputStream = ByteArrayOutputStream()
                                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                                        val byteArray = outputStream.toByteArray()
                                        val imagenRefugio = Base64.encodeToString(byteArray, Base64.DEFAULT)

                                        val refugio= Refugio(idRef,editTextNombre.text.toString(),editTextDireccion.text.toString(),
                                            editTextTelefono.text.toString(),editTextEmail.text.toString(),editTextPassword.text.toString(),
                                            editTextMaxAnimales.text.toString().toInt(),idUsu,imagenRefugio)


                                        val local= LocalDate.of(dateNacimientoUsu.year,dateNacimientoUsu.month+1,dateNacimientoUsu.dayOfMonth)

                                        val bitmap2 = BitmapFactory.decodeResource(resources, R.drawable.im_usuario)
                                        val outputStream2 = ByteArrayOutputStream()
                                        bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, outputStream2)
                                        val byteArray2 = outputStream2.toByteArray()
                                        val imagenUsuario = Base64.encodeToString(byteArray2, Base64.DEFAULT)

                                        val usu= Usuario(idUsu,idRef,editTextNombreUsu.text.toString(),local.toString(),editTextTelefonoUsu.text.toString(),
                                            editTextCorreoUsu.text.toString(),editTextPasswordUsu.text.toString(),editTextDireccionUsu.text.toString(),imagenUsuario)


                                        refugioViewModel.insertarRefugio(refugio)
                                        refugioViewModel.respuestaInsertarRefugio.observe(viewLifecycleOwner, Observer {

                                            refugioViewModel.insertarUsuario(usu)
                                            refugioViewModel.respuestaInsertarUsuario.observe(viewLifecycleOwner, Observer {
                                                val intent = Intent(context, ProtectoraActivity::class.java)
                                                intent.putExtra("refugio", refugio)
                                                startActivity(intent)
                                            })
                                        })
                                    }
                                }else{
                                    val local= LocalDate.of(dateNacimiento.year,dateNacimiento.month+1,dateNacimiento.dayOfMonth)

                                    val bitmap3 = BitmapFactory.decodeResource(resources, R.drawable.im_usuario)
                                    val outputStream3 = ByteArrayOutputStream()
                                    bitmap3.compress(Bitmap.CompressFormat.JPEG, 100, outputStream3)
                                    val byteArray3 = outputStream3.toByteArray()
                                    val imagenUsuario = Base64.encodeToString(byteArray3, Base64.DEFAULT)

                                    val usu= Usuario(idUsu,null,editTextNombre.text.toString(),local.toString(),editTextTelefono.text.toString(),
                                        editTextEmail.text.toString(),editTextPassword.text.toString(),editTextDireccion.text.toString(),imagenUsuario)

                                    refugioViewModel.insertarUsuario(usu)
                                    refugioViewModel.respuestaInsertarUsuario.observe(viewLifecycleOwner, Observer {
                                        val intent = Intent(context, DuenoActivity::class.java)
                                        intent.putExtra("dueño", usu)
                                        startActivity(intent)
                                    })
                                }
                            }
                        })
                    })
                }
            }
        }
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (editTextEmail.text.toString().isEmpty())
            {
                Toast.makeText(context, "El correo no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (editTextPassword.text.toString().isEmpty() || editTextConfirm.text.toString().isEmpty())
            {
                Toast.makeText(context, "La contraseña no es valida.", Toast.LENGTH_LONG).show()
                return false
            }

            if (editTextPassword.text.toString() != editTextConfirm.text.toString())
            {
                Toast.makeText(context, "La contraseña no coincide.", Toast.LENGTH_LONG).show()
                return false
            }

            if (editTextDireccion.text.toString().isEmpty())
            {
                Toast.makeText(context, "La direccion no es valida.", Toast.LENGTH_LONG).show()
                return false
            }

            if (editTextNombre.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (editTextTelefono.text.toString().isEmpty())
            {
                Toast.makeText(context, "El telefono no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if(radioGrupo.checkedRadioButtonId == R.id.radioButtonRefugio){

                if (editTextMaxAnimales.text.toString().isEmpty())
                {
                    Toast.makeText(context, "El numero de animales no es valido.", Toast.LENGTH_LONG).show()
                    return false
                }

                if (editTextCorreoUsu.text.toString().isEmpty())
                {
                    Toast.makeText(context, "El correo del responsable no es valido.", Toast.LENGTH_LONG).show()
                    return false
                }

                if (editTextPasswordUsu.text.toString().isEmpty())
                {
                    Toast.makeText(context, "La contraseña del responsable no es valida.", Toast.LENGTH_LONG).show()
                    return false
                }

                if (editTextNombreUsu.text.toString().isEmpty())
                {
                    Toast.makeText(context, "El nombre del responsable no es valido.", Toast.LENGTH_LONG).show()
                    return false
                }

                if (editTextTelefonoUsu.text.toString().isEmpty())
                {
                    Toast.makeText(context, "El telefono del responsable no es valido.", Toast.LENGTH_LONG).show()
                    return false
                }

                if (editTextDireccionUsu.text.toString().isEmpty())
                {
                    Toast.makeText(context, "La direccion del responsable no es valida.", Toast.LENGTH_LONG).show()
                    return false
                }

                val date= LocalDate.of(dateNacimientoUsu.year,dateNacimientoUsu.month+1,dateNacimientoUsu.dayOfMonth)
                if (LocalDate.now() < date)
                {
                    Toast.makeText(context, "La fecha del responsable no es valida.", Toast.LENGTH_LONG).show()
                    return false
                }

                val difFechas = LocalDate.now().year - date.year
                if (difFechas < 18)
                {
                    Toast.makeText(context, "El responsable no puede registrarse menores de 18 años.", Toast.LENGTH_LONG).show()
                    return false
                }

            }else if(radioGrupo.checkedRadioButtonId == R.id.radioButtonUsuario){

                val date= LocalDate.of(dateNacimiento.year,dateNacimiento.month+1,dateNacimiento.dayOfMonth)
                if (LocalDate.now() < date)
                {
                    Toast.makeText(context, "La fecha no es valida.", Toast.LENGTH_LONG).show()
                    return false
                }

                val difFechas = LocalDate.now().year - date.year
                if (difFechas < 18)
                {
                    Toast.makeText(context, "No puede registrarse menores de 18 años.", Toast.LENGTH_LONG).show()
                    return false
                }

            }else{
                Toast.makeText(context, "Es necesario especificar si es un refugio o un usuario independiente.", Toast.LENGTH_LONG).show()
                return false
            }

            return true
        }
    }
}