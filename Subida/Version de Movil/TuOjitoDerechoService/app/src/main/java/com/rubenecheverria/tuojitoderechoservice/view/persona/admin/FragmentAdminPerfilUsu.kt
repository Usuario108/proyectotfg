package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminPerfilUsuBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminPerfilUsu : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminPerfilUsuBinding
    private lateinit var usuario: Usuario
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            usuario = it.get("usuario") as Usuario
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminPerfilUsuBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            val longblobBytes: ByteArray = Base64.decode(usuario.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            imageViewPerfil.setImageBitmap(bitmap)
            imageViewPerfil.setScaleType(ImageView.ScaleType.FIT_CENTER)

            tvEmail.text = usuario.correo
            etNombre.setText(usuario.nombre)
            etTelefono.setText(usuario.telefono)
            etDireccion.setText(usuario.direccion)

            val fec=usuario.nacimiento
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val dia=ax.substring(posAx+1).toInt()
            etFecha.setText(dia.toString()+"-"+mes.toString()+"-" +anho.toString())
        }
    }
}