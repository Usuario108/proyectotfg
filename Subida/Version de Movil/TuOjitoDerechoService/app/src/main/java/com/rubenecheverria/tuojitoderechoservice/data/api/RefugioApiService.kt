package com.rubenecheverria.tuojitoderechoservice.data.api

import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.model.Mensaje
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

private const val URL_BASE = "http://192.168.0.18:8080"

private val retrofit = Retrofit.Builder()
    .baseUrl(URL_BASE)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

interface RefugioApiService
{
    @GET("/refugios")
    suspend fun getRefugios() : List<Refugio>

    @GET("/refugios/{id}")
    suspend fun getRefugioPorId(@Path("id") id: Int) : Refugio

    @POST("/refugios")
    suspend fun insertarRefugio(@Body refugio: Refugio): Response<Refugio>

    @PUT("/refugios/{id}")
    suspend fun updateRefugio(@Path("id") id: Int, @Body refugio: Refugio): Response<Refugio>

    @GET("/mascotas")
    suspend fun getMascotas() : List<Mascota>

    @GET("/mascotas/{id}")
    suspend fun getMascotaPorId(@Path("id") id: Int) : Mascota

    @POST("/mascotas")
    suspend fun insertarMascota(@Body mascota: Mascota): Response<Mascota>

    @PUT("/mascotas/{id}")
    suspend fun updateMascota(@Path("id") id: Int, @Body mascota: Mascota): Response<Mascota>

    @DELETE("/mascotas/{id}")
    suspend fun deleteMascota(@Path("id") id: Int): Response<Mascota>

    @GET("/usuarios")
    suspend fun getUsuarios() : List<Usuario>

    @GET("/usuarios/{id}")
    suspend fun getUsuarioPorId(@Path("id") id: Int) : Usuario

    @POST("/usuarios")
    suspend fun insertarUsuario(@Body usuario: Usuario): Response<Usuario>

    @PUT("/usuarios/{id}")
    suspend fun updateUsuario(@Path("id") id: Int, @Body usuario: Usuario): Response<Usuario>

    @DELETE("/usuarios/{id}")
    suspend fun deleteUsuario(@Path("id") id: Int): Response<Usuario>

    @GET("/mensajes")
    suspend fun getMensajes() : List<Mensaje>

    @POST("/mensajes")
    suspend fun insertarMensaje(@Body mensaje: Mensaje): Response<Mensaje>

    @GET("/adopciones")
    suspend fun getAdopciones() : List<Adopcion>

    @POST("/adopciones")
    suspend fun insertarAdopcion(@Body adopcion: Adopcion): Response<Adopcion>

    @PUT("/adopciones/{id}")
    suspend fun updateAdopcion(@Path("id") id: Int, @Body adopcion: Adopcion): Response<Adopcion>

    @DELETE("/adopciones/{id}")
    suspend fun deleteAdopcion(@Path("id") id: Int): Response<Adopcion>

}

object RefugiosApi {
    val retrofitService: RefugioApiService by lazy { retrofit.create(RefugioApiService::class.java) }
}