package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemAdopcionBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.model.Usuario

class SolicitudAdapter (
    private val listaUsuarios:List<Usuario>,
    private val listaMascotas:List<Mascota>,
    private val listaSolicitudes:List<Adopcion>?,
    private val onClickAdopcion: (Adopcion) ->Unit
) : RecyclerView.Adapter<SolicitudAdapter.SolicitudViewHolder>(){

    class SolicitudViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val binding = ItemAdopcionBinding.bind(view)

        @SuppressLint("SetTextI18n")
        fun bind(listaUsuarios: List<Usuario>, listaMascotas: List<Mascota>, adopcion: Adopcion, onClickAdopcion: (Adopcion) -> Unit) {
            listaUsuarios.forEach { user ->
                if(user.id == adopcion.userid){
                    binding.tvDueno.text = user.nombre
                }
            }
            listaMascotas.forEach { mascota ->
                if(mascota.id == adopcion.mascotaid){
                    binding.tvMascota.text = mascota.nombre
                }
            }

            val fec=adopcion.solicitudfec!!
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val dia=ax.substring(posAx+1).toInt()

            binding.tvFecha.text = "$dia-$mes-$anho"
            binding.cardView.setOnClickListener { onClickAdopcion (adopcion) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SolicitudViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_adopcion,parent,false)

        return SolicitudViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: SolicitudViewHolder, position: Int) {
        if(listaSolicitudes?.size!=null) {
            val solicitud = listaSolicitudes[position]
            holder.bind(listaUsuarios, listaMascotas, solicitud, onClickAdopcion)
        }
    }

    override fun getItemCount(): Int {
        return if(listaSolicitudes?.size!=null){
            listaSolicitudes.size
        }else{
            0
        }
    }
}