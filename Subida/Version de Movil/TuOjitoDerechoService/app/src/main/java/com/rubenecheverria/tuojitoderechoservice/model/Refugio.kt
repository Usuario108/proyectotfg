package com.rubenecheverria.tuojitoderechoservice.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Refugio (
    @SerializedName("id")
    val id:Int,
    @SerializedName("nombre")
    val nombre:String,
    @SerializedName("direccion")
    val direccion:String,
    @SerializedName("telefono")
    val telefono:String,
    @SerializedName("correo")
    val correo:String,
    @SerializedName("password")
    val password:String,
    @SerializedName("maximoanimales")
    val maximoanimales:Int,
    @SerializedName("responsableid")
    val responsableid:Int,
    @SerializedName("imagen")
    val imagen: String
) : Serializable