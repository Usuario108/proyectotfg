package com.rubenecheverria.tuojitoderechoservice.view.persona.admin

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentAdminMensajesBinding
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentAdminMensajes : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentAdminMensajesBinding
    private lateinit var refugioViewModel: RefugioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentAdminMensajesBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarUsuarios()
            refugioViewModel.listaCuidadores.observe(viewLifecycleOwner , Observer{ usuarios ->

                refugioViewModel.sacarMensajes()
                refugioViewModel.listaMens.observe(viewLifecycleOwner, Observer { mensajes ->
                    with(binding.recyclerViewAdminMensajes) {
                        adapter = MensajesAdapter(mensajes, usuarios )
                        layoutManager = LinearLayoutManager(context)
                    }
                })
            })
        }
    }
}