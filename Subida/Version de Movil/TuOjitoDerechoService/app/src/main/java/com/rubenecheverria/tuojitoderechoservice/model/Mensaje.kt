package com.rubenecheverria.tuojitoderechoservice.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Mensaje (
    @SerializedName("id")
    val id:Int,
    @SerializedName("emisorid")
    val emisorid:Int,
    @SerializedName("receptorid")
    val receptorid:Int,
    @SerializedName("hora")
    val hora:String,
    @SerializedName("mensaje")
    val mensaje:String
) : Serializable