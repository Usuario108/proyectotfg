package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.R
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentPerfilRefugioBinding
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentPerfilRefugio : Fragment() {

    private lateinit var binding: FragmentPerfilRefugioBinding
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentPerfilRefugioBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            tvEmail.text = auxViewModel.refugioLogueado.correo

            val longblobBytes: ByteArray = Base64.decode(auxViewModel.refugioLogueado.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            imageViewPerfil.setImageBitmap(bitmap)
            imageViewPerfil.setScaleType(ImageView.ScaleType.FIT_CENTER)

            etNombre.setText(auxViewModel.refugioLogueado.nombre)
            etNumMaxAnimales.setText(auxViewModel.refugioLogueado.maximoanimales.toString())
            etDireccion.setText(auxViewModel.refugioLogueado.direccion)
            etTelefono.setText(auxViewModel.refugioLogueado.telefono)

            val lista = mutableListOf<String>()
            refugioViewModel.getUsuarioPorId(auxViewModel.refugioLogueado.responsableid)
            refugioViewModel.usuario.observe(viewLifecycleOwner, Observer { responsable ->
                lista.add(""+responsable.id+"-"+responsable.nombre)

                refugioViewModel.sacarCuidadores(auxViewModel.refugioLogueado.id)
                refugioViewModel.listaCuidadores.observe(viewLifecycleOwner, Observer { usuarios ->
                    usuarios.forEach { u->
                        lista.add(""+u.id+"-"+u.nombre)
                    }
                    val distinct = lista.distinct()

                    val adapterCategorias = ArrayAdapter(requireContext(), R.layout.simple_spinner_item, distinct)
                    spinnerResponsable.adapter = adapterCategorias

                    botonAceptar.setOnClickListener {
                        if(ValidarFormulario()){

                            refugioViewModel.sacarMascotasDeRefugio(auxViewModel.refugioLogueado.id)
                            refugioViewModel.listaMascotas.observe(viewLifecycleOwner, Observer { mascotas ->

                                //Comprobar el numero de animales y modificar animales
                                if(etNumMaxAnimales.text.toString().toInt()<mascotas.size){
                                    Toast.makeText(context, "El limite de animales es menor que el numero de animales en plantilla.", Toast.LENGTH_LONG).show()
                                }else{
                                    val nom=etNombre.text.toString()
                                    val num=etNumMaxAnimales.text.toString().toInt()
                                    val dir=etDireccion.text.toString()
                                    val tel=etTelefono.text.toString()
                                    val idR=spinnerResponsable.selectedItem.toString().substring(0,spinnerResponsable.selectedItem.toString().indexOf("-")).toInt()

                                    var update= Refugio(0,nom,dir,tel, auxViewModel.refugioLogueado.correo, auxViewModel.refugioLogueado.password,num,idR, auxViewModel.refugioLogueado.imagen)
                                    refugioViewModel.updateRefugio(auxViewModel.refugioLogueado.id, update)
                                    refugioViewModel.respuestaModificarRefugio.observe(viewLifecycleOwner, Observer {
                                        Toast.makeText(context, "Se han modificado los datos del refugio.", Toast.LENGTH_LONG).show()
                                        val ax =auxViewModel.refugioLogueado
                                        auxViewModel.refugioLogueado = Refugio(ax.id,nom,dir,tel,ax.correo,ax.password,num,idR,ax.imagen)
                                    })
                                }
                            })
                        }
                    }
                })
            })
        }
    }

    private fun ValidarFormulario():Boolean{
        binding.apply {
            if (etNombre.text.toString().isEmpty())
            {
                Toast.makeText(context, "El nombre no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etNumMaxAnimales.text.toString().isEmpty())
            {
                Toast.makeText(context, "El maximo de animales no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etTelefono.text.toString().isEmpty())
            {
                Toast.makeText(context, "El telefono no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            if (etDireccion.text.toString().isEmpty())
            {
                Toast.makeText(context, "La direccion no es valido.", Toast.LENGTH_LONG).show()
                return false
            }

            return true
        }
    }
}