package com.rubenecheverria.tuojitoderechoservice.view.persona.dueno

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ActivityDuenoBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario
import com.rubenecheverria.tuojitoderechoservice.view.login.MainActivity
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class DuenoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDuenoBinding
    private lateinit var navController: NavController
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by viewModels  { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityDuenoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)
        auxViewModel.usuarioLogueado = intent.extras?.get("dueño") as Usuario

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fcvDueno) as NavHostFragment
        navController = navHostFragment.navController

        binding.apply {
            toggle = ActionBarDrawerToggle(this@DuenoActivity, drawerLayout, R.string.opciones, R.string.opciones)
            drawerLayout.addDrawerListener(toggle)
            toggle.syncState()
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            nvDueno.setCheckedItem(R.id.perfil)
            nvDueno.setNavigationItemSelectedListener {
                when (it.itemId) {
                    R.id.perfil -> { navController.navigate(R.id.fragmentPerfilUsuario) }
                    R.id.refugios -> { navController.navigate(R.id.fragmentListaRefugios) }
                    R.id.animales -> { navController.navigate(R.id.fragmentListaMascotas) }
                    R.id.chat -> { navController.navigate(R.id.fragmentListaContactos) }
                    R.id.adopciones -> { navController.navigate(R.id.fragmentHistorialAdopciones) }
                    R.id.cerrar -> {
                        val intent = Intent(this@DuenoActivity, MainActivity::class.java)
                        startActivity(intent)
                    }
                }
                drawerLayout.closeDrawer(GravityCompat.START)
                true
            }

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}