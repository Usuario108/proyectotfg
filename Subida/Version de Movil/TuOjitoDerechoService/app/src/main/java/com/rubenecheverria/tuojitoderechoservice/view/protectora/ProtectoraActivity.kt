package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.os.bundleOf
import androidx.core.view.GravityCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ActivityProtectoraBinding
import com.rubenecheverria.tuojitoderechoservice.model.Refugio
import com.rubenecheverria.tuojitoderechoservice.view.login.MainActivity
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class ProtectoraActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProtectoraBinding
    private lateinit var navController: NavController
    private lateinit var toggle: ActionBarDrawerToggle
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by viewModels { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding=ActivityProtectoraBinding.inflate(layoutInflater)
        setContentView(binding.root)

        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)
        auxViewModel.refugioLogueado = intent.extras?.get("refugio") as Refugio

        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fcvProtectora) as NavHostFragment
        navController = navHostFragment.navController

        binding.apply {
            toggle = ActionBarDrawerToggle(this@ProtectoraActivity, drawerLayout, R.string.opciones, R.string.opciones)
            drawerLayout.addDrawerListener(toggle)
            toggle.syncState()
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            nvProtectora.setCheckedItem(R.id.perfil)
            nvProtectora.setNavigationItemSelectedListener {
                when (it.itemId) {
                    R.id.perfil -> { navController.navigate(R.id.fragmentPerfilRefugio) }
                    R.id.adoptados -> {
                        val elim = bundleOf("eliminacion" to false)
                        navController.navigate(R.id.fragmentListaAnimales,elim)
                    }
                    R.id.sinadoptar -> {
                        val elim = bundleOf("eliminacion" to true)
                        navController.navigate(R.id.fragmentListaAnimales,elim)
                    }
                    R.id.cuidadores -> { navController.navigate(R.id.fragmentListaCuidadores) }
                    R.id.aceptadas -> {
                        val elim = bundleOf("aceptadas" to true)
                        navController.navigate(R.id.fragmentListaAdopciones,elim)
                    }
                    R.id.pendientes -> {
                        val elim = bundleOf("aceptadas" to false)
                        navController.navigate(R.id.fragmentListaAdopciones,elim)
                    }
                    R.id.cerrar -> {
                        val intent = Intent(this@ProtectoraActivity, MainActivity::class.java)
                        startActivity(intent)
                    }
                }
                drawerLayout.closeDrawer(GravityCompat.START)
                true
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

}