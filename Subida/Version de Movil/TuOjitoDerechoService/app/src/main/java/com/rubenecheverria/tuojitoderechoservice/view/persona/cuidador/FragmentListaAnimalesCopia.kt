package com.rubenecheverria.tuojitoderechoservice.view.persona.cuidador

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentListaAnimalesCopiaBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mascota
import com.rubenecheverria.tuojitoderechoservice.view.protectora.AnimalAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentListaAnimalesCopia : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentListaAnimalesCopiaBinding
    private var eliminacion:Boolean = false
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            eliminacion = it.get("eliminacion") as Boolean
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentListaAnimalesCopiaBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            if(eliminacion){
                refugioViewModel.sacarMascotasSinAdoptarDeRefugio(auxViewModel.usuarioLogueado.refugioid!!)

                refugioViewModel.listaMascotas.observe(viewLifecycleOwner, Observer { mascotas ->
                    with(recyclerViewListaAnimales) {
                        adapter = AnimalAdapter(mascotas) { mascota -> onClickMascota(mascota) }
                        layoutManager = LinearLayoutManager(context)
                    }

                    if(mascotas.isEmpty()){
                        textSin.isVisible = true
                    }
                })
            }else{
                refugioViewModel.sacarMascotasAdoptadasDeRefugio(auxViewModel.usuarioLogueado.refugioid!!)

                refugioViewModel.listaMascotas.observe(viewLifecycleOwner, Observer { mascotas ->
                    with(recyclerViewListaAnimales) {
                        adapter = AnimalAdapter(mascotas) { mascota -> onClickMascota(mascota) }
                        layoutManager = LinearLayoutManager(context)
                    }

                    if(mascotas.isEmpty()){
                        textSin.isVisible = true
                    }
                })
            }
        }

        binding.botonFlotante.setOnClickListener { navController.navigate(R.id.action_fragmentListaAnimalesCopia_to_fragmentNuevoAnimalCopia) }
    }

    private fun onClickMascota(mascota: Mascota)
    {
        val accion = FragmentListaAnimalesCopiaDirections.actionFragmentListaAnimalesCopiaToFragmentPerfilAnimalCopia(mascota=mascota,eliminacion = eliminacion)
        navController.navigate(accion)
    }
}