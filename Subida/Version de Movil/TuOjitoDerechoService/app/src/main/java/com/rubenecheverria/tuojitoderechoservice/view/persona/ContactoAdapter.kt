package com.rubenecheverria.tuojitoderechoservice.view.persona

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemContactoBinding
import com.rubenecheverria.tuojitoderechoservice.model.Usuario

class ContactoAdapter (
    private val listaContactos: MutableList<Usuario>?,
    private val onClickContacto: (Usuario) ->Unit
): RecyclerView.Adapter<ContactoAdapter.ContactoViewHolder>()
{

    class ContactoViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val binding = ItemContactoBinding.bind(view)

        fun bind(contacto: Usuario, onClickContacto: (Usuario) -> Unit) {
            binding.textViewNombre.text = contacto.nombre

            val longblobBytes: ByteArray = Base64.decode(contacto.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            binding.imagenContacto.setImageBitmap(bitmap)

            binding.cardView.setOnClickListener { onClickContacto (contacto) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_contacto,parent,false)

        return ContactoViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ContactoViewHolder, position: Int) {
        if(listaContactos?.size!=null) {
            val contacto = listaContactos[position]
            holder.bind(contacto, onClickContacto)
        }
    }

    override fun getItemCount(): Int {
        return if(listaContactos?.size!=null){
            listaContactos.size
        }else{
            0
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addContacto(user:Usuario) {
        listaContactos?.add(user)
        notifyDataSetChanged()
    }

}