package com.rubenecheverria.tuojitoderechoservice.view.protectora

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rubenecheverria.tuojitoderechoservice.R
import com.rubenecheverria.tuojitoderechoservice.databinding.ItemAnimalBinding
import com.rubenecheverria.tuojitoderechoservice.model.Mascota

class AnimalAdapter (
    private val listaAnimales:List<Mascota>?,
    private val onClickAnimal: (Mascota) ->Unit
) : RecyclerView.Adapter<AnimalAdapter.AnimalViewHolder>()
{

    class AnimalViewHolder(view: View): RecyclerView.ViewHolder(view){
        private val binding = ItemAnimalBinding.bind(view)

        @SuppressLint("SetTextI18n")
        fun bind(mascota: Mascota, onClickAnimal: (Mascota) -> Unit){
            binding.textViewNombre.text = mascota.nombre
            binding.textViewRaza.text = mascota.raza

            val fec=mascota.nacimiento
            val pos=fec.indexOf("-")
            val anho=fec.substring(0,pos).toInt()
            val ax= fec.substring(pos+1)
            val posAx=ax.indexOf("-")
            val mes=ax.substring(0,posAx).toInt()
            val str=ax.substring(posAx+1)
            val posStr=str.indexOf("T")
            val dia=str.substring(0,posStr)
            binding.textViewFecha.text= "$dia-$mes-$anho"

            val longblobBytes: ByteArray = Base64.decode(mascota.imagen, Base64.DEFAULT)
            val bitmap: Bitmap? = BitmapFactory.decodeByteArray(longblobBytes, 0, longblobBytes.size)
            binding.imagenAnimal.setImageBitmap(bitmap)

            binding.cardView.setOnClickListener { onClickAnimal (mascota) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalViewHolder {
        val adapterLayout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_animal,parent,false)

        return AnimalViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: AnimalViewHolder, position: Int) {
        if(listaAnimales?.size!=null) {
            val mascota = listaAnimales[position]
            holder.bind(mascota, onClickAnimal)
        }
    }

    override fun getItemCount(): Int {
        return if(listaAnimales?.size!=null){
            listaAnimales.size
        }else{
            0
        }
    }
}