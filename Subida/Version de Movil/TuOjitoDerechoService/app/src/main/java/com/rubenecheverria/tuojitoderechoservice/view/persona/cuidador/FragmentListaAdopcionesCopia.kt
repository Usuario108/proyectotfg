package com.rubenecheverria.tuojitoderechoservice.view.persona.cuidador

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rubenecheverria.tuojitoderechoservice.databinding.FragmentListaAdopcionesCopiaBinding
import com.rubenecheverria.tuojitoderechoservice.model.Adopcion
import com.rubenecheverria.tuojitoderechoservice.view.protectora.SolicitudAdapter
import com.rubenecheverria.tuojitoderechoservice.viewmodel.AuxViewModel
import com.rubenecheverria.tuojitoderechoservice.viewmodel.RefugioViewModel

class FragmentListaAdopcionesCopia : Fragment() {

    private lateinit var navController: NavController
    private lateinit var binding: FragmentListaAdopcionesCopiaBinding
    private var aceptadas:Boolean = false
    private lateinit var refugioViewModel: RefugioViewModel
    private val auxViewModel: AuxViewModel by activityViewModels { AuxViewModel.Factory}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            aceptadas = it.get("aceptadas") as Boolean
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding= FragmentListaAdopcionesCopiaBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = view.findNavController()
        refugioViewModel = ViewModelProvider(this).get(RefugioViewModel::class.java)

        binding.apply {
            refugioViewModel.sacarMascotas()
            refugioViewModel.sacarUsuarios()

            refugioViewModel.listaCuidadores.observe(viewLifecycleOwner , Observer{ usuarios ->
                refugioViewModel.listaMascotas.observe(viewLifecycleOwner , Observer{ mascotas ->

                    if(aceptadas){
                        refugioViewModel.sacarAdopcionesConfirmadas(auxViewModel.usuarioLogueado.refugioid!!)
                        refugioViewModel.listaAdopciones.observe(viewLifecycleOwner, Observer { adopciones ->

                            with(binding.recyclerViewListaAdopciones) {
                                adapter = SolicitudAdapter(usuarios,mascotas, adopciones, onClickAdopcion = { onClickAdopcion(it) } )
                                layoutManager = LinearLayoutManager(context)
                            }

                            if(adopciones.isEmpty()){
                                binding.textSin.isVisible = true
                            }
                        })
                    }else{
                        refugioViewModel.sacarAdopcionesPendientes(auxViewModel.usuarioLogueado.refugioid!!)
                        refugioViewModel.listaAdopciones.observe(viewLifecycleOwner, Observer { adopciones ->
                            with(binding.recyclerViewListaAdopciones) {
                                adapter = SolicitudAdapter(usuarios,mascotas, adopciones, onClickAdopcion = { onClickAdopcion(it) } )
                                layoutManager = LinearLayoutManager(context)
                            }

                            if(adopciones.isEmpty()){
                                binding.textSin.isVisible = true
                            }
                        })
                    }

                })
            })
        }
    }

    private fun onClickAdopcion(adopcion: Adopcion)
    {
        val accion = FragmentListaAdopcionesCopiaDirections.actionFragmentListaAdopcionesCopiaToFragmentPerfilAdopcionCopia(adopcion=adopcion)
        navController.navigate(accion)
    }
}